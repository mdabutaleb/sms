-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 11, 2017 at 01:45 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.0.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scms`
--

-- --------------------------------------------------------

--
-- Table structure for table `acsessions`
--

CREATE TABLE `acsessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `end_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `acsessions`
--

INSERT INTO `acsessions` (`id`, `title`, `start_date`, `end_date`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, '2017', '2017-01-01', '2017-12-31', NULL, NULL, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `atnd_dates`
--

CREATE TABLE `atnd_dates` (
  `id` int(10) UNSIGNED NOT NULL,
  `date_text` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `date_str` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atnd_dates`
--

INSERT INTO `atnd_dates` (`id`, `date_text`, `date_str`, `created_at`, `updated_at`) VALUES
(1, '2017-08-21', 1503252000, '2017-08-21 10:07:24', '2017-08-21 10:07:24'),
(2, '2017-08-01', 1501524000, '2017-08-21 11:18:03', '2017-08-21 11:18:03'),
(3, '2017-08-22', 1503338400, '2017-08-22 07:51:43', '2017-08-22 07:51:43'),
(4, '2017-08-23', 1503424800, '2017-08-23 07:01:18', '2017-08-23 07:01:18'),
(5, '2017-08-24', 1503511200, '2017-08-24 06:24:11', '2017-08-24 06:24:11'),
(6, '2017-08-25', 1503597600, '2017-08-25 05:43:14', '2017-08-25 05:43:14'),
(7, '2017-08-06', 1501956000, '2017-08-25 10:20:28', '2017-08-25 10:20:28'),
(8, '2017-08-08', 1502128800, '2017-08-25 10:37:50', '2017-08-25 10:37:50'),
(9, '2017-08-28', 1503856800, '2017-08-28 06:11:33', '2017-08-28 06:11:33'),
(10, '2017-08-15', 1502733600, '2017-08-28 06:12:33', '2017-08-28 06:12:33'),
(11, '2017-08-30', 1504029600, '2017-08-30 08:42:29', '2017-08-30 08:42:29');

-- --------------------------------------------------------

--
-- Table structure for table `atnd_infos`
--

CREATE TABLE `atnd_infos` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `routine_id` int(10) UNSIGNED NOT NULL,
  `date_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `atnd_infos`
--

INSERT INTO `atnd_infos` (`id`, `school_id`, `user_id`, `routine_id`, `date_id`, `status`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 43, 16, 1, 1, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(2, 5, 44, 16, 1, 1, 11, 11, '2017-08-13 11:07:09', '2017-08-21 11:07:09'),
(3, 5, 45, 16, 1, 1, 11, 11, '2017-08-13 11:07:09', '2017-08-21 11:07:09'),
(4, 5, 46, 16, 1, 1, 11, 11, '2017-08-13 11:07:09', '2017-08-21 11:07:09'),
(5, 5, 47, 16, 1, 1, 11, 11, '2017-08-13 11:07:09', '2017-08-21 11:07:09'),
(6, 5, 48, 16, 1, 1, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(7, 5, 49, 16, 1, 1, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(8, 5, 50, 16, 1, 0, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(9, 5, 51, 16, 1, 1, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(10, 5, 52, 16, 1, 1, 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(11, 5, 13, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(12, 5, 14, 1, 1, 0, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(13, 5, 15, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(14, 5, 16, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(15, 5, 17, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(16, 5, 18, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(17, 5, 19, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(18, 5, 20, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(19, 5, 21, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(20, 5, 22, 1, 1, 1, 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(21, 5, 13, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(22, 5, 14, 7, 1, 1, 6, 6, '2017-08-20 11:15:47', '2017-08-21 11:15:47'),
(23, 5, 15, 7, 1, 1, 6, 6, '2017-08-20 11:15:47', '2017-08-21 11:15:47'),
(24, 5, 16, 7, 1, 1, 6, 6, '2017-08-20 11:15:47', '2017-08-21 11:15:47'),
(25, 5, 17, 7, 1, 1, 6, 6, '2017-08-20 11:15:47', '2017-08-21 11:15:47'),
(26, 5, 18, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(27, 5, 19, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(28, 5, 20, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(29, 5, 21, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(30, 5, 22, 7, 1, 1, 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(31, 5, 13, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(32, 5, 14, 31, 3, 1, 11, 11, '2017-05-15 08:02:43', '2017-08-22 08:02:43'),
(33, 5, 15, 31, 3, 1, 11, 11, '2017-06-13 08:02:43', '2017-08-22 08:02:43'),
(34, 5, 16, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(35, 5, 17, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(36, 5, 18, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(37, 5, 19, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(38, 5, 20, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(39, 5, 21, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(40, 5, 22, 31, 3, 1, 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(41, 5, 43, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(42, 5, 44, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(43, 5, 45, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(44, 5, 46, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(45, 5, 47, 34, 3, 1, 5, 5, '2017-05-01 08:51:12', '2017-08-22 08:51:12'),
(46, 5, 48, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(47, 5, 49, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(48, 5, 50, 34, 3, 1, 5, 5, '2017-05-22 08:51:12', '2017-08-22 08:51:12'),
(49, 5, 51, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(50, 5, 52, 34, 3, 1, 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(51, 57, 58, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(52, 57, 59, 57, 3, 1, 57, 57, '2017-06-12 09:57:30', '2017-08-22 09:57:30'),
(53, 57, 60, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(54, 57, 61, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(55, 57, 62, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(56, 57, 63, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(57, 57, 64, 57, 3, 1, 57, 57, '2017-06-12 09:57:30', '2017-08-22 09:57:30'),
(58, 57, 65, 57, 3, 0, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(59, 57, 66, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(60, 57, 67, 57, 3, 1, 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(61, 5, 23, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(62, 5, 24, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(63, 5, 25, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(64, 5, 26, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(65, 5, 27, 38, 3, 1, 11, 11, '2017-05-08 15:15:37', '2017-08-22 15:15:37'),
(66, 5, 28, 38, 3, 1, 11, 11, '2017-04-09 15:15:37', '2017-08-22 15:15:37'),
(67, 5, 29, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(68, 5, 30, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(69, 5, 31, 38, 3, 1, 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(70, 5, 32, 38, 3, 1, 11, 11, '2017-05-07 15:15:37', '2017-08-22 15:15:37'),
(71, 5, 33, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(72, 5, 34, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(73, 5, 35, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(74, 5, 36, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(75, 5, 37, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(76, 5, 38, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(77, 5, 39, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(78, 5, 40, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(79, 5, 41, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(80, 5, 42, 81, 4, 1, 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(81, 5, 13, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(82, 5, 14, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(83, 5, 15, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 14:36:01'),
(84, 5, 16, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 14:36:01'),
(85, 5, 17, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 14:36:01'),
(86, 5, 18, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(87, 5, 19, 86, 4, 0, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(88, 5, 20, 86, 4, 0, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(89, 5, 21, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(90, 5, 22, 86, 4, 1, 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(91, 54, 76, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(92, 54, 115, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(93, 54, 77, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(94, 54, 116, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(95, 54, 78, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(96, 54, 117, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(97, 54, 79, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(98, 54, 118, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(99, 54, 80, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(100, 54, 119, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(101, 54, 81, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(102, 54, 120, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(103, 54, 82, 100, 4, 0, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(104, 54, 121, 100, 4, 0, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(105, 54, 83, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(106, 54, 122, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(107, 54, 84, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(108, 54, 123, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(109, 54, 85, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(110, 54, 124, 100, 4, 1, 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(111, 57, 125, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(112, 57, 126, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(113, 57, 127, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(114, 57, 128, 116, 4, 0, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:49'),
(115, 57, 129, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(116, 57, 130, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(117, 57, 131, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(118, 57, 132, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(119, 57, 133, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(120, 57, 134, 116, 4, 1, 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(121, 57, 135, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(122, 57, 136, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(123, 57, 137, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(124, 57, 138, 117, 4, 1, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(125, 57, 139, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(126, 57, 140, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(127, 57, 141, 117, 4, 1, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(128, 57, 142, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(129, 57, 143, 117, 4, 0, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(130, 57, 144, 117, 4, 1, 57, 57, '2017-08-23 14:29:20', '2017-08-23 14:29:20'),
(131, 5, 33, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(132, 5, 34, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(133, 5, 35, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(134, 5, 36, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(135, 5, 37, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(136, 5, 38, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(137, 5, 39, 95, 4, 0, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(138, 5, 40, 95, 4, 0, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(139, 5, 41, 95, 4, 0, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(140, 5, 42, 95, 4, 1, 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(141, 5, 33, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(142, 5, 34, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(143, 5, 35, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(144, 5, 36, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(145, 5, 37, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(146, 5, 38, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(147, 5, 39, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(148, 5, 40, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(149, 5, 41, 129, 5, 0, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(150, 5, 42, 129, 5, 1, 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(151, 5, 13, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(152, 5, 14, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(153, 5, 15, 134, 5, 0, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(154, 5, 16, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(155, 5, 17, 134, 5, 0, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(156, 5, 18, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(157, 5, 19, 134, 5, 0, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(158, 5, 20, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(159, 5, 21, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(160, 5, 22, 134, 5, 1, 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(161, 54, 76, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(162, 54, 115, 148, 5, 0, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:05:58'),
(163, 54, 77, 148, 5, 0, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:05:58'),
(164, 54, 116, 148, 5, 0, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:05:58'),
(165, 54, 78, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(166, 54, 117, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(167, 54, 79, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(168, 54, 118, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(169, 54, 80, 148, 5, 0, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:05:43'),
(170, 54, 119, 148, 5, 0, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:05:43'),
(171, 54, 81, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(172, 54, 120, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(173, 54, 82, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(174, 54, 121, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(175, 54, 83, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(176, 54, 122, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(177, 54, 84, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(178, 54, 123, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(179, 54, 85, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(180, 54, 124, 148, 5, 1, 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(181, 5, 23, 135, 5, 0, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(182, 5, 24, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(183, 5, 25, 135, 5, 0, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(184, 5, 26, 135, 5, 0, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(185, 5, 27, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(186, 5, 28, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(187, 5, 29, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(188, 5, 30, 135, 5, 0, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(189, 5, 31, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(190, 5, 32, 135, 5, 1, 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(191, 5, 145, 158, 6, 0, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(192, 5, 146, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(193, 5, 147, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(194, 5, 148, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(195, 5, 149, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(196, 5, 150, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(197, 5, 151, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(198, 5, 152, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(199, 5, 153, 158, 6, 0, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(200, 5, 154, 158, 6, 1, 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(201, 5, 13, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(202, 5, 14, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(203, 5, 15, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(204, 5, 16, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(205, 5, 17, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(206, 5, 18, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(207, 5, 19, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(208, 5, 20, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(209, 5, 21, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(210, 5, 22, 153, 6, 1, 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(211, 5, 13, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(212, 5, 14, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(213, 5, 15, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(214, 5, 16, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(215, 5, 17, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(216, 5, 18, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(217, 5, 19, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(218, 5, 20, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(219, 5, 21, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(220, 5, 22, 160, 6, 1, 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(221, 5, 43, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(222, 5, 44, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(223, 5, 45, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(224, 5, 46, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(225, 5, 47, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(226, 5, 48, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(227, 5, 49, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(228, 5, 50, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(229, 5, 51, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(230, 5, 52, 177, 6, 1, 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(231, 5, 23, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(232, 5, 24, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(233, 5, 25, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(234, 5, 26, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(235, 5, 27, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(236, 5, 28, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(237, 5, 29, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(238, 5, 30, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(239, 5, 31, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09'),
(240, 5, 32, 2, 9, 1, 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE `courses` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aliase` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`id`, `school_id`, `title`, `aliase`, `order`, `created_at`, `updated_at`) VALUES
(1, 1, 'Six', 'six', 6, NULL, NULL),
(2, 1, 'Seven', 'seven', 7, NULL, NULL),
(3, 1, 'Eight', 'eight', 8, NULL, NULL),
(4, 1, 'Nine', 'nine', 9, NULL, NULL),
(5, 1, 'Ten', 'ten', 10, NULL, NULL),
(6, 5, 'Eleven', 'Eleven', NULL, '2017-08-21 14:08:21', '2017-08-21 14:40:46');

-- --------------------------------------------------------

--
-- Table structure for table `coursestudents`
--

CREATE TABLE `coursestudents` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `session` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED DEFAULT NULL,
  `total_student` mediumint(8) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `distadmins`
--

CREATE TABLE `distadmins` (
  `id` int(10) UNSIGNED NOT NULL,
  `district_id` mediumint(8) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `photo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `distadmins`
--

INSERT INTO `distadmins` (`id`, `district_id`, `user_id`, `photo`, `created_at`, `updated_at`) VALUES
(2, 1, 75, NULL, '2017-08-22 11:53:25', '2017-08-22 11:53:25'),
(3, 2, 86, NULL, '2017-08-22 13:10:09', '2017-08-22 13:10:09');

-- --------------------------------------------------------

--
-- Table structure for table `districts`
--

CREATE TABLE `districts` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `districts`
--

INSERT INTO `districts` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Dhaka', '2017-08-21 10:12:08', '2017-08-21 10:12:08'),
(2, 'Gazipur', '2017-08-21 10:12:15', '2017-08-21 10:12:15');

-- --------------------------------------------------------

--
-- Table structure for table `guardians`
--

CREATE TABLE `guardians` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `address` longtext COLLATE utf8mb4_unicode_ci,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `guardians`
--

INSERT INTO `guardians` (`id`, `school_id`, `user_id`, `mobile`, `address`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 87, '24234242', 'anywhere in bangladesh', 5, 5, '2017-08-22 13:19:48', '2017-08-22 13:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `homeworks`
--

CREATE TABLE `homeworks` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `routine_id` int(10) UNSIGNED NOT NULL,
  `date_id` int(10) UNSIGNED NOT NULL,
  `status` tinyint(1) NOT NULL,
  `subject_id` int(10) UNSIGNED NOT NULL,
  `home_work` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `homeworks`
--

INSERT INTO `homeworks` (`id`, `school_id`, `routine_id`, `date_id`, `status`, `subject_id`, `home_work`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 16, 1, 1, 44, 'Make a bike using paper', 11, 11, '2017-08-21 11:07:09', '2017-08-21 11:07:09'),
(2, 5, 1, 1, 1, 1, 'Make a mobile using wood', 6, 6, '2017-08-21 11:15:00', '2017-08-21 11:15:00'),
(3, 5, 7, 1, 1, 7, 'Draw a machine gun', 6, 6, '2017-08-21 11:15:47', '2017-08-21 11:15:47'),
(4, 5, 31, 3, 1, 1, 'বাংলাদেশ এর বন্যা সমস্যা নিয়ে একটি রচনা লিখ।', 11, 11, '2017-08-22 08:02:43', '2017-08-22 08:02:43'),
(5, 5, 34, 3, 1, 54, 'this homework by head master', 5, 5, '2017-08-22 08:51:12', '2017-08-22 08:51:12'),
(6, 57, 57, 3, 1, 1, 'this is homework by head master of Gazipur Shaeen Caded School', 57, 57, '2017-08-22 09:57:30', '2017-08-22 09:57:30'),
(7, 5, 38, 3, 1, 5, 'homework', 11, 11, '2017-08-22 15:15:37', '2017-08-22 15:15:37'),
(8, 5, 81, 4, 1, 27, 'this is homework by Abul teachers', 11, 11, '2017-08-23 07:44:35', '2017-08-23 07:44:35'),
(9, 5, 86, 4, 1, 2, '5 students are absent', 11, 11, '2017-08-23 11:36:49', '2017-08-23 11:36:49'),
(10, 54, 100, 4, 1, 1, 'good.', 102, 102, '2017-08-23 13:54:58', '2017-08-23 13:54:58'),
(11, 57, 116, 4, 1, 20, 'good', 57, 57, '2017-08-23 14:27:25', '2017-08-23 14:27:25'),
(12, 5, 95, 4, 1, 22, 'homeowrk', 5, 5, '2017-08-23 14:53:46', '2017-08-23 14:53:46'),
(13, 5, 129, 5, 1, 23, 'Draw a bike', 5, 5, '2017-08-24 06:44:14', '2017-08-24 06:44:14'),
(14, 5, 134, 5, 1, 5, 'homwork', 6, 6, '2017-08-24 07:21:53', '2017-08-24 07:21:53'),
(15, 54, 148, 5, 1, 2, 'good', 104, 104, '2017-08-24 10:04:49', '2017-08-24 10:04:49'),
(16, 5, 135, 5, 1, 1, 'done', 5, 5, '2017-08-24 14:39:20', '2017-08-24 14:39:20'),
(17, 5, 158, 6, 1, 84, 'this is homework by head master', 5, 5, '2017-08-25 05:55:12', '2017-08-25 05:55:12'),
(18, 5, 153, 6, 1, 4, 'good', 11, 11, '2017-08-25 11:15:21', '2017-08-25 11:15:21'),
(19, 5, 160, 6, 1, 1, 'bangla', 11, 11, '2017-08-25 11:16:07', '2017-08-25 11:16:07'),
(20, 5, 177, 6, 1, 46, 'make an assignment based on your own idea.', 11, 11, '2017-08-25 11:17:12', '2017-08-25 11:17:12'),
(21, 5, 2, 9, 1, 5, 'Monday assignment', 5, 5, '2017-08-28 06:14:09', '2017-08-28 06:14:09');

-- --------------------------------------------------------

--
-- Table structure for table `institutes`
--

CREATE TABLE `institutes` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `en_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `alise` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clg_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_eiin` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `directory` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_vill` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_post` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_upazilla` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_zilla` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ins_mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `principle_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principle_img` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principle_phone` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `principle_email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `website` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `youtube` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `district_id` mediumint(9) DEFAULT NULL,
  `logo` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `institutes`
--

INSERT INTO `institutes` (`id`, `name`, `en_name`, `alise`, `ins_code`, `clg_code`, `ins_eiin`, `directory`, `ins_vill`, `ins_post`, `ins_upazilla`, `ins_zilla`, `ins_mobile`, `email`, `principle_name`, `principle_img`, `principle_phone`, `principle_email`, `website`, `facebook`, `twitter`, `youtube`, `user_id`, `district_id`, `logo`, `created_at`, `updated_at`) VALUES
(1, 'Northern University, Bangladesh', NULL, 'NUB', NULL, NULL, 'nub', 'institute/nub', 'Bananai', '1213', 'Banani', NULL, '01700 000000', 'nub@ac.bd', 'Khandakar  Hossain', 'institute/nub/36857abutaleb.png', '0170000000', 'principle@nub.ac.bd', NULL, NULL, NULL, NULL, 5, 1, 'institute/nub/50005logo_nub.png', '2017-08-21 10:22:03', '2017-08-21 12:41:02'),
(2, 'Brain Station-23', NULL, NULL, NULL, NULL, 'bs', 'institute/bs', NULL, NULL, NULL, NULL, NULL, 'bs@23.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 1, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(3, 'Philipnagar Secondary School', NULL, NULL, NULL, NULL, 'pss', 'institute/pss', NULL, NULL, NULL, NULL, NULL, 'info@pss.com', 'Philipnagar Principle', NULL, '242342', 'principle@pss.com', NULL, NULL, NULL, NULL, 55, 1, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(4, 'Hosenabad Technical Collage', NULL, 'Hosenabad Technical Collage', NULL, NULL, 'HTC', 'institute/HTC', NULL, NULL, NULL, NULL, NULL, 'info@htc.com', 'HTC Principle', NULL, '234234242', 'principle@htc.com', NULL, NULL, NULL, NULL, 56, 1, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(5, 'Gazipur shaheen Cadet school', NULL, NULL, NULL, NULL, 'gscs', 'institute/gscs', NULL, NULL, NULL, NULL, '234213123213', 'info@gscs.com', 'Abdul Kuddus Rahman', NULL, '203482304', 'headmaster@gscs.com', NULL, NULL, NULL, NULL, 57, 2, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(72, '2014_10_12_000000_create_users_table', 1),
(73, '2014_10_12_100000_create_password_resets_table', 1),
(74, '2017_07_03_103521_entrust_setup_tables', 1),
(75, '2017_07_04_084902_create_institutes_table', 1),
(76, '2017_07_05_070144_create_courses_table', 1),
(77, '2017_07_05_084456_create_sections_table', 1),
(78, '2017_07_05_102000_create_subjects_table', 1),
(79, '2017_07_05_125218_create_teachers_table', 1),
(80, '2017_07_09_115809_create_students_table', 1),
(81, '2017_07_09_130147_create_sessions_table', 1),
(82, '2017_07_10_141150_create_coursestudents_table', 1),
(83, '2017_07_11_115816_create_routinedays_table', 1),
(84, '2017_07_11_140500_create_routineperiods_table', 1),
(85, '2017_07_12_101417_create_routins_table', 1),
(86, '2017_07_17_072043_atnd_date', 1),
(87, '2017_07_17_090240_create_atnd_infos_table', 1),
(88, '2017_07_20_163602_create_homeworks_table', 1),
(89, '2017_08_03_190000_create_guardians_table', 1),
(90, '2017_08_06_200438_guardianstudent', 1),
(91, '2017_08_08_145746_create_districts_table', 1),
(92, '2017_08_09_160158_create_distadmins_table', 1),
(93, '2017_08_10_200737_create_notices_table', 1),
(94, '2017_08_16_173124_create_remarks_table', 1),
(95, '2017_08_17_144828_create_teacheractivities_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `notices`
--

CREATE TABLE `notices` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `notices`
--

INSERT INTO `notices` (`id`, `school_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 'This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday This is notice for holiday', '2017-08-21 11:11:44', '2017-08-21 11:11:44'),
(2, 5, 'another good short notice', '2017-08-21 11:12:03', '2017-08-21 11:12:03');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'create-school', 'School Creation', 'Can Able To Create School', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `permission_role`
--

CREATE TABLE `permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permission_role`
--

INSERT INTO `permission_role` (`permission_id`, `role_id`) VALUES
(1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `remarks`
--

CREATE TABLE `remarks` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `district_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `remarks`
--

INSERT INTO `remarks` (`id`, `school_id`, `district_id`, `content`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'This is remarks for Northern University, Bangladesh', '2017-08-21 11:10:47', '2017-08-21 11:10:47'),
(2, 5, 1, 'Thank you Northern University, Bangladesh for your quality of education', '2017-08-21 11:11:08', '2017-08-21 11:11:08'),
(3, 5, 1, 'best of luck Northern University, Bangladesh', '2017-08-21 11:11:15', '2017-08-21 11:11:15'),
(4, 54, 1, 'can you please give me the contact number of Brain Station-23?', '2017-08-25 05:59:07', '2017-08-25 05:59:07');

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `display_name`, `description`, `created_at`, `updated_at`) VALUES
(1, 'owner', 'Owner', 'Owner Of The Site', NULL, NULL),
(2, 'school', 'School', 'Admin of School', NULL, NULL),
(3, 'teacher', 'Teacher', 'Teachers of the Institution', NULL, NULL),
(4, 'student', 'Student', 'Students of the Websites', NULL, NULL),
(5, 'parent', 'Parent', 'Student Parent', NULL, NULL),
(6, 'distadmin', 'District Admin', 'District Admin', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `role_user`
--

CREATE TABLE `role_user` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_user`
--

INSERT INTO `role_user` (`user_id`, `role_id`) VALUES
(1, 1),
(5, 2),
(5, 3),
(6, 3),
(7, 3),
(8, 3),
(9, 3),
(10, 3),
(11, 3),
(12, 3),
(13, 4),
(14, 4),
(15, 4),
(16, 4),
(17, 4),
(18, 4),
(19, 4),
(20, 4),
(21, 4),
(22, 4),
(23, 4),
(24, 4),
(25, 4),
(26, 4),
(27, 4),
(28, 4),
(29, 4),
(30, 4),
(31, 4),
(32, 4),
(33, 4),
(34, 4),
(35, 4),
(36, 4),
(37, 4),
(38, 4),
(39, 4),
(40, 4),
(41, 4),
(42, 4),
(43, 4),
(44, 4),
(45, 4),
(46, 4),
(47, 4),
(48, 4),
(49, 4),
(50, 4),
(51, 4),
(52, 4),
(54, 2),
(55, 2),
(55, 3),
(56, 2),
(56, 3),
(57, 2),
(57, 3),
(58, 4),
(59, 4),
(60, 4),
(61, 4),
(62, 4),
(63, 4),
(64, 4),
(65, 4),
(66, 4),
(67, 4),
(68, 3),
(69, 3),
(70, 3),
(71, 3),
(72, 3),
(73, 3),
(74, 3),
(75, 6),
(76, 4),
(77, 4),
(78, 4),
(79, 4),
(80, 4),
(81, 4),
(82, 4),
(83, 4),
(84, 4),
(85, 4),
(86, 6),
(87, 5),
(88, 3),
(89, 3),
(90, 3),
(91, 3),
(92, 3),
(93, 3),
(94, 3),
(95, 3),
(96, 3),
(97, 3),
(98, 3),
(99, 3),
(100, 3),
(101, 3),
(102, 3),
(103, 3),
(104, 3),
(105, 3),
(106, 3),
(107, 3),
(108, 3),
(109, 3),
(110, 3),
(111, 3),
(112, 3),
(113, 3),
(114, 3),
(115, 4),
(116, 4),
(117, 4),
(118, 4),
(119, 4),
(120, 4),
(121, 4),
(122, 4),
(123, 4),
(124, 4),
(125, 4),
(126, 4),
(127, 4),
(128, 4),
(129, 4),
(130, 4),
(131, 4),
(132, 4),
(133, 4),
(134, 4),
(135, 4),
(136, 4),
(137, 4),
(138, 4),
(139, 4),
(140, 4),
(141, 4),
(142, 4),
(143, 4),
(144, 4),
(145, 4),
(146, 4),
(147, 4),
(148, 4),
(149, 4),
(150, 4),
(151, 4),
(152, 4),
(153, 4),
(154, 4);

-- --------------------------------------------------------

--
-- Table structure for table `routinedays`
--

CREATE TABLE `routinedays` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `session` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` mediumint(8) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routinedays`
--

INSERT INTO `routinedays` (`id`, `school_id`, `session`, `title`, `order`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'Monday', 3, '2017-08-21 10:52:30', '2017-08-21 10:52:30'),
(2, 5, 1, 'Tuesday', 4, '2017-08-21 10:52:40', '2017-08-21 10:52:40'),
(3, 57, 1, 'Tuesday', 4, '2017-08-22 09:31:21', '2017-08-22 09:31:21'),
(4, 5, 1, 'Wednesday', 5, '2017-08-23 07:35:47', '2017-08-23 07:35:47'),
(5, 54, 1, 'Wednesday', 5, '2017-08-23 13:50:44', '2017-08-23 13:50:44'),
(6, 57, 1, 'Wednesday', 5, '2017-08-23 14:25:08', '2017-08-23 14:25:08'),
(7, 5, 1, 'Thursday', 6, '2017-08-24 06:41:19', '2017-08-24 06:41:19'),
(8, 54, 1, 'Thursday', 6, '2017-08-24 09:58:06', '2017-08-24 09:58:06'),
(9, 5, 1, 'Friday', 7, '2017-08-25 05:45:22', '2017-08-25 05:45:22');

-- --------------------------------------------------------

--
-- Table structure for table `routineperiods`
--

CREATE TABLE `routineperiods` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `session` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `start_time` time NOT NULL,
  `end_time` time NOT NULL,
  `start_str` int(10) UNSIGNED NOT NULL,
  `end_str` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routineperiods`
--

INSERT INTO `routineperiods` (`id`, `school_id`, `session`, `title`, `start_time`, `end_time`, `start_str`, `end_str`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'First Period', '10:00:00', '11:55:00', 1503288000, 1503294900, '2017-08-21 10:56:09', '2017-08-21 10:56:09'),
(2, 5, 1, 'Second Period', '12:01:00', '17:00:00', 1503295260, 1503313200, '2017-08-21 10:56:43', '2017-08-21 10:56:43'),
(3, 5, 1, '3rd Period', '17:00:00', '20:00:00', 1503313200, 1503324000, '2017-08-21 10:57:03', '2017-08-21 10:57:03'),
(4, 5, 1, '4th period', '20:00:00', '22:00:00', 1503324000, 1503331200, '2017-08-21 10:58:08', '2017-08-21 10:58:08'),
(5, 57, 1, 'First Period', '12:10:00', '17:00:00', 1503382200, 1503399600, '2017-08-22 09:32:57', '2017-08-22 09:32:57'),
(6, 57, 1, 'Second Period', '17:00:00', '20:00:00', 1503399600, 1503410400, '2017-08-22 09:33:14', '2017-08-22 09:33:14'),
(7, 57, 1, 'Third Period', '20:00:00', '23:00:00', 1503410400, 1503421200, '2017-08-22 09:33:35', '2017-08-22 09:33:35'),
(8, 54, 1, 'First Period', '12:10:00', '22:50:00', 1503468600, 1503507000, '2017-08-23 13:50:01', '2017-08-23 13:50:01'),
(16, 5, 1, 'xsesf', '15:25:00', '16:25:00', 1503653100, 1503656700, '2017-08-25 09:24:18', '2017-08-25 09:24:18');

-- --------------------------------------------------------

--
-- Table structure for table `routins`
--

CREATE TABLE `routins` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `session` int(10) UNSIGNED NOT NULL,
  `day_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `period_id` int(10) UNSIGNED NOT NULL,
  `teacher_id` int(10) UNSIGNED DEFAULT NULL,
  `subject_id` int(10) UNSIGNED DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `routins`
--

INSERT INTO `routins` (`id`, `school_id`, `session`, `day_id`, `course_id`, `section_id`, `period_id`, `teacher_id`, `subject_id`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 1, 1, 1, 1, 6, 1, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(2, 5, 1, 1, 1, 6, 1, 5, 5, 5, 5, '2017-08-21 11:01:43', '2017-08-28 06:13:10'),
(3, 5, 1, 1, 2, 2, 1, 8, 26, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(4, 5, 1, 1, 3, 3, 1, 10, 42, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(5, 5, 1, 1, 4, 4, 1, 8, 59, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(6, 5, 1, 1, 5, 5, 1, 8, 91, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(7, 5, 1, 1, 1, 1, 2, 6, 7, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(8, 5, 1, 1, 1, 6, 2, 9, 4, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(9, 5, 1, 1, 2, 2, 2, 9, 35, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(10, 5, 1, 1, 3, 3, 2, 9, 41, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(11, 5, 1, 1, 4, 4, 2, 10, 60, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(12, 5, 1, 1, 5, 5, 2, 9, 87, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(13, 5, 1, 1, 1, 1, 3, 10, 3, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(14, 5, 1, 1, 1, 6, 3, 10, 5, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(15, 5, 1, 1, 2, 2, 3, 9, 35, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(16, 5, 1, 1, 3, 3, 3, 11, 44, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(17, 5, 1, 1, 4, 4, 3, 9, 62, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(18, 5, 1, 1, 5, 5, 3, 7, 86, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(19, 5, 1, 1, 1, 1, 4, 10, 4, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(20, 5, 1, 1, 1, 6, 4, 10, 5, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(21, 5, 1, 1, 2, 2, 4, 8, 31, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(22, 5, 1, 1, 3, 3, 4, 10, 43, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(23, 5, 1, 1, 4, 4, 4, 12, 62, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(24, 5, 1, 1, 5, 5, 4, 9, 80, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(25, 5, 1, 2, 1, 1, 1, 6, 2, 5, 5, '2017-08-21 11:01:43', '2017-08-22 08:01:05'),
(26, 5, 1, 2, 1, 6, 1, NULL, NULL, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(27, 5, 1, 2, 2, 2, 1, NULL, NULL, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(28, 5, 1, 2, 3, 3, 1, NULL, NULL, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(29, 5, 1, 2, 4, 4, 1, NULL, NULL, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(30, 5, 1, 2, 5, 5, 1, NULL, NULL, 5, 5, '2017-08-21 11:01:43', '2017-08-21 11:01:43'),
(31, 5, 1, 2, 1, 1, 2, 11, 1, 5, 5, '2017-08-21 11:01:43', '2017-08-22 08:01:45'),
(32, 5, 1, 2, 1, 6, 2, 9, 6, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(33, 5, 1, 2, 2, 2, 2, 8, 21, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(34, 5, 1, 2, 3, 3, 2, 5, 54, 5, 5, '2017-08-21 11:01:43', '2017-08-22 08:50:30'),
(35, 5, 1, 2, 4, 4, 2, 9, 67, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(36, 5, 1, 2, 5, 5, 2, 10, 82, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(37, 5, 1, 2, 1, 1, 3, 8, 4, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(38, 5, 1, 2, 1, 6, 3, 11, 5, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(39, 5, 1, 2, 2, 2, 3, 8, 28, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(40, 5, 1, 2, 3, 3, 3, 12, 43, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(41, 5, 1, 2, 4, 4, 3, 11, 64, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(42, 5, 1, 2, 5, 5, 3, 8, 81, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(43, 5, 1, 2, 1, 1, 4, 9, 10, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(44, 5, 1, 2, 1, 6, 4, 11, 3, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(45, 5, 1, 2, 2, 2, 4, 11, 26, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(46, 5, 1, 2, 3, 3, 4, 10, 41, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(47, 5, 1, 2, 4, 4, 4, 10, 61, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(48, 5, 1, 2, 5, 5, 4, 8, 87, 5, 5, '2017-08-21 11:01:43', '2017-08-22 07:55:23'),
(49, 5, 1, 1, 6, 12, 1, NULL, NULL, 5, 5, '2017-08-21 14:45:59', '2017-08-21 14:45:59'),
(50, 5, 1, 1, 6, 12, 2, NULL, NULL, 5, 5, '2017-08-21 14:45:59', '2017-08-21 14:45:59'),
(51, 5, 1, 1, 6, 12, 3, NULL, NULL, 5, 5, '2017-08-21 14:45:59', '2017-08-21 14:45:59'),
(52, 5, 1, 1, 6, 12, 4, 6, 96, 5, 5, '2017-08-21 14:45:59', '2017-08-21 14:45:59'),
(53, 5, 1, 2, 6, 12, 1, NULL, NULL, 5, 5, '2017-08-21 14:45:59', '2017-08-21 14:45:59'),
(54, 5, 1, 2, 6, 12, 2, 9, 96, 5, 5, '2017-08-21 14:45:59', '2017-08-22 07:55:23'),
(55, 5, 1, 2, 6, 12, 3, 8, 96, 5, 5, '2017-08-21 14:45:59', '2017-08-22 07:55:23'),
(56, 5, 1, 2, 6, 12, 4, 9, 96, 5, 5, '2017-08-21 14:45:59', '2017-08-22 07:55:23'),
(57, 57, 1, 3, 1, 23, 5, 57, 1, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:39:35'),
(58, 57, 1, 3, 2, 24, 5, 71, 22, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:39:35'),
(59, 57, 1, 3, 3, 25, 5, 69, 48, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:39:35'),
(60, 57, 1, 3, 4, 26, 5, 69, 69, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:39:35'),
(61, 57, 1, 3, 5, 27, 5, 74, 84, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:39:35'),
(62, 57, 1, 3, 1, 23, 6, 71, 6, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:00'),
(63, 57, 1, 3, 2, 24, 6, 73, 23, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:00'),
(64, 57, 1, 3, 3, 25, 6, 70, 41, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(65, 57, 1, 3, 4, 26, 6, 57, 69, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(66, 57, 1, 3, 5, 27, 6, 70, 84, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(67, 57, 1, 3, 1, 23, 7, 57, 6, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:56:46'),
(68, 57, 1, 3, 2, 24, 7, 72, 22, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(69, 57, 1, 3, 3, 25, 7, 70, 43, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(70, 57, 1, 3, 4, 26, 7, 68, 64, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(71, 57, 1, 3, 5, 27, 7, 72, 85, 57, 57, '2017-08-22 09:39:35', '2017-08-22 09:50:01'),
(72, 5, 1, 4, 1, 1, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(73, 5, 1, 4, 1, 6, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(74, 5, 1, 4, 2, 2, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(75, 5, 1, 4, 3, 3, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(76, 5, 1, 4, 4, 4, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(77, 5, 1, 4, 5, 5, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(78, 5, 1, 4, 6, 12, 1, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(79, 5, 1, 4, 1, 1, 2, 8, 1, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(80, 5, 1, 4, 1, 6, 2, 89, 2, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(81, 5, 1, 4, 2, 2, 2, 11, 27, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:44:03'),
(82, 5, 1, 4, 3, 3, 2, 92, 48, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(83, 5, 1, 4, 4, 4, 2, 92, 66, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(84, 5, 1, 4, 5, 5, 2, 95, 82, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(85, 5, 1, 4, 6, 12, 2, 5, 96, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(86, 5, 1, 4, 1, 1, 3, 11, 2, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:43:30'),
(87, 5, 1, 4, 1, 6, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(88, 5, 1, 4, 2, 2, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(89, 5, 1, 4, 3, 3, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(90, 5, 1, 4, 4, 4, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(91, 5, 1, 4, 5, 5, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(92, 5, 1, 4, 6, 12, 3, NULL, NULL, 5, 5, '2017-08-23 07:37:34', '2017-08-23 07:37:34'),
(93, 5, 1, 4, 1, 1, 4, 7, 1, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(94, 5, 1, 4, 1, 6, 4, 8, 3, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(95, 5, 1, 4, 2, 2, 4, 5, 22, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(96, 5, 1, 4, 3, 3, 4, 12, 45, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(97, 5, 1, 4, 4, 4, 4, 12, 64, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(98, 5, 1, 4, 5, 5, 4, 89, 84, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(99, 5, 1, 4, 6, 12, 4, 89, 96, 5, 5, '2017-08-23 07:37:34', '2017-08-23 14:53:31'),
(100, 54, 1, 5, 1, 7, 8, 102, 1, 54, 54, '2017-08-23 13:52:18', '2017-08-23 13:54:30'),
(101, 54, 1, 5, 2, 8, 8, 102, 23, 54, 54, '2017-08-23 13:52:18', '2017-08-23 13:52:18'),
(102, 54, 1, 5, 3, 9, 8, 106, 47, 54, 54, '2017-08-23 13:52:18', '2017-08-23 13:52:18'),
(103, 54, 1, 5, 4, 10, 8, 109, 67, 54, 54, '2017-08-23 13:52:18', '2017-08-23 13:52:18'),
(104, 54, 1, 5, 5, 11, 8, 113, 83, 54, 54, '2017-08-23 13:52:18', '2017-08-23 13:52:18'),
(105, 57, 1, 6, 1, 23, 5, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(106, 57, 1, 6, 2, 24, 5, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(107, 57, 1, 6, 3, 25, 5, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(108, 57, 1, 6, 4, 26, 5, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(109, 57, 1, 6, 5, 27, 5, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(110, 57, 1, 6, 1, 23, 6, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(111, 57, 1, 6, 2, 24, 6, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(112, 57, 1, 6, 3, 25, 6, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(113, 57, 1, 6, 4, 26, 6, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(114, 57, 1, 6, 5, 27, 6, NULL, NULL, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(115, 57, 1, 6, 1, 23, 7, 70, 4, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(116, 57, 1, 6, 2, 24, 7, 57, 20, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(117, 57, 1, 6, 3, 25, 7, 57, 50, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(118, 57, 1, 6, 4, 26, 7, 71, 69, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(119, 57, 1, 6, 5, 27, 7, 69, 82, 57, 57, '2017-08-23 14:25:46', '2017-08-23 14:25:46'),
(120, 5, 1, 7, 1, 1, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(121, 5, 1, 7, 1, 6, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(122, 5, 1, 7, 2, 2, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(123, 5, 1, 7, 3, 3, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(124, 5, 1, 7, 4, 4, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(125, 5, 1, 7, 5, 5, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(126, 5, 1, 7, 6, 12, 1, NULL, NULL, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(127, 5, 1, 7, 1, 1, 2, 11, 1, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(128, 5, 1, 7, 1, 6, 2, 8, 2, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(129, 5, 1, 7, 2, 2, 2, 5, 23, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(130, 5, 1, 7, 3, 3, 2, 95, 50, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(131, 5, 1, 7, 4, 4, 2, 90, 65, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(132, 5, 1, 7, 5, 5, 2, 97, 86, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(133, 5, 1, 7, 6, 12, 2, 12, 96, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(134, 5, 1, 7, 1, 1, 3, 6, 5, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(135, 5, 1, 7, 1, 6, 3, 5, 1, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(136, 5, 1, 7, 2, 2, 3, 11, 27, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(137, 5, 1, 7, 3, 3, 3, 89, 45, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(138, 5, 1, 7, 4, 4, 3, 90, 67, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(139, 5, 1, 7, 5, 5, 3, 91, 85, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(140, 5, 1, 7, 6, 12, 3, 89, 96, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(141, 5, 1, 7, 1, 1, 4, 91, 8, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(142, 5, 1, 7, 1, 6, 4, 5, 11, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(143, 5, 1, 7, 2, 2, 4, 11, 27, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(144, 5, 1, 7, 3, 3, 4, 12, 39, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(145, 5, 1, 7, 4, 4, 4, 89, 65, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(146, 5, 1, 7, 5, 5, 4, 89, 84, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(147, 5, 1, 7, 6, 12, 4, 90, 96, 5, 5, '2017-08-24 06:43:15', '2017-08-24 06:43:15'),
(148, 54, 1, 8, 1, 7, 8, 104, 2, 54, 54, '2017-08-24 10:02:01', '2017-08-24 10:02:01'),
(149, 54, 1, 8, 2, 8, 8, 104, 21, 54, 54, '2017-08-24 10:02:01', '2017-08-24 10:02:01'),
(150, 54, 1, 8, 3, 9, 8, 106, 45, 54, 54, '2017-08-24 10:02:01', '2017-08-24 10:02:01'),
(151, 54, 1, 8, 4, 10, 8, 103, 66, 54, 54, '2017-08-24 10:02:01', '2017-08-24 10:02:01'),
(152, 54, 1, 8, 5, 11, 8, 109, 83, 54, 54, '2017-08-24 10:02:01', '2017-08-24 10:02:01'),
(153, 5, 1, 9, 1, 1, 1, 11, 4, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(154, 5, 1, 9, 1, 6, 1, 12, 7, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(155, 5, 1, 9, 2, 2, 1, 10, 26, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(156, 5, 1, 9, 3, 3, 1, 10, 44, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(157, 5, 1, 9, 4, 4, 1, 11, 62, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(158, 5, 1, 9, 5, 5, 1, 5, 84, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(159, 5, 1, 9, 6, 12, 1, 90, 96, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(160, 5, 1, 9, 1, 1, 2, 11, 1, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(161, 5, 1, 9, 1, 6, 2, 5, 2, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(162, 5, 1, 9, 2, 2, 2, 93, 22, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(163, 5, 1, 9, 3, 3, 2, 5, 50, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(164, 5, 1, 9, 4, 4, 2, 90, 64, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(165, 5, 1, 9, 5, 5, 2, 91, 86, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(166, 5, 1, 9, 6, 12, 2, 89, 96, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(167, 5, 1, 9, 1, 1, 3, 91, 10, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(168, 5, 1, 9, 1, 6, 3, 12, 11, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(169, 5, 1, 9, 2, 2, 3, 11, 21, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(170, 5, 1, 9, 3, 3, 3, 5, 40, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(171, 5, 1, 9, 4, 4, 3, 5, 62, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(172, 5, 1, 9, 5, 5, 3, 12, 84, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(173, 5, 1, 9, 6, 12, 3, 90, 96, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(174, 5, 1, 9, 1, 1, 4, 91, 11, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(175, 5, 1, 9, 1, 6, 4, 92, 12, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(176, 5, 1, 9, 2, 2, 4, 89, 29, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(177, 5, 1, 9, 3, 3, 4, 11, 46, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(178, 5, 1, 9, 4, 4, 4, 89, 62, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(179, 5, 1, 9, 5, 5, 4, 5, 80, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(180, 5, 1, 9, 6, 12, 4, 90, 96, 5, 5, '2017-08-25 05:48:12', '2017-08-25 05:48:12'),
(181, 5, 1, 1, 1, 1, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(182, 5, 1, 1, 1, 6, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(183, 5, 1, 1, 2, 2, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(184, 5, 1, 1, 3, 3, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(185, 5, 1, 1, 4, 4, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(186, 5, 1, 1, 5, 5, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(187, 5, 1, 1, 6, 12, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(188, 5, 1, 2, 1, 1, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(189, 5, 1, 2, 1, 6, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(190, 5, 1, 2, 2, 2, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(191, 5, 1, 2, 3, 3, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(192, 5, 1, 2, 4, 4, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(193, 5, 1, 2, 5, 5, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(194, 5, 1, 2, 6, 12, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(195, 5, 1, 4, 1, 1, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(196, 5, 1, 4, 1, 6, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(197, 5, 1, 4, 2, 2, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(198, 5, 1, 4, 3, 3, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(199, 5, 1, 4, 4, 4, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(200, 5, 1, 4, 5, 5, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(201, 5, 1, 4, 6, 12, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(202, 5, 1, 7, 1, 1, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(203, 5, 1, 7, 1, 6, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(204, 5, 1, 7, 2, 2, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(205, 5, 1, 7, 3, 3, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(206, 5, 1, 7, 4, 4, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(207, 5, 1, 7, 5, 5, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(208, 5, 1, 7, 6, 12, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(209, 5, 1, 9, 1, 1, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(210, 5, 1, 9, 1, 6, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(211, 5, 1, 9, 2, 2, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(212, 5, 1, 9, 3, 3, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(213, 5, 1, 9, 4, 4, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(214, 5, 1, 9, 5, 5, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11'),
(215, 5, 1, 9, 6, 12, 16, NULL, NULL, 5, 5, '2017-08-28 06:13:11', '2017-08-28 06:13:11');

-- --------------------------------------------------------

--
-- Table structure for table `sections`
--

CREATE TABLE `sections` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `aliase` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sections`
--

INSERT INTO `sections` (`id`, `school_id`, `course_id`, `title`, `aliase`, `order`, `created_at`, `updated_at`) VALUES
(1, 5, 1, 'A', NULL, NULL, '2017-08-21 10:22:03', '2017-08-21 10:22:03'),
(2, 5, 2, 'A', NULL, NULL, '2017-08-21 10:22:03', '2017-08-21 10:22:03'),
(3, 5, 3, 'A', NULL, NULL, '2017-08-21 10:22:03', '2017-08-21 10:22:03'),
(4, 5, 4, 'A', NULL, NULL, '2017-08-21 10:22:03', '2017-08-21 10:22:03'),
(5, 5, 5, 'A', NULL, NULL, '2017-08-21 10:22:03', '2017-08-21 10:22:03'),
(6, 5, 1, 'Yellow', NULL, NULL, '2017-08-21 10:43:12', '2017-08-21 10:43:12'),
(7, 54, 1, 'A', NULL, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(8, 54, 2, 'A', NULL, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(9, 54, 3, 'A', NULL, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(10, 54, 4, 'A', NULL, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(11, 54, 5, 'A', NULL, NULL, '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(12, 5, 6, 'A', NULL, NULL, '2017-08-21 14:40:57', '2017-08-21 14:40:57'),
(13, 55, 1, 'A', NULL, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(14, 55, 2, 'A', NULL, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(15, 55, 3, 'A', NULL, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(16, 55, 4, 'A', NULL, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(17, 55, 5, 'A', NULL, NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(18, 56, 1, 'A', NULL, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(19, 56, 2, 'A', NULL, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(20, 56, 3, 'A', NULL, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(21, 56, 4, 'A', NULL, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(22, 56, 5, 'A', NULL, NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(23, 57, 1, 'A', NULL, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(24, 57, 2, 'A', NULL, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(25, 57, 3, 'A', NULL, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(26, 57, 4, 'A', NULL, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(27, 57, 5, 'A', NULL, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42');

-- --------------------------------------------------------

--
-- Table structure for table `stdgurdians`
--

CREATE TABLE `stdgurdians` (
  `id` int(10) UNSIGNED NOT NULL,
  `guardian_id` int(10) UNSIGNED NOT NULL,
  `student_id` int(10) UNSIGNED NOT NULL,
  `relation_with` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED NOT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `stdgurdians`
--

INSERT INTO `stdgurdians` (`id`, `guardian_id`, `student_id`, `relation_with`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 87, 13, 'Brother', 5, 5, '2017-08-22 13:19:48', '2017-08-22 13:19:48');

-- --------------------------------------------------------

--
-- Table structure for table `students`
--

CREATE TABLE `students` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bn_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` text COLLATE utf8mb4_unicode_ci,
  `blood_group` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `section_id` int(10) UNSIGNED NOT NULL,
  `shift_id` int(10) UNSIGNED DEFAULT NULL,
  `session` int(10) UNSIGNED NOT NULL,
  `roll_no` mediumint(8) UNSIGNED DEFAULT NULL,
  `std_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `third_subject` int(10) UNSIGNED DEFAULT NULL,
  `fourth_subject` int(10) UNSIGNED DEFAULT NULL,
  `status` tinyint(1) DEFAULT NULL,
  `public_status` tinyint(1) DEFAULT NULL,
  `previous_school` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `others` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent` int(10) UNSIGNED DEFAULT NULL,
  `added_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `students`
--

INSERT INTO `students` (`id`, `school_id`, `user_id`, `name`, `bn_name`, `religion`, `blood_group`, `gender`, `dob`, `course_id`, `section_id`, `shift_id`, `session`, `roll_no`, `std_group`, `mobile`, `email`, `image`, `third_subject`, `fourth_subject`, `status`, `public_status`, `previous_school`, `others`, `parent`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 13, 'Hasan', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 1, NULL, '1920886809', 'e@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(2, 5, 14, 'an Habib', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 2, NULL, '1920886809', 'd@gmai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(3, 5, 15, 'Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 3, NULL, '1920886809', 'e@dfsd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(4, 5, 16, 'Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 4, NULL, '1920886809', 'dfs@fsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(5, 5, 17, 'Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 5, NULL, '1920886809', 'fdsfs@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(6, 5, 18, 'Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 6, NULL, '1920886809', 'fsdfsklj@sdfsf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(7, 5, 19, 'Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 7, NULL, '1920886809', 'sdfksdjfl@sfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(8, 5, 20, 'Pisah', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 8, NULL, '1920886809', 'sfdfs@sdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(9, 5, 21, 'Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 9, NULL, '1920886809', 'fsdfsdf@sfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(10, 5, 22, 'Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 1, 1, NULL, 1, 10, NULL, '1920886809', 'sdfsf@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(11, 5, 23, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 1, NULL, '1920886809', 'dfse@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(12, 5, 24, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 2, NULL, '1920886809', 'sdd@gmai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(13, 5, 25, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 3, NULL, '1920886809', 'de@dfsd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(14, 5, 26, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 4, NULL, '1920886809', 'ddfs@fsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(15, 5, 27, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 5, NULL, '1920886809', 'dfdsfs@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(16, 5, 28, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 6, NULL, '1920886809', 'efsdfsklj@sdfsf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(17, 5, 29, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 7, NULL, '1920886809', 'sdefksdjfl@sfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(18, 5, 30, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 8, NULL, '1920886809', 'esfdfs@sdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(19, 5, 31, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 9, NULL, '1920886809', 'fsdefsdf@sfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(20, 5, 32, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 1, 6, NULL, 1, 10, NULL, '1920886809', 'esdfsf@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(21, 5, 33, 'asan', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 1, NULL, '1920886809', 'ef@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:43', '2017-08-21 10:46:43'),
(22, 5, 34, 'san Habib', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 2, NULL, '1920886809', 'dd@gmai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:43', '2017-08-21 10:46:43'),
(23, 5, 35, 'hahidul Islam', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 3, NULL, '1920886809', 'e@dfssfd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(24, 5, 36, 'akir Hosen', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 4, NULL, '1920886809', 'dfs@fsfdsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(25, 5, 37, 'tiqur Khan', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 5, NULL, '1920886809', 'fdsfs@sddsffsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(26, 5, 38, 'idujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 6, NULL, '1920886809', 'fsdfsklj@fdsdfsf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(27, 5, 39, 'afin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 7, NULL, '1920886809', 'sdfksdjfl@sffsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(28, 5, 40, 'isah', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 8, NULL, '1920886809', 'sfdfs@sdfss.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(29, 5, 41, 'izanur Rahaman', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 9, NULL, '1920886809', 'fsdfsdf@sdsfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(30, 5, 42, 'mrul Hasan', NULL, NULL, NULL, NULL, NULL, 2, 2, NULL, 1, 10, NULL, '1920886809', 'sdfsf@sdfssddf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(31, 5, 43, 'sdfMd. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 1, NULL, '1920886809', 'fsde@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(32, 5, 44, 'sd sdfsMd. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 2, NULL, '1920886809', 'sfsdfd@gmai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(33, 5, 45, 's fsd sMd. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 3, NULL, '1920886809', 'sdfsfe@dfsd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(34, 5, 46, 'sfs Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 4, NULL, '1920886809', 'dsdfsffs@fsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(35, 5, 47, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 5, NULL, '1920886809', 'fdsfdsfsfs@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(36, 5, 48, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 6, NULL, '1920886809', 'fssfddsfdfsklj@sdfsf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(37, 5, 49, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 7, NULL, '1920886809', 'sdfsfsdksdjfl@sfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(38, 5, 50, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 8, NULL, '1920886809', 'sfdsfdsfs@sdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(39, 5, 51, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 9, NULL, '1920886809', 'fsdfssdfsfdf@sfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(40, 5, 52, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 3, 3, NULL, 1, 10, NULL, '1920886809', 'sdfsfssf@sdfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 11:06:37', '2017-08-21 11:06:37'),
(41, 57, 58, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 1, NULL, '1920886809', 'e@mdfgail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:31', '2017-08-22 09:30:31'),
(42, 57, 59, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 2, NULL, '1920886809', 'd@gmdgai.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(43, 57, 60, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 3, NULL, '1920886809', 'dge@dfsd.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(44, 57, 61, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 4, NULL, '1920886809', 'dfdfgs@dgfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(45, 57, 62, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 5, NULL, '1920886809', 'fdfgdsfs@sdgddfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(46, 57, 63, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 6, NULL, '1920886809', 'fsddfgfsklj@dgsdfsf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(47, 57, 64, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 7, NULL, '1920886809', 'sdfkdfgsdjfl@sdfgfsdf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(48, 57, 65, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 8, NULL, '1920886809', 'sfdfdgs@sddgfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(49, 57, 66, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 9, NULL, '1920886809', 'fdgsdfsdf@sdgfsdfs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(50, 57, 67, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 1, 23, NULL, 1, 10, NULL, '1920886809', 'sddgfsf@sdfsddgf.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(51, 54, 76, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 1, NULL, '1920886809', 'easdfsf@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(52, 54, 77, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 2, NULL, '1920886809', 'bs117002@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(53, 54, 78, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 3, NULL, '1920886809', 'bs117003@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(54, 54, 79, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 4, NULL, '1920886809', 'bs117004@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(55, 54, 80, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 5, NULL, '1920886809', 'bs117005@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(56, 54, 81, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 6, NULL, '1920886809', 'bs117006@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(57, 54, 82, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 7, NULL, '1920886809', 'bs117007@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(58, 54, 83, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 8, NULL, '1920886809', 'bs117008@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(59, 54, 84, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 9, NULL, '1920886809', 'bs117009@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(60, 54, 85, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 10, NULL, '1920886809', 'bs117010@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-22 13:03:10', '2017-08-22 13:03:10'),
(61, 54, 115, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 1, NULL, '1920886809', 'e@maisdfdsl.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(62, 54, 116, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 2, NULL, '1920886809', 'bs1170fsdfs02@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(63, 54, 117, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 3, NULL, '1920886809', 'bs117fsdfsd003@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(64, 54, 118, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 4, NULL, '1920886809', 'bs117fdsfdsf004@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(65, 54, 119, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 5, NULL, '1920886809', 'bs11sdfsf7005@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(66, 54, 120, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 6, NULL, '1920886809', 'bs11sdfsf7006@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(67, 54, 121, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 7, NULL, '1920886809', 'bs117dsfs007@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(68, 54, 122, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 8, NULL, '1920886809', 'bs117fsdfsd008@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(69, 54, 123, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 9, NULL, '1920886809', 'bs117fsdf009@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(70, 54, 124, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 1, 7, NULL, 1, 10, NULL, '1920886809', 'bs117sdfs010@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(71, 57, 125, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 1, NULL, '1920886809', 'e@masdfsdfsdfil.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(72, 57, 126, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 2, NULL, '1920886809', 'gscs1sdfsd224002@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(73, 57, 127, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 3, NULL, '1920886809', 'gscs1s224003@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(74, 57, 128, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 4, NULL, '1920886809', 'gscs1224004@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(75, 57, 129, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 5, NULL, '1920886809', 'gscs1224005@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(76, 57, 130, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 6, NULL, '1920886809', 'gscs1224006@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(77, 57, 131, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 7, NULL, '1920886809', 'gscs1224007@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(78, 57, 132, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 8, NULL, '1920886809', 'gscs1224008@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(79, 57, 133, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 9, NULL, '1920886809', 'gscs1224009@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(80, 57, 134, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 2, 24, NULL, 1, 10, NULL, '1920886809', 'gscs1224010@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(81, 57, 135, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 1, NULL, '1920886809', 'esdad@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:07', '2017-08-23 14:27:07'),
(82, 57, 136, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 2, NULL, '1920886809', 'gscs1asds325002@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(83, 57, 137, 'Md. Shahidul Islam', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 3, NULL, '1920886809', 'gscs1aasda325003@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(84, 57, 138, 'Md. Jakir Hosen', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 4, NULL, '1920886809', 'gscasdadas1325004@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(85, 57, 139, 'Md. Atiqur Khan', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 5, NULL, '1920886809', 'gscasdas1325005@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(86, 57, 140, 'Md. Saidujjaman Sajib', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 6, NULL, '1920886809', 'gscsasdas1325006@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(87, 57, 141, 'Md. Safin Istiaq Rubel', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 7, NULL, '1920886809', 'gscasdas1325007@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(88, 57, 142, 'Md. Pisah', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 8, NULL, '1920886809', 'gscasdas1325008@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(89, 57, 143, 'Md. Mizanur Rahaman', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 9, NULL, '1920886809', 'gsasdacs1325009@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(90, 57, 144, 'Md. Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 3, 25, NULL, 1, 10, NULL, '1920886809', 'gasdsascs1325010@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(91, 5, 145, 'kuddus', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 1, NULL, '1920886809', 'efsd@mail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(92, 5, 146, 'abib', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 2, NULL, '1920886809', 'nubf155002@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(93, 5, 147, 'hidul Islam', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 3, NULL, '1920886809', 'nub1f55003@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(94, 5, 148, 'Md. Ja', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 4, NULL, '1920886809', 'nub15f5004@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(95, 5, 149, 'han', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 5, NULL, '1920886809', 'nub1f55005@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(96, 5, 150, 'man Sajib', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 6, NULL, '1920886809', 'nub1f55006@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(97, 5, 151, 'stiaq Rubel', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 7, NULL, '1920886809', 'nub1f55007@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(98, 5, 152, 'Pisah', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 8, NULL, '1920886809', 'nub15f5008@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(99, 5, 153, 'haman', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 9, NULL, '1920886809', 'nub15f5009@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(100, 5, 154, 'Kamrul Hasan', NULL, NULL, NULL, NULL, NULL, 5, 5, NULL, 1, 10, NULL, '1920886809', 'nub1f55010@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-25 05:53:36', '2017-08-25 05:53:36');

-- --------------------------------------------------------

--
-- Table structure for table `subjects`
--

CREATE TABLE `subjects` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `course_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `marks` smallint(5) UNSIGNED DEFAULT NULL,
  `mcq` smallint(5) UNSIGNED DEFAULT NULL,
  `written` smallint(5) UNSIGNED DEFAULT NULL,
  `practical` smallint(6) DEFAULT NULL,
  `added_by` int(10) UNSIGNED DEFAULT NULL,
  `update_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subjects`
--

INSERT INTO `subjects` (`id`, `school_id`, `course_id`, `title`, `code`, `sub_group`, `marks`, `mcq`, `written`, `practical`, `added_by`, `update_by`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'বাংলা-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(2, 1, 1, 'বাংলা-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(3, 1, 1, 'ইংরেজী-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(4, 1, 1, 'ইংরেজী-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(5, 1, 1, 'আরবী', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(6, 1, 1, 'গণিত', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(7, 1, 1, 'বাংলাদেশ ও বিশ্বপরিচয়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(8, 1, 1, 'বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 1, 1, 'কৃষি শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(10, 1, 1, 'গার্হস্থ্য বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(11, 1, 1, 'ধর্ম ও নৈতিক শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(12, 1, 1, 'ইসলাম ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(13, 1, 1, 'হিন্দু ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(14, 1, 1, 'বৌদ্ধধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(15, 1, 1, 'শারিরীক শিক্ষা ও স্বাস্থ্য', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(16, 1, 1, 'কর্ম ও জীবনমুখী শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(17, 1, 1, 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(18, 1, 1, 'চারু ও কারুকলা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(19, 1, 1, ' তথ্য ও যোগাযোগ প্রযুক্তি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(20, 1, 2, 'বাংলা-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(21, 1, 2, 'বাংলা-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(22, 1, 2, 'ইংরেজী-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(23, 1, 2, 'ইংরেজী-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(24, 1, 2, 'আরবী', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(25, 1, 2, 'গণিত', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(26, 1, 2, 'বাংলাদেশ ও বিশ্বপরিচয়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(27, 1, 2, 'বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(28, 1, 2, 'কৃষি শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(29, 1, 2, 'গার্হস্থ্য বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(30, 1, 2, 'ধর্ম ও নৈতিক শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(31, 1, 2, 'ইসলাম ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(32, 1, 2, 'হিন্দু ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(33, 1, 2, 'বৌদ্ধধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(34, 1, 2, 'শারিরীক শিক্ষা ও স্বাস্থ্য', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(35, 1, 2, 'কর্ম ও জীবনমুখী শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(36, 1, 2, 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(37, 1, 2, 'চারু ও কারুকলা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(38, 1, 2, ' তথ্য ও যোগাযোগ প্রযুক্তি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(39, 1, 3, 'বাংলা-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(40, 1, 3, 'বাংলা-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(41, 1, 3, 'ইংরেজী-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(42, 1, 3, 'ইংরেজী-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(43, 1, 3, 'আরবী', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(44, 1, 3, 'গণিত', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(45, 1, 3, 'বাংলাদেশ ও বিশ্বপরিচয়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(46, 1, 3, 'বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(47, 1, 3, 'কৃষি শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(48, 1, 3, 'গার্হস্থ্য বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(49, 1, 3, 'ধর্ম ও নৈতিক শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(50, 1, 3, 'ইসলাম ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(51, 1, 3, 'হিন্দু ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(52, 1, 3, 'বৌদ্ধধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(53, 1, 3, 'শারিরীক শিক্ষা ও স্বাস্থ্য', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(54, 1, 3, 'কর্ম ও জীবনমুখী শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(55, 1, 3, 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(56, 1, 3, 'চারু ও কারুকলা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(57, 1, 3, ' তথ্য ও যোগাযোগ প্রযুক্তি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(58, 1, 4, 'বাংলা-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(59, 1, 4, 'বাংলা-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(60, 1, 4, 'ইংরেজী-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(61, 1, 4, 'ইংরেজী-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(62, 1, 4, 'আরবী', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(63, 1, 4, 'গণিত', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(64, 1, 4, 'বাংলাদেশ ও বিশ্বপরিচয়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(65, 1, 4, 'বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(66, 1, 4, 'কৃষি শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(67, 1, 4, 'গার্হস্থ্য বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(68, 1, 4, 'ধর্ম ও নৈতিক শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(69, 1, 4, 'ইসলাম ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(70, 1, 4, 'হিন্দু ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(71, 1, 4, 'বৌদ্ধধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(72, 1, 4, 'শারিরীক শিক্ষা ও স্বাস্থ্য', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(73, 1, 4, 'কর্ম ও জীবনমুখী শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(74, 1, 4, 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(75, 1, 4, 'চারু ও কারুকলা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(76, 1, 4, ' তথ্য ও যোগাযোগ প্রযুক্তি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(77, 1, 5, 'বাংলা-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(78, 1, 5, 'বাংলা-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(79, 1, 5, 'ইংরেজী-১ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(80, 1, 5, 'ইংরেজী-২য়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(81, 1, 5, 'আরবী', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(82, 1, 5, 'গণিত', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(83, 1, 5, 'বাংলাদেশ ও বিশ্বপরিচয়', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(84, 1, 5, 'বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(85, 1, 5, 'কৃষি শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(86, 1, 5, 'গার্হস্থ্য বিজ্ঞান', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(87, 1, 5, 'ধর্ম ও নৈতিক শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(88, 1, 5, 'ইসলাম ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(89, 1, 5, 'হিন্দু ধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(90, 1, 5, 'বৌদ্ধধর্ম', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(91, 1, 5, 'শারিরীক শিক্ষা ও স্বাস্থ্য', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(92, 1, 5, 'কর্ম ও জীবনমুখী শিক্ষা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(93, 1, 5, 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(94, 1, 5, 'চারু ও কারুকলা', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(95, 1, 5, ' তথ্য ও যোগাযোগ প্রযুক্তি', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(96, 5, 6, 'only one subject', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2017-08-21 14:45:18', '2017-08-21 14:45:18');

-- --------------------------------------------------------

--
-- Table structure for table `teachers`
--

CREATE TABLE `teachers` (
  `id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bn_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `religion` text COLLATE utf8mb4_unicode_ci,
  `blood_group` text COLLATE utf8mb4_unicode_ci,
  `designition` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `teach_subject` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nationality` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nid` text COLLATE utf8mb4_unicode_ci,
  `gender` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dob` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `joining_date` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mobile` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mpo_index` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `staff_order` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `education` longtext COLLATE utf8mb4_unicode_ci,
  `training` longtext COLLATE utf8mb4_unicode_ci,
  `ex_school` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `others` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `added_by` int(10) UNSIGNED DEFAULT NULL,
  `updated_by` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teachers`
--

INSERT INTO `teachers` (`id`, `school_id`, `user_id`, `name`, `bn_name`, `religion`, `blood_group`, `designition`, `teach_subject`, `nationality`, `nid`, `gender`, `dob`, `joining_date`, `mobile`, `email`, `mpo_index`, `image`, `staff_order`, `education`, `training`, `ex_school`, `others`, `added_by`, `updated_by`, `created_at`, `updated_at`) VALUES
(1, 5, 6, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1920886809', 'nubmd-adil-hasan@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(2, 5, 7, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-ahasan-habib@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(3, 5, 8, 'Md. Nazmul Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-nazmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(4, 5, 9, 'Md. Amzad Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-amzad-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(5, 5, 10, 'Md. Tanim Hosssain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-tanim-hosssain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(6, 5, 11, 'Abul Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'Male', NULL, NULL, NULL, 'abul@abul.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:40', '2017-08-21 11:03:03'),
(7, 5, 12, 'Hasina Begum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hasina@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-21 10:38:41', '2017-08-21 10:38:41'),
(8, 5, 5, 'Head Master(NUB)', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL),
(9, 57, 57, 'Abdul Kuddus Rahman', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '203482304', 'headmaster@gscs.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(10, 57, 68, 'Roton', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1920886809', 'gscsmd-adil-hasan@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:33', '2017-08-22 09:38:33'),
(11, 57, 69, 'Al amin', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gscsmd-ahasan-habib@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(12, 57, 70, 'Ruhul hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gscsmd-nazmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(13, 57, 71, 'Liton ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gscsmd-amzad-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(14, 57, 72, 'Mohammad ali', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gscsmd-tanim-hosssain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(15, 57, 73, 'Ikramul islam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'gscsmd-tansin-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(16, 57, 74, 'Pavel Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'pavel@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(17, 57, 88, 'New Teacher Added', NULL, 'Islam', 'A+', 'Professor', 'Mathematics', 'Bangladeshi', NULL, 'Male', NULL, NULL, NULL, 'newteacher@gscs.com', NULL, '', NULL, NULL, NULL, NULL, NULL, 57, 57, '2017-08-22 14:10:17', '2017-08-22 14:10:17'),
(18, 5, 89, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1920886809', 'nubmdd-adil-hasan@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:41', '2017-08-22 14:18:41'),
(19, 5, 90, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-ahdasan-habib@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:41', '2017-08-22 14:18:41'),
(20, 5, 91, 'Md. Nazmul Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-nazdmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(21, 5, 92, 'Md. Amzad Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-amszad-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(22, 5, 93, 'Md. Tanim Hosssain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-tanimf-hosssain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(23, 5, 94, 'Md. Tansin Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubmd-tansin-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(24, 5, 95, 'Hasina Begum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hassina@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(25, 5, 96, 'Ronjon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'ron@jon.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(26, 5, 97, 'Dilruba yeasmeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubdilruba-yeasmeen@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(27, 5, 98, 'Imranul Islam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubimranul-islam@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(28, 5, 99, 'Nuzmul hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubnuzmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(29, 5, 100, 'Liton kumar dus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubliton-kumar-dus@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:43', '2017-08-22 14:18:43'),
(30, 5, 101, 'Saddam Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'nubsaddam-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 5, 5, '2017-08-22 14:18:43', '2017-08-22 14:18:43'),
(31, 54, 102, 'Md. Adil Hasan', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '1920886809', 'bsmfd-adil-hasan@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:38', '2017-08-23 13:51:38'),
(32, 54, 103, 'Md. Ahasan Habib', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsmd-ahasan-habib@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:38', '2017-08-23 13:51:38'),
(33, 54, 104, 'Md. Nazmul Hossain', NULL, 'Islam', 'A+', NULL, NULL, NULL, NULL, 'Male', NULL, NULL, NULL, 'bsmd-dfgnazmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-24 10:04:06'),
(34, 54, 105, 'Md. Amzad Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsmd-dfgdamzad-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(35, 54, 106, 'Md. Tanim Hosssain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsmd-dfgdfgtanim-hosssain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(36, 54, 107, 'Md. Tansin Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsmddfgd-tansin-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(37, 54, 108, 'Hasina Begum', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'hasigfdgna@gmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(38, 54, 109, 'Ronjon', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'dfgdfron@jon.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(39, 54, 110, 'Dilruba yeasmeen', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsdilruba-yeasmeen@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(40, 54, 111, 'Imranul Islam', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsimranul-islam@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(41, 54, 112, 'Nuzmul hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsnuzmul-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(42, 54, 113, 'Liton kumar dus', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bsliton-kumar-dus@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(43, 54, 114, 'Saddam Hossain', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'bssaddam-hossain@lms.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 54, 54, '2017-08-23 13:51:40', '2017-08-23 13:51:40');

-- --------------------------------------------------------

--
-- Table structure for table `teacher_activities`
--

CREATE TABLE `teacher_activities` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `school_id` int(10) UNSIGNED NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `due_date` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Arabi', 'arabi@iedp.com.bd', '$2y$10$OVxAFV9VA9foSje4QfEXSucvoc66B6Sj1gq.NNkNXr9prCB7Q18ni', 'QXyit5EUgloaCrR0XXSqXvyTt1ATHeMA44acfjiNgylsRVqnTQl5cEghxFSZ', NULL, NULL),
(5, 'Northern University, Bangladesh', 'nub@ac.bd', '$2y$10$PTRkhffChvhY2sToHKCo.u9JePTlJIk6t4AmoPiIDXoQUdbN4Lnjq', 'h2KW6MVP0PFqDQjF16oUHU5agjIPTIB7wKDrNXGtUu3CF6r9KOQgrBwvFNhe', '2017-08-21 10:22:03', '2017-08-21 11:58:19'),
(6, 'Md. Adil Hasan', 'nubmd-adil-hasan@lms.com', '$2y$10$3jRRKMFvaAFaWBlm1k4df.2Xua7.G1ItID6qJR9PLhvzoydjNfkSC', '0nMXXzKWY7MXnYhb2PIyFmGMUkIDan1xI0dpK5r3DRX2bbOrKIqxnXWNaqtP', '2017-08-21 10:38:39', '2017-08-21 10:38:39'),
(7, 'Md. Ahasan Habib', 'nubmd-ahasan-habib@lms.com', '$2y$10$sG.egL.G41gMmHdy4vY2vuBwkHyVnxnoNO9/.PL/6O/VxJaVPZHum', NULL, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(8, 'Md. Nazmul Hossain', 'nubmd-nazmul-hossain@lms.com', '$2y$10$lqYEkhhoEHwAy14APOKhO.ILdEeuyVgvqOMTvc2938IOAdbGkv9ly', NULL, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(9, 'Md. Amzad Hossain', 'nubmd-amzad-hossain@lms.com', '$2y$10$l8.zmMo9u0I9DyCfow/83.9HWCOBf5g5ECTCYXZyA.P9Rf/CMG9YS', NULL, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(10, 'Md. Tanim Hosssain', 'nubmd-tanim-hosssain@lms.com', '$2y$10$u1V4FYYwJG7j42HkFQfHiuWQGS4rScku466pvSZUkVsBem6uTIsri', NULL, '2017-08-21 10:38:40', '2017-08-21 10:38:40'),
(11, 'Abul Hossain', 'abul@abul.com', '$2y$10$c9clAqhVEMFh3VfNS14JQO3ezVCsVha0BSSul7wsGIemj2p4UpLWK', 'fWYOFEXlAL8zp7bHNYMzUPYGDvRzxlFPE2OgTsGmJ2VIndcMaoad21tlLmQU', '2017-08-21 10:38:40', '2017-08-21 11:03:03'),
(12, 'Hasina Begum', 'hasina@gmail.com', '$2y$10$ydfeUEpOdUwGm3pTyoNf/O7.Dt8qKLR3AV0PUIuFXDWcaYZkeo5/m', NULL, '2017-08-21 10:38:41', '2017-08-21 10:38:41'),
(13, 'Hasan', 'e@mail.com', '$2y$10$XIFsDdzIlgtTMPERFggk7uAMVrpjiLlfhX/J8TKaH9nNC5VVCludu', NULL, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(14, 'an Habib', 'd@gmai.com', '$2y$10$mNQ6ErVg4G5JlP0uEgUsBOB0NeXiDONCYW6YpVPApSCeYbq6G/qOi', NULL, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(15, 'Shahidul Islam', 'e@dfsd.com', '$2y$10$cCV.IjkEApT8RU/l5MDVNe9ROFAo2a0i4dR3FCze0IbtpSsoQV26a', NULL, '2017-08-21 10:40:33', '2017-08-21 10:40:33'),
(16, 'Jakir Hosen', 'dfs@fsdfs.com', '$2y$10$zQJxaZXBktWAFE3xAZ0G1Oq5oPyEsugib4/QPlcjDOEQicZSXtl0S', NULL, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(17, 'Atiqur Khan', 'fdsfs@sdfsdf.com', '$2y$10$c6Pwbnsj.dPZ0ihisG2Y9u9PXi5Z3MJnbu0pSWBPAg7z/WOTMM4Eq', NULL, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(18, 'Saidujjaman Sajib', 'fsdfsklj@sdfsf.com', '$2y$10$r.RdnM8C7CfSB8EKkzMQo.3OwbpDR5EC3mfGRXFcSl3j9Nu8hYM4a', NULL, '2017-08-21 10:40:34', '2017-08-21 10:40:34'),
(19, 'Safin Istiaq Rubel', 'sdfksdjfl@sfsdf.com', '$2y$10$A4UYayUsBv1IkiqoJfquU.W34cjlMiC.lYJEVe1nbl0/92ddAd34C', NULL, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(20, 'Pisah', 'sfdfs@sdfs.com', '$2y$10$lnKv3EXYo5zqF7fNAUWgIutFA/BTpJKICWBqAD3AXmiModVVMDAUu', NULL, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(21, 'Mizanur Rahaman', 'fsdfsdf@sfsdfs.com', '$2y$10$9TVCRmz6r5Ai.DPnEJgt7.sjfJ325PEOZmwA3qKNi.x900f4gooou', NULL, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(22, 'Kamrul Hasan', 'sdfsf@sdfsdf.com', '$2y$10$rjRzJxqvew4jgZ9PaLi3BeHLQ9WTU9JVMTNUrwAOR3Wyluuo22MAq', NULL, '2017-08-21 10:40:35', '2017-08-21 10:40:35'),
(23, 'Md. Adil Hasan', 'dfse@mail.com', '$2y$10$KcUU.DjMnVpcHRxw3Aa/ZuHJ59gTw3a2N4XoZ.EvMfdRLP/AYVC3.', NULL, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(24, 'Md. Ahasan Habib', 'sdd@gmai.com', '$2y$10$ULKpEdgG/YhWnUW252AHfOIQTJjg8vcHxUVQ8kspYBERmcbGCjVjG', NULL, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(25, 'Md. Shahidul Islam', 'de@dfsd.com', '$2y$10$XYZTBc0vdcJ93yTFsPSAMuo06ZV7Vl0knpcOtFIIzrpjSrH8Bgb6y', NULL, '2017-08-21 10:44:42', '2017-08-21 10:44:42'),
(26, 'Md. Jakir Hosen', 'ddfs@fsdfs.com', '$2y$10$OmoO3Pg.CKjCXy9AvhS5XOgeU2ykZYgYiPFb0gg5QqrUt5wkYddLO', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(27, 'Md. Atiqur Khan', 'dfdsfs@sdfsdf.com', '$2y$10$k5AM7Ddl0u7VK3LEj4mgI.9jeAnY8qbejt6AVJSlmX0m9.iwZXrXW', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(28, 'Md. Saidujjaman Sajib', 'efsdfsklj@sdfsf.com', '$2y$10$Faz8jfFyzCmfFG6dEUBm0OMxwTPMfesHaxOIO0uZ2GqzfMQYrt9UG', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(29, 'Md. Safin Istiaq Rubel', 'sdefksdjfl@sfsdf.com', '$2y$10$s.iYKqlx8PtBIA3cFBC3YuCSaahLoKRjrrFLGb8USTpVAClQOyLt6', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(30, 'Md. Pisah', 'esfdfs@sdfs.com', '$2y$10$qTLcXpoHpjTybmYnq4fpveKHdjHdRKreuonX4arkFJqMWfjvVmmAC', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(31, 'Md. Mizanur Rahaman', 'fsdefsdf@sfsdfs.com', '$2y$10$qXha2esmqju4D6qMPGjZHekECyy1lCKpaOsvfrTWodret6e8SZR8y', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(32, 'Md. Kamrul Hasan', 'esdfsf@sdfsdf.com', '$2y$10$x4f9vxv.yAdaUW4wecsV2usxjVUDP.bmtqpSw5lhVqRWUcJ5CZDii', NULL, '2017-08-21 10:44:43', '2017-08-21 10:44:43'),
(33, 'asan', 'ef@mail.com', '$2y$10$5rzAP6Mb95vXlsnsW9cXQeQ19hFChuVNjyVQPSNlxcbMnJzLDn7km', NULL, '2017-08-21 10:46:43', '2017-08-21 10:46:43'),
(34, 'san Habib', 'dd@gmai.com', '$2y$10$ZUF6IJPkBBvS5ppJcJNFzecUACsmYUwBNkMV77HZb/IBrFYR0DIBS', NULL, '2017-08-21 10:46:43', '2017-08-21 10:46:43'),
(35, 'hahidul Islam', 'e@dfssfd.com', '$2y$10$ycgNw9G/Loa4OYDavDf2y.iEa3cTRBq838yi8txxctGswv4f0rF6a', NULL, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(36, 'akir Hosen', 'dfs@fsfdsdfs.com', '$2y$10$lRWNq95DK7zdo1taY5N9muFvxJ1K0ygqlNn/YeqGT2AvsLn9Vt0Je', NULL, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(37, 'tiqur Khan', 'fdsfs@sddsffsdf.com', '$2y$10$POB/AlnDbxV.tufnleFHm.U1K/cvviSS9wYpTH9nIkXso4QGtEhMy', NULL, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(38, 'idujjaman Sajib', 'fsdfsklj@fdsdfsf.com', '$2y$10$Z5zPVGiwDLmNzudejagHE.wyE3z3aGQoVp/uD0271sODm2SqMRUmu', NULL, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(39, 'afin Istiaq Rubel', 'sdfksdjfl@sffsdf.com', '$2y$10$yrl6cHNAf7bKniNbSl3b/umP9sBOLTbZNxKW6mUYdRHr5eslX6uYq', NULL, '2017-08-21 10:46:44', '2017-08-21 10:46:44'),
(40, 'isah', 'sfdfs@sdfss.com', '$2y$10$x9HueL5EbnYr0flkhkFAOurzjBvACaPdieJVjMLwC5VF1QvJtg7Ma', NULL, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(41, 'izanur Rahaman', 'fsdfsdf@sdsfsdfs.com', '$2y$10$zw5NrOFgsrKM8mkzsLWQKeAI1OyK2UItzxc8l34AaDDlvd7N8L3tq', NULL, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(42, 'mrul Hasan', 'sdfsf@sdfssddf.com', '$2y$10$9OfZ2YwQcourIEKxEp/sme1s7iv7k1ZkPtSTDUBB.6L6hXxxNb.4.', NULL, '2017-08-21 10:46:45', '2017-08-21 10:46:45'),
(43, 'sdfMd. Adil Hasan', 'fsde@mail.com', '$2y$10$q.FNsGZMWPtD8vrcGfeyGOcfn882tHfXoYqrGPz/QDiYCfspRkTG6', NULL, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(44, 'sd sdfsMd. Ahasan Habib', 'sfsdfd@gmai.com', '$2y$10$7aN4PrFv9yH1JeWdaQAO3.Zo4jQDgQt9VAqb6qJWBj0JsfqGUrmLO', NULL, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(45, 's fsd sMd. Shahidul Islam', 'sdfsfe@dfsd.com', '$2y$10$Ol2QOOaW.dPLDH61IiO5E.ep/BsQ2HxVQmH5Pocj9K9iqyupUPrva', NULL, '2017-08-21 11:06:35', '2017-08-21 11:06:35'),
(46, 'sfs Md. Jakir Hosen', 'dsdfsffs@fsdfs.com', '$2y$10$Q6Mfz5cwY5OCEuYiZzyDVu25l2sOgWj2x.pRO4iWln4PvGSadCVfa', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(47, 'Md. Atiqur Khan', 'fdsfdsfsfs@sdfsdf.com', '$2y$10$Uot7jqV4DjCFkFe/CTlKOOLr5q.joMygLIEds31DtFHhftrZG17Sa', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(48, 'Md. Saidujjaman Sajib', 'fssfddsfdfsklj@sdfsf.com', '$2y$10$M8DSneJHA1V/bkgBB41Kv.rfRKyyIoydfxiQHZT/bBQrKKf0NMHEG', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(49, 'Md. Safin Istiaq Rubel', 'sdfsfsdksdjfl@sfsdf.com', '$2y$10$tifEVXnoRdI0m.cWDElSxux0sN1GDJFxTUaMBCDnZD3NKkCptPzEG', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(50, 'Md. Pisah', 'sfdsfdsfs@sdfs.com', '$2y$10$vkSI6hwShG2d4CA1Kc/BNOs8yb3DPAgvXASbCgxWLdi5ecRXoyfXi', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(51, 'Md. Mizanur Rahaman', 'fsdfssdfsfdf@sfsdfs.com', '$2y$10$bgH0gmzfjDJn2yDX7sOCdOcSMzA6zTzAa26y2DhdRI5bFgV24fGDS', NULL, '2017-08-21 11:06:36', '2017-08-21 11:06:36'),
(52, 'Md. Kamrul Hasan', 'sdfsfssf@sdfsdf.com', '$2y$10$7BgPq5H2AgTvtr3EtOMWvebdUwRepZ2Td92HeOYzefRkJqe6uICBm', NULL, '2017-08-21 11:06:37', '2017-08-21 11:06:37'),
(54, 'BS', 'bs@23.com', '$2y$10$jTAX33k0QjixZcI0.mqA3OIoFaMRdgDVkzil2xK8ALgOuoQJOCex.', 'jrHA9Q4efgDpIU7FIKm3mm1AB2NhKoNjUpRmvPfULltTqtqIDOu5bWedczVk', '2017-08-21 14:15:40', '2017-08-21 14:15:40'),
(55, 'PSS', 'info@pss.com', '$2y$10$Q3AnEzo25D0Tckgp7Tc8MeNN0K7IUfmm59sxd/IVptCUMMxSrJdw2', NULL, '2017-08-22 09:09:17', '2017-08-22 09:09:17'),
(56, 'HTC', 'info@htc.com', '$2y$10$k8Y3aksg7/zALUCi.7d5R.RqJarqTn9RjknaObFLbWE/MG.kz2iVO', NULL, '2017-08-22 09:12:33', '2017-08-22 09:12:33'),
(57, 'GsCs', 'info@gscs.com', '$2y$10$0wJAS0MJABxnNKHN7JlrMuHGRjKDcILqhBw9UJyxyCh9hffuvN0fm', 'PYpJ5aYFyPljhxMJbcUmfla68bZcqOWh7MQ8EE8flRCUyjiCyFTkeJD71hOO', '2017-08-22 09:26:42', '2017-08-22 09:26:42'),
(58, 'Md. Adil Hasan', 'e@mdfgail.com', '$2y$10$.PFgoak83slq2Ftc51IHk.Ol6Sx/611ArLyr1S7jMrwyhLA2Y.wXu', NULL, '2017-08-22 09:30:31', '2017-08-22 09:30:31'),
(59, 'Md. Ahasan Habib', 'd@gmdgai.com', '$2y$10$Z6GnCCv4h4sSjNgYgp8PNe/k/lLlAQQdS4p3v3xUD3iW3X0IDCV6a', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(60, 'Md. Shahidul Islam', 'dge@dfsd.com', '$2y$10$vBo9a0IvWilfOCneUq/9deV/HI0gcNqvTulzL4jOSWY2dnCJQEfmC', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(61, 'Md. Jakir Hosen', 'dfdfgs@dgfsdfs.com', '$2y$10$ow46P00MdpqzRrrqcrPjD.sQQVdk55y/3BLuN5cPwsqf005iwKV0m', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(62, 'Md. Atiqur Khan', 'fdfgdsfs@sdgddfsdf.com', '$2y$10$gnIpdYZP1TqoJN1jGVwGUueWn9uUbApL3VojaBNKY7GPurF6BoOZ.', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(63, 'Md. Saidujjaman Sajib', 'fsddfgfsklj@dgsdfsf.com', '$2y$10$vEwacKJK/vDLxnleD4V9iOAsBjRHq9lPlU5cDEgOU34DLDJ35LRdK', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(64, 'Md. Safin Istiaq Rubel', 'sdfkdfgsdjfl@sdfgfsdf.com', '$2y$10$0O/SxWZEWHC7.GPGIIAB9OPur1tJJWVvJDlenfoasO6oFJ.1fOKeG', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(65, 'Md. Pisah', 'sfdfdgs@sddgfs.com', '$2y$10$ysiEcv3T1smqDZhb1OjLDeN8jbqGgxDqgy5VG72wtW76l.zCQ0QbG', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(66, 'Md. Mizanur Rahaman', 'fdgsdfsdf@sdgfsdfs.com', '$2y$10$X9BHL//b5L3BxYnzUhYMieX7dvnXP4.SkYxSxihP7WcvDo5Lw.WTq', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(67, 'Md. Kamrul Hasan', 'sddgfsf@sdfsddgf.com', '$2y$10$OFDIKT0ZFiq/wJE9umNOyeNxFNZsxwFYIgkyrT4l2XaguxpkGgfaW', NULL, '2017-08-22 09:30:32', '2017-08-22 09:30:32'),
(68, 'Roton', 'gscsmd-adil-hasan@lms.com', '$2y$10$.IadcWNUHpmxuEM5lFrNQOdkkELezWGd8llosdPHwu5sIcyS354Oe', NULL, '2017-08-22 09:38:33', '2017-08-22 09:38:33'),
(69, 'Al amin', 'gscsmd-ahasan-habib@lms.com', '$2y$10$ja.k9zIXhKh1ofIUOsELsev1YnIC0RE333hqSYZgQkeaL.QNqsrGy', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(70, 'Ruhul hossain', 'gscsmd-nazmul-hossain@lms.com', '$2y$10$5gjYYVD2zIU9iulDoQsmKe63o0B7d4EGebQmaMkQ3jZGP2bkATejy', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(71, 'Liton ali', 'gscsmd-amzad-hossain@lms.com', '$2y$10$v4h/UhHdvb9cSZw1Bun/uOqKlzw6MIChHgYwEGJBTiMewWhaW6jty', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(72, 'Mohammad ali', 'gscsmd-tanim-hosssain@lms.com', '$2y$10$JE630gutQS35TG2b0iY0KOVOTpzyMorDdtmuH9KDAKwReB070Rsf2', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(73, 'Ikramul islam', 'gscsmd-tansin-hossain@lms.com', '$2y$10$TdmzKoZJeaUl7EXr.J3cWOOfy2osLaLtucp0PquFNXuHR9TAPoTJK', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(74, 'Pavel Hossain', 'pavel@gmail.com', '$2y$10$rO9aEkJ9NU5mxGpw1O6q3uyyHylClbOFFve7jWKiV5hfynKH61nVC', NULL, '2017-08-22 09:38:34', '2017-08-22 09:38:34'),
(75, 'Azmal Huda Mehdi', 'admin@dhaka.com', '$2y$10$iF9O/i39qHV448770jwhC.s.EJXT8QlNiMAj3BHgj2PM6eMoz6say', NULL, '2017-08-22 11:53:25', '2017-08-22 11:53:25'),
(76, 'Md. Adil Hasan', 'easdfsf@mail.com', '$2y$10$MV.wQUH1rTVezGipy4H9Fu7WWubsuYSn6N4krOVYAjS3FogiQ6d8C', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(77, 'Md. Ahasan Habib', 'bs117002@lms.com', '$2y$10$EG8VyvLadx1OgfuQXXijWeC38jiG3huk7noWD.ckWvyIZp6bOUxWW', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(78, 'Md. Shahidul Islam', 'bs117003@lms.com', '$2y$10$9z0bfI0whfhmRxQ1Rl3PsObo1EpcxV9z0.3xO7F4TOHKJQrZDiW4G', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(79, 'Md. Jakir Hosen', 'bs117004@lms.com', '$2y$10$JTT9rvsGFxqOXs3GeE5Cn.9zUwF3v21IbDzphNsPUwlK15M6g3glC', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(80, 'Md. Atiqur Khan', 'bs117005@lms.com', '$2y$10$f1Bw8zG.wHoXHE1S8rlGLuzmqqc0pABiTJhqJ/3YGD5tPz6agACJC', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(81, 'Md. Saidujjaman Sajib', 'bs117006@lms.com', '$2y$10$hAkkm6QpSp3.VQOzRGN2OeonFFcJveTIPLVFqMF/gCt854xW8IBG6', NULL, '2017-08-22 13:03:08', '2017-08-22 13:03:08'),
(82, 'Md. Safin Istiaq Rubel', 'bs117007@lms.com', '$2y$10$Y2ZCfRsKhMgXaa9AGDZKduFVK979cui4P01etQ6nHW0NUtHlKvrc.', NULL, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(83, 'Md. Pisah', 'bs117008@lms.com', '$2y$10$GNvW7j2kvPm.WR6GDGpV4OJJsuoBd5Y9.4evVJ.XdRHi4VptasyTG', NULL, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(84, 'Md. Mizanur Rahaman', 'bs117009@lms.com', '$2y$10$/0wJlvzDUtBMT8C9bRaE/.XwN6bsBxwdFO63WxvgBK.u5r375TlZe', NULL, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(85, 'Md. Kamrul Hasan', 'bs117010@lms.com', '$2y$10$aOtFEMKVrc05PnallIDOteM9FF8qEcj5iS7x9vVU5Ag7oc8CvlNVC', NULL, '2017-08-22 13:03:09', '2017-08-22 13:03:09'),
(86, 'Latifur Rahman', 'admin@gazipur.com', '$2y$10$y26IhJy.x89L5EAndhHYY.JIPVGLrqWvurT0kzSTfjRLVQKuNz7fO', NULL, '2017-08-22 13:10:08', '2017-08-22 13:10:08'),
(87, 'Solaiman Hossain', 'g@nub.com', '$2y$10$Se1YJ0TIZLjS33lTmhJrIOLCajFsc891oqErJks.Dfv4GYAfrthi6', NULL, '2017-08-22 13:19:48', '2017-08-22 13:19:48'),
(88, 'New Teacher Added', 'newteacher@gscs.com', '$2y$10$voPOsA7YHgvAlzMsGUqLDOq/q/PrWjkC8JVaMoiwS0gpkKVsShrlu', NULL, '2017-08-22 14:10:17', '2017-08-22 14:10:17'),
(89, 'Md. Adil Hasan', 'nubmdd-adil-hasan@lms.com', '$2y$10$5ytpFriJGQGbd6lJIJS2q.rhOpQirw3B9oKzMNkdJUkuZmRuAnBt6', NULL, '2017-08-22 14:18:41', '2017-08-22 14:18:41'),
(90, 'Md. Ahasan Habib', 'nubmd-ahdasan-habib@lms.com', '$2y$10$baYCsCuISUD2zTKfvHEpJ./kqh6AffcO2k6uxDph3eE7OGwICDTMm', NULL, '2017-08-22 14:18:41', '2017-08-22 14:18:41'),
(91, 'Md. Nazmul Hossain', 'nubmd-nazdmul-hossain@lms.com', '$2y$10$o085g2Rd/nNjkHuIwkkjBuQ5oLCe9KtdHBMxq.DIJSwreogO7Urjm', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(92, 'Md. Amzad Hossain', 'nubmd-amszad-hossain@lms.com', '$2y$10$Lm1RQeQg1xjG.VUEiugJH.tQ5SBKcvPwPszJjSTWXJqqVk8a7ua0C', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(93, 'Md. Tanim Hosssain', 'nubmd-tanimf-hosssain@lms.com', '$2y$10$0mYCBeOogZ.DMU9Ih4k82.IXjV01n.TIgMqmca9.j1VDgmyrAvWLK', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(94, 'Md. Tansin Hossain', 'nubmd-tansin-hossain@lms.com', '$2y$10$RNecb.ry.ZBURqnF80A.WuJEczcOlXmRrn/1EvWV07MeOUSG1lYma', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(95, 'Hasina Begum', 'hassina@gmail.com', '$2y$10$Vh5iUKW.4jxRpRJK/gZuvORdU8Q9zWkyo/5RblqCKBsouWWfPBmeO', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(96, 'Ronjon', 'ron@jon.com', '$2y$10$xI9XTcQHEN2Gsm54LCL6Tu30dxCoCf5flXELdNAXx5ptxSN2jmrae', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(97, 'Dilruba yeasmeen', 'nubdilruba-yeasmeen@lms.com', '$2y$10$YMjND/XoSnm6WjmIUreDzO4HHJgnFT0Qov8aZq9kMNvA2NnZdtSs2', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(98, 'Imranul Islam', 'nubimranul-islam@lms.com', '$2y$10$irG6gi/uc/bQJF3D3XEvQuCWqYANYuvL7bZg4jAvkMDJHE82S5Rj2', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(99, 'Nuzmul hossain', 'nubnuzmul-hossain@lms.com', '$2y$10$XksxggZnkMl80DP2PAQHtOJ0MUhfpiqA/4N1ZAS0.xpARIx/StLqO', NULL, '2017-08-22 14:18:42', '2017-08-22 14:18:42'),
(100, 'Liton kumar dus', 'nubliton-kumar-dus@lms.com', '$2y$10$2muI6nS1g/n6MzrErBcTHOFL/LoMyrAVGkEpPzceUwMWncULM6OUK', NULL, '2017-08-22 14:18:43', '2017-08-22 14:18:43'),
(101, 'Saddam Hossain', 'nubsaddam-hossain@lms.com', '$2y$10$3PCc4nwj31eoKtshxsFZx.4gHRifljYKKnlLpPY4I8c9MN1W9jadC', NULL, '2017-08-22 14:18:43', '2017-08-22 14:18:43'),
(102, 'Md. Adil Hasan', 'bsmfd-adil-hasan@lms.com', '$2y$10$ewMxrvbcvKTNBLQxZ3p8KeQBTDWY6GIgH2ktQKQWif4B.x7kaK6Xm', NULL, '2017-08-23 13:51:38', '2017-08-23 13:51:38'),
(103, 'Md. Ahasan Habib', 'bsmd-ahasan-habib@lms.com', '$2y$10$.bXMqMtN.JVO6jKGTuL10eGlYNRdUoESrnXORA6lgFwEyvjcUPUH6', NULL, '2017-08-23 13:51:38', '2017-08-23 13:51:38'),
(104, 'Md. Nazmul Hossain', 'bsmd-dfgnazmul-hossain@lms.com', '$2y$10$fy7mCARhGwPXCv4FQgUIeeS3P9vAf8fYpMQm34CPVZURo3aimnz1G', NULL, '2017-08-23 13:51:39', '2017-08-24 10:04:06'),
(105, 'Md. Amzad Hossain', 'bsmd-dfgdamzad-hossain@lms.com', '$2y$10$MhPQVc37/OCOKpdkLYgk0.tKIlVeaddizxzduvaqqX8NqTM5hRFQS', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(106, 'Md. Tanim Hosssain', 'bsmd-dfgdfgtanim-hosssain@lms.com', '$2y$10$GXcc6FSjZA.Kx/HzpZnDpOskOkZKr8jbbWqts/FSGovflspPvKy.6', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(107, 'Md. Tansin Hossain', 'bsmddfgd-tansin-hossain@lms.com', '$2y$10$SflrldidcyjETPz3HaxbS.mGzGME7DvLqwv/Epa7emCgZbQVhzmGS', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(108, 'Hasina Begum', 'hasigfdgna@gmail.com', '$2y$10$THzweJrGvobxK9wcK/J6r.ILOZDEgA3XPdMBkMGPgRoyTBdXaOxDG', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(109, 'Ronjon', 'dfgdfron@jon.com', '$2y$10$aRb7LujlAaVBbg21FtD14uu6b.EqI8kdUh7/g1c6MAPgtp/0gQsjC', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(110, 'Dilruba yeasmeen', 'bsdilruba-yeasmeen@lms.com', '$2y$10$rUqDq8Avgs941EI5joKlAeL/bY1bS8n6mA9EpKX7bWo9C0CCTBfue', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(111, 'Imranul Islam', 'bsimranul-islam@lms.com', '$2y$10$0sXfZf1nfisFH8KsXsELmemV/c60z8kri3BbEdoQ1cTsRPotdHrB6', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(112, 'Nuzmul hossain', 'bsnuzmul-hossain@lms.com', '$2y$10$fGvqAWxsouijUf15cbuIruk/TB8Na5ov0cjo/aMdoXTN1x.6nx4Sy', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(113, 'Liton kumar dus', 'bsliton-kumar-dus@lms.com', '$2y$10$3MGEZtAnb3RfNxiY6Sw/JumtRyLZz/LAeqsxIWue8TPckfYDZmlTW', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(114, 'Saddam Hossain', 'bssaddam-hossain@lms.com', '$2y$10$GJGm6xwKkOkWMZn7I8QWie5JZRkVviGAPb4lmIATzbmKKiBU3ykt.', NULL, '2017-08-23 13:51:39', '2017-08-23 13:51:39'),
(115, 'Md. Adil Hasan', 'e@maisdfdsl.com', '$2y$10$P4PRY0y6c9zh7lXR4ufkbOl.w3skyVqWSztSlsAiAdysrXm5YWXKK', NULL, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(116, 'Md. Ahasan Habib', 'bs1170fsdfs02@lms.com', '$2y$10$upoxlO752M8eQbgePep9tOfUlikPBIO9I16pV6HsVTVo8hPKx5QSG', NULL, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(117, 'Md. Shahidul Islam', 'bs117fsdfsd003@lms.com', '$2y$10$3U0YQpYhWMDCO1ODVsHJCOtvWelbu1Aquex88xeLanfNUCagjSzpi', NULL, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(118, 'Md. Jakir Hosen', 'bs117fdsfdsf004@lms.com', '$2y$10$WFGMDT37YzTPgwrG.E9NO.zOrALfl1RrOhlarsWrG4cEFLgObTdP.', NULL, '2017-08-23 13:53:42', '2017-08-23 13:53:42'),
(119, 'Md. Atiqur Khan', 'bs11sdfsf7005@lms.com', '$2y$10$8Y6sddCnYXTUvGMAolNxU.OVdETeBVfpTEeXW7AfWm5JeYJ3ayBUq', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(120, 'Md. Saidujjaman Sajib', 'bs11sdfsf7006@lms.com', '$2y$10$fx0C.xwKSvZNC1hCtzQteOvuzvPP.XteTsfQAviyGiz.VI.PTnPha', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(121, 'Md. Safin Istiaq Rubel', 'bs117dsfs007@lms.com', '$2y$10$T5oo/.qtNPa6a7s2vfAGE..xI/WcZrKveeBIJc1KGUlTm9TDOzelS', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(122, 'Md. Pisah', 'bs117fsdfsd008@lms.com', '$2y$10$YeRzGefIg7xd.kZvXI5.5eXaJw46faoQM/XSQSFBeFep8aqyGYpoG', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(123, 'Md. Mizanur Rahaman', 'bs117fsdf009@lms.com', '$2y$10$EWVg1XHYrGlS37KVz6zl/OoRAVADFh.lw8.3JqqeOspTulr0Q.Soy', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(124, 'Md. Kamrul Hasan', 'bs117sdfs010@lms.com', '$2y$10$dk4jdHkwGjTx0l45ELyVd.JTj3FQRb9voxOfYgXzwTrmClAabISS2', NULL, '2017-08-23 13:53:43', '2017-08-23 13:53:43'),
(125, 'Md. Adil Hasan', 'e@masdfsdfsdfil.com', '$2y$10$T0B.RkKdDjC08TJDb.SiaOtiaGQ8Uam1lareYMTEtvTT6PSaPKQZO', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(126, 'Md. Ahasan Habib', 'gscs1sdfsd224002@lms.com', '$2y$10$UWgNrKqXvj1KUpik.vfdGukDtSOmDjx5bGMDIKk9GJDOMGljARcqm', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(127, 'Md. Shahidul Islam', 'gscs1s224003@lms.com', '$2y$10$6L1MI.BTW1DfDcgYbV7d6eF24c3yEtfTvRsTWH7TjGAizcxPOLQrO', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(128, 'Md. Jakir Hosen', 'gscs1224004@lms.com', '$2y$10$ukej9SKMzeoD2IUAtxCBPOBJyNoL2emNyhOjhw4qBTTPz8oTiFwWe', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(129, 'Md. Atiqur Khan', 'gscs1224005@lms.com', '$2y$10$3namfcwP1xpMirqqklf99uDZ5gy3VWSIjoWqyuivA1H9wzbx4vy16', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(130, 'Md. Saidujjaman Sajib', 'gscs1224006@lms.com', '$2y$10$fjZawo43KA4DLfstNDsdZu.KJeKGpm0hyOwCDkzSuxBsLkcaD.ogy', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(131, 'Md. Safin Istiaq Rubel', 'gscs1224007@lms.com', '$2y$10$N6gyynJC1lcnp0v0lh3Nxe0ROYtNUfKGoyToZRqdA7x31gT6QCvu.', NULL, '2017-08-23 14:26:26', '2017-08-23 14:26:26'),
(132, 'Md. Pisah', 'gscs1224008@lms.com', '$2y$10$u9ggFkCreoaYSeAFOXFIFeY83VTlNhbW9oSJa/H9j.kqKG9fQ0Kk2', NULL, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(133, 'Md. Mizanur Rahaman', 'gscs1224009@lms.com', '$2y$10$qeTwbKNE9ZJSlpcmq9T/5OBUsD7n7dGozF/SpUaABWWcRG3PsWGde', NULL, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(134, 'Md. Kamrul Hasan', 'gscs1224010@lms.com', '$2y$10$AmAFN052MvRHJCsaH5HEGOAPrfvZipIjLVNh5BY7TteRagzoIzLSa', NULL, '2017-08-23 14:26:27', '2017-08-23 14:26:27'),
(135, 'Md. Adil Hasan', 'esdad@mail.com', '$2y$10$aDoLsk1bBvaXcRyZM0h/ceHr0Tgm6JdQV5u5Klt3UpmyiGQWT2qL2', NULL, '2017-08-23 14:27:07', '2017-08-23 14:27:07'),
(136, 'Md. Ahasan Habib', 'gscs1asds325002@lms.com', '$2y$10$MTUcjz0.3TpjoNxrFGZU8eSRmxkeZI0XQiHqLYqbRT3q4Wlmjz0Vi', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(137, 'Md. Shahidul Islam', 'gscs1aasda325003@lms.com', '$2y$10$FqMtdiIYn4TRIHxDdTeIC..vG.7ltGyVSiDpDSnGWzsdVvzu0bq5K', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(138, 'Md. Jakir Hosen', 'gscasdadas1325004@lms.com', '$2y$10$hIdQN4ZFyT.lLWXbLlPTJ.Uh3wLU8.oSspdBkfHZFfLUKnpJoJJAq', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(139, 'Md. Atiqur Khan', 'gscasdas1325005@lms.com', '$2y$10$q6ga3XjYOdAEtXak8ZbPlOUsQTYJhU7Gg3u.v6L1Kx0cOQHGSfGr2', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(140, 'Md. Saidujjaman Sajib', 'gscsasdas1325006@lms.com', '$2y$10$IWWG/AY6DnDttrQk7Hnj7.fZZGxps.VjTotXsVN0SZBF/IKwhKlLy', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(141, 'Md. Safin Istiaq Rubel', 'gscasdas1325007@lms.com', '$2y$10$ayKO3i0PMVGRzJDee494c.1Ld9Nb04Df.flIMZMWnmEOmp1TzpCIm', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(142, 'Md. Pisah', 'gscasdas1325008@lms.com', '$2y$10$/1WehrkaoaSdlYsuBsHpq.tZg3NxBB50QELzXsEFL4fQKMt2WkfBO', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(143, 'Md. Mizanur Rahaman', 'gsasdacs1325009@lms.com', '$2y$10$GQzLf/FaoWGJu0A5YPdev.SWVw6Vo7O/n0Lvvi4ImXOoeincFkIW2', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(144, 'Md. Kamrul Hasan', 'gasdsascs1325010@lms.com', '$2y$10$k2uWhQTsHCgoUg3eQr8MIujlVia6jisRP9..4R4NizPsPKICHmeCu', NULL, '2017-08-23 14:27:08', '2017-08-23 14:27:08'),
(145, 'kuddus', 'efsd@mail.com', '$2y$10$y24nlAkSXuW0mKmudPhp2.QEAsDlkaQWjnB2YPLd1i1RfiMm0c1um', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(146, 'abib', 'nubf155002@lms.com', '$2y$10$eJN5v7g249ZmCaEdDu/18OVnZyLPj1E036pjpC5SUJM0bE08buhby', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(147, 'hidul Islam', 'nub1f55003@lms.com', '$2y$10$loeL3RA91a/DBBrH/X5X9u4WXZcMQhdySrGcpu7gRWDnuOEihXNXW', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(148, 'Md. Ja', 'nub15f5004@lms.com', '$2y$10$VR96yD/2XT5t/uh7YdBky.s4wUMvJAJQgxs0Iy9t2szADb9xbqQCC', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(149, 'han', 'nub1f55005@lms.com', '$2y$10$KrLdLgsUruusFcqxvRWRG.J2X/tdzu1ZSAGChbt0tuETn0MbL4IEu', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(150, 'man Sajib', 'nub1f55006@lms.com', '$2y$10$z.OD6AnzLvk5/dgE4ste.O8F9/siVdByuOY7DOuFGqUT/iIO7AjMm', NULL, '2017-08-25 05:53:35', '2017-08-25 05:53:35'),
(151, 'stiaq Rubel', 'nub1f55007@lms.com', '$2y$10$1lWTJiFQIovhmDb6xWsPPuKm.OR5EfBZ0Jsjrd6BThhNAFcZnWxde', NULL, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(152, 'Pisah', 'nub15f5008@lms.com', '$2y$10$lDVaDGwz0QQShe0056Lo4OQinKy6Hc3zJs1CmplVrbBFBkc2ZU10.', NULL, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(153, 'haman', 'nub15f5009@lms.com', '$2y$10$slTrlLv/emM.AIMh4e/q9uUewpkDCHZ.3ThznMQZDYl4yRJnodJE2', NULL, '2017-08-25 05:53:36', '2017-08-25 05:53:36'),
(154, 'Kamrul Hasan', 'nub1f55010@lms.com', '$2y$10$uORvff4fhBxX46lc84U8tebrulWSHuD/LYdDghVJRKNB7NFLU8HC6', NULL, '2017-08-25 05:53:36', '2017-08-25 05:53:36');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `acsessions`
--
ALTER TABLE `acsessions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atnd_dates`
--
ALTER TABLE `atnd_dates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `atnd_infos`
--
ALTER TABLE `atnd_infos`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `courses`
--
ALTER TABLE `courses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `coursestudents`
--
ALTER TABLE `coursestudents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `distadmins`
--
ALTER TABLE `distadmins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `districts`
--
ALTER TABLE `districts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `guardians`
--
ALTER TABLE `guardians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `homeworks`
--
ALTER TABLE `homeworks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutes`
--
ALTER TABLE `institutes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `notices`
--
ALTER TABLE `notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `permissions_name_unique` (`name`);

--
-- Indexes for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_role_id_foreign` (`role_id`);

--
-- Indexes for table `remarks`
--
ALTER TABLE `remarks`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `role_user`
--
ALTER TABLE `role_user`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `role_user_role_id_foreign` (`role_id`);

--
-- Indexes for table `routinedays`
--
ALTER TABLE `routinedays`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routineperiods`
--
ALTER TABLE `routineperiods`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `routins`
--
ALTER TABLE `routins`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `stdgurdians`
--
ALTER TABLE `stdgurdians`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `students`
--
ALTER TABLE `students`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subjects`
--
ALTER TABLE `subjects`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teachers`
--
ALTER TABLE `teachers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teacher_activities`
--
ALTER TABLE `teacher_activities`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `acsessions`
--
ALTER TABLE `acsessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `atnd_dates`
--
ALTER TABLE `atnd_dates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `atnd_infos`
--
ALTER TABLE `atnd_infos`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;
--
-- AUTO_INCREMENT for table `courses`
--
ALTER TABLE `courses`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `coursestudents`
--
ALTER TABLE `coursestudents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `distadmins`
--
ALTER TABLE `distadmins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `districts`
--
ALTER TABLE `districts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `guardians`
--
ALTER TABLE `guardians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `homeworks`
--
ALTER TABLE `homeworks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `institutes`
--
ALTER TABLE `institutes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=96;
--
-- AUTO_INCREMENT for table `notices`
--
ALTER TABLE `notices`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `remarks`
--
ALTER TABLE `remarks`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `routinedays`
--
ALTER TABLE `routinedays`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `routineperiods`
--
ALTER TABLE `routineperiods`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `routins`
--
ALTER TABLE `routins`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=216;
--
-- AUTO_INCREMENT for table `sections`
--
ALTER TABLE `sections`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `stdgurdians`
--
ALTER TABLE `stdgurdians`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `students`
--
ALTER TABLE `students`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `subjects`
--
ALTER TABLE `subjects`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;
--
-- AUTO_INCREMENT for table `teachers`
--
ALTER TABLE `teachers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;
--
-- AUTO_INCREMENT for table `teacher_activities`
--
ALTER TABLE `teacher_activities`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=155;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `permission_role`
--
ALTER TABLE `permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `role_user`
--
ALTER TABLE `role_user`
  ADD CONSTRAINT `role_user_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `role_user_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
