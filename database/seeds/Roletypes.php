<?php

use Illuminate\Database\Seeder;

class Roletypes extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            ['name' => 'owner','display_name' => 'Owner','description'   =>'Owner Of The Site'],
            ['name' => 'school','display_name' => 'School','description'   =>'Admin of School'],
            ['name' => 'teacher','display_name' => 'Teacher','description'   =>'Teachers of the Institution'],
            ['name' => 'student','display_name' => 'Student','description'   =>'Students of the Websites'],
            ['name' => 'parent','display_name' => 'Parent','description'   =>'Student Parent'],
            ['name' => 'distadmin','display_name' => 'District Admin','description'   =>'District Admin']
        ]);

        DB::table('permissions')->insert([
            ['name' => 'create-school','display_name' => 'School Creation','description'   =>'Can Able To Create School'],
        ]);
        $pass = Hash::make('12345678');
        DB::table('users')->insert(
            array(
                'name'  => 'Arabi',
                'email' => 'arabi@iedp.com.bd',
                'password' => $pass
            )
        );

        DB::table('role_user')->insert(
            array(
                'user_id'  => 1,
                'role_id' => 1,
            )
        );

        DB::table('permission_role')->insert(
            array(
                'permission_id'  => 1,
                'role_id' => 1,
            )
        );

        DB::table('courses')->insert([
            ['school_id' => 1,'title' => 'Six','order' => 6, 'aliase' => 'six'],
            ['school_id' => 1,'title' => 'Seven','order' => 7, 'aliase' => 'seven'],
            ['school_id' => 1,'title' => 'Eight','order' => 8, 'aliase' => 'eight'],
            ['school_id' => 1,'title' => 'Nine','order' => 9, 'aliase' => 'nine'],
            ['school_id' => 1,'title' => 'Ten','order' => 10, 'aliase' => 'ten'],
        ]);

        DB::table('acsessions')->insert([
            ['title' => '2017','start_date' => '2017-01-01','end_date' => '2017-12-31']
        ]);

        DB::table('subjects')->insert([
            ['school_id' => 1,'course_id' => 1,'title' => 'বাংলা-১ম'],
            ['school_id' => 1,'course_id' => 1,'title' => 'বাংলা-২য়'],
            ['school_id' => 1,'course_id' => 1,'title' => 'ইংরেজী-১ম'],
            ['school_id' => 1,'course_id' => 1,'title' => 'ইংরেজী-২য়'],
            ['school_id' => 1,'course_id' => 1,'title' => 'আরবী'],
            ['school_id' => 1,'course_id' => 1,'title' => 'গণিত'],
            ['school_id' => 1,'course_id' => 1,'title' => 'বাংলাদেশ ও বিশ্বপরিচয়'],
            ['school_id' => 1,'course_id' => 1,'title' => 'বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 1,'title' => 'কৃষি শিক্ষা'],
            ['school_id' => 1,'course_id' => 1,'title' => 'গার্হস্থ্য বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 1,'title' => 'ধর্ম ও নৈতিক শিক্ষা'],
            ['school_id' => 1,'course_id' => 1,'title' => 'ইসলাম ধর্ম'],
            ['school_id' => 1,'course_id' => 1,'title' => 'হিন্দু ধর্ম'],
            ['school_id' => 1,'course_id' => 1,'title' => 'বৌদ্ধধর্ম'],
            ['school_id' => 1,'course_id' => 1,'title' => 'শারিরীক শিক্ষা ও স্বাস্থ্য'],
            ['school_id' => 1,'course_id' => 1,'title' => 'কর্ম ও জীবনমুখী শিক্ষা'],
            ['school_id' => 1,'course_id' => 1,'title' => 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি'],
            ['school_id' => 1,'course_id' => 1,'title' => 'চারু ও কারুকলা'],
            ['school_id' => 1,'course_id' => 1,'title' => ' তথ্য ও যোগাযোগ প্রযুক্তি'],


            ['school_id' => 1,'course_id' => 2,'title' => 'বাংলা-১ম'],
            ['school_id' => 1,'course_id' => 2,'title' => 'বাংলা-২য়'],
            ['school_id' => 1,'course_id' => 2,'title' => 'ইংরেজী-১ম'],
            ['school_id' => 1,'course_id' => 2,'title' => 'ইংরেজী-২য়'],
            ['school_id' => 1,'course_id' => 2,'title' => 'আরবী'],
            ['school_id' => 1,'course_id' => 2,'title' => 'গণিত'],
            ['school_id' => 1,'course_id' => 2,'title' => 'বাংলাদেশ ও বিশ্বপরিচয়'],
            ['school_id' => 1,'course_id' => 2,'title' => 'বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 2,'title' => 'কৃষি শিক্ষা'],
            ['school_id' => 1,'course_id' => 2,'title' => 'গার্হস্থ্য বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 2,'title' => 'ধর্ম ও নৈতিক শিক্ষা'],
            ['school_id' => 1,'course_id' => 2,'title' => 'ইসলাম ধর্ম'],
            ['school_id' => 1,'course_id' => 2,'title' => 'হিন্দু ধর্ম'],
            ['school_id' => 1,'course_id' => 2,'title' => 'বৌদ্ধধর্ম'],
            ['school_id' => 1,'course_id' => 2,'title' => 'শারিরীক শিক্ষা ও স্বাস্থ্য'],
            ['school_id' => 1,'course_id' => 2,'title' => 'কর্ম ও জীবনমুখী শিক্ষা'],
            ['school_id' => 1,'course_id' => 2,'title' => 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি'],
            ['school_id' => 1,'course_id' => 2,'title' => 'চারু ও কারুকলা'],
            ['school_id' => 1,'course_id' => 2,'title' => ' তথ্য ও যোগাযোগ প্রযুক্তি'],

            ['school_id' => 1,'course_id' => 3,'title' => 'বাংলা-১ম'],
            ['school_id' => 1,'course_id' => 3,'title' => 'বাংলা-২য়'],
            ['school_id' => 1,'course_id' => 3,'title' => 'ইংরেজী-১ম'],
            ['school_id' => 1,'course_id' => 3,'title' => 'ইংরেজী-২য়'],
            ['school_id' => 1,'course_id' => 3,'title' => 'আরবী'],
            ['school_id' => 1,'course_id' => 3,'title' => 'গণিত'],
            ['school_id' => 1,'course_id' => 3,'title' => 'বাংলাদেশ ও বিশ্বপরিচয়'],
            ['school_id' => 1,'course_id' => 3,'title' => 'বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 3,'title' => 'কৃষি শিক্ষা'],
            ['school_id' => 1,'course_id' => 3,'title' => 'গার্হস্থ্য বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 3,'title' => 'ধর্ম ও নৈতিক শিক্ষা'],
            ['school_id' => 1,'course_id' => 3,'title' => 'ইসলাম ধর্ম'],
            ['school_id' => 1,'course_id' => 3,'title' => 'হিন্দু ধর্ম'],
            ['school_id' => 1,'course_id' => 3,'title' => 'বৌদ্ধধর্ম'],
            ['school_id' => 1,'course_id' => 3,'title' => 'শারিরীক শিক্ষা ও স্বাস্থ্য'],
            ['school_id' => 1,'course_id' => 3,'title' => 'কর্ম ও জীবনমুখী শিক্ষা'],
            ['school_id' => 1,'course_id' => 3,'title' => 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি'],
            ['school_id' => 1,'course_id' => 3,'title' => 'চারু ও কারুকলা'],
            ['school_id' => 1,'course_id' => 3,'title' => ' তথ্য ও যোগাযোগ প্রযুক্তি'],

            ['school_id' => 1,'course_id' => 4,'title' => 'বাংলা-১ম'],
            ['school_id' => 1,'course_id' => 4,'title' => 'বাংলা-২য়'],
            ['school_id' => 1,'course_id' => 4,'title' => 'ইংরেজী-১ম'],
            ['school_id' => 1,'course_id' => 4,'title' => 'ইংরেজী-২য়'],
            ['school_id' => 1,'course_id' => 4,'title' => 'আরবী'],
            ['school_id' => 1,'course_id' => 4,'title' => 'গণিত'],
            ['school_id' => 1,'course_id' => 4,'title' => 'বাংলাদেশ ও বিশ্বপরিচয়'],
            ['school_id' => 1,'course_id' => 4,'title' => 'বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 4,'title' => 'কৃষি শিক্ষা'],
            ['school_id' => 1,'course_id' => 4,'title' => 'গার্হস্থ্য বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 4,'title' => 'ধর্ম ও নৈতিক শিক্ষা'],
            ['school_id' => 1,'course_id' => 4,'title' => 'ইসলাম ধর্ম'],
            ['school_id' => 1,'course_id' => 4,'title' => 'হিন্দু ধর্ম'],
            ['school_id' => 1,'course_id' => 4,'title' => 'বৌদ্ধধর্ম'],
            ['school_id' => 1,'course_id' => 4,'title' => 'শারিরীক শিক্ষা ও স্বাস্থ্য'],
            ['school_id' => 1,'course_id' => 4,'title' => 'কর্ম ও জীবনমুখী শিক্ষা'],
            ['school_id' => 1,'course_id' => 4,'title' => 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি'],
            ['school_id' => 1,'course_id' => 4,'title' => 'চারু ও কারুকলা'],
            ['school_id' => 1,'course_id' => 4,'title' => ' তথ্য ও যোগাযোগ প্রযুক্তি'],

            ['school_id' => 1,'course_id' => 5,'title' => 'বাংলা-১ম'],
            ['school_id' => 1,'course_id' => 5,'title' => 'বাংলা-২য়'],
            ['school_id' => 1,'course_id' => 5,'title' => 'ইংরেজী-১ম'],
            ['school_id' => 1,'course_id' => 5,'title' => 'ইংরেজী-২য়'],
            ['school_id' => 1,'course_id' => 5,'title' => 'আরবী'],
            ['school_id' => 1,'course_id' => 5,'title' => 'গণিত'],
            ['school_id' => 1,'course_id' => 5,'title' => 'বাংলাদেশ ও বিশ্বপরিচয়'],
            ['school_id' => 1,'course_id' => 5,'title' => 'বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 5,'title' => 'কৃষি শিক্ষা'],
            ['school_id' => 1,'course_id' => 5,'title' => 'গার্হস্থ্য বিজ্ঞান'],
            ['school_id' => 1,'course_id' => 5,'title' => 'ধর্ম ও নৈতিক শিক্ষা'],
            ['school_id' => 1,'course_id' => 5,'title' => 'ইসলাম ধর্ম'],
            ['school_id' => 1,'course_id' => 5,'title' => 'হিন্দু ধর্ম'],
            ['school_id' => 1,'course_id' => 5,'title' => 'বৌদ্ধধর্ম'],
            ['school_id' => 1,'course_id' => 5,'title' => 'শারিরীক শিক্ষা ও স্বাস্থ্য'],
            ['school_id' => 1,'course_id' => 5,'title' => 'কর্ম ও জীবনমুখী শিক্ষা'],
            ['school_id' => 1,'course_id' => 5,'title' => 'ক্ষুদ্র নৃগোষ্ঠীর ভাষা ও সংস্কৃতি'],
            ['school_id' => 1,'course_id' => 5,'title' => 'চারু ও কারুকলা'],
            ['school_id' => 1,'course_id' => 5,'title' => ' তথ্য ও যোগাযোগ প্রযুক্তি'],
        ]);
    }
}
