<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateRoutinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routins', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('session')->unsigned();
            $table->integer('day_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->integer('period_id')->unsigned();
            $table->integer('teacher_id')->unsigned()->nullable();
            $table->integer('subject_id')->unsigned()->nullable();
            $table->integer('added_by')->unsigned();
            $table->integer('updated_by')->unsigned();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('routins');
    }
}
