<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateInstitutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('institutes', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('en_name')->nullable();
            $table->string('alise')->nullable();
            $table->string('ins_code')->nullable();
            $table->string('clg_code')->nullable();
            $table->string('ins_eiin')->nullable();
            $table->string('directory')->nullable();
            $table->string('ins_vill')->nullable();
            $table->string('ins_post')->nullable();
            $table->string('ins_upazilla')->nullable();
            $table->string('ins_zilla')->nullable();
            $table->string('ins_mobile')->nullable();
            $table->string('email');
            $table->string('principle_name')->nullable();
            $table->string('principle_img')->nullable();
            $table->string('principle_phone')->nullable();
            $table->string('principle_email')->nullable();
            $table->string('website')->nullable();
            $table->string('facebook')->nullable();
            $table->string('twitter')->nullable();
            $table->string('youtube')->nullable();
            $table->integer('user_id')->nullable();
            $table->mediumInteger('district_id')->nullable();
            $table->string('logo')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('institutes');
    }
}
