<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('course_id')->unsigned();
            $table->string('title');
            $table->string('code')->nullable();
            $table->string('sub_group')->nullable();
            $table->smallInteger('marks')->unsigned()->nullable();
            $table->smallInteger('mcq')->unsigned()->nullable();
            $table->smallInteger('written')->unsigned()->nullable();
            $table->smallInteger('practical')->nullable();
            $table->integer('added_by')->unsigned()->nullable();
            $table->integer('update_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subjects');
    }
}
