<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teachers', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name')->nullable();
            $table->string('bn_name')->nullable();
            $table->text('religion')->nullable();
            $table->text('blood_group')->nullable();
            $table->string('designition')->nullable();
            $table->string('teach_subject')->nullable();
            $table->string('nationality')->nullable();
            $table->text('nid')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->string('joining_date')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('mpo_index')->nullable();
            $table->string('image')->nullable();
            $table->string('staff_order')->nullable();
            $table->longText('education')->nullable();
            $table->longText('training')->nullable();
            $table->string('ex_school')->nullable();
            $table->string('others')->nullable();
            $table->integer('added_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('teachers');
    }
}
