<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('school_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('name');
            $table->string('bn_name')->nullable();
            $table->text('religion')->nullable();
            $table->text('blood_group')->nullable();
            $table->string('gender')->nullable();
            $table->string('dob')->nullable();
            $table->integer('course_id')->unsigned();
            $table->integer('section_id')->unsigned();
            $table->integer('shift_id')->unsigned()->nullable();
            $table->integer('session')->unsigned();
            $table->mediumInteger('roll_no')->unsigned()->nullable();
            $table->string('std_group')->nullable();
            $table->string('mobile')->nullable();
            $table->string('email')->nullable();
            $table->string('image')->nullable();
            $table->integer('third_subject')->unsigned()->nullable();
            $table->integer('fourth_subject')->unsigned()->nullable();
            $table->boolean('status')->nullable();
            $table->boolean('public_status')->nullable();
            $table->string('previous_school')->nullable();
            $table->string('others')->nullable();
            $table->integer('parent')->unsigned()->nullable();
            $table->integer('added_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('students');
    }
}
