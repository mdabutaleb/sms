<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Homework extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'homeworks';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'routine_id', 'date_id', 'status', 'subject_id', 'home_work', 'added_by', 'updated_by'];

    
}
