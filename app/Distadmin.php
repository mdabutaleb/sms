<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Distadmin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'distadmins';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['district_id', 'user_id', 'photo'];

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function institutes()
    {
        return $this->hasMany('App\Institute', '');
    }


}
