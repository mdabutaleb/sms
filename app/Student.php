<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'students';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'user_id', 'name', 'bn_name', 'religion', 'blood_group', 'gender', 'dob', 'course_id', 'section_id', 'std_group', 'shift_id', 'session', 'roll_no', 'mobile', 'email', 'image', 'third_subject', 'fourth_subject', 'status', 'public_status', 'previous_school', 'others', 'parent', 'added_by', 'updated_by'];

    public function course(){
        return $this->hasOne('App\Course','id','course_id');
    }

    public function section(){
        return $this->hasOne('App\Section','id','section_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function acsession()
    {
        return $this->belongsTo('App\Acsession','session','id');
    }

    public function attends()
    {
        return $this->hasMany('App\Atnd_info','user_id','user_id');
    }
    
}
