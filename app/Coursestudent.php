<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coursestudent extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coursestudents';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'session', 'course_id', 'section_id', 'total_student'];

    public function sessionInfo(){
        return $this->belongsTo('App\Acsession','session','id');
    }
    public function course(){
        return $this->belongsTo('App\Course','course_id','id');
    }

    public function section(){
        return $this->belongsTo('App\Section','section_id','id');
    }
    
    public function student(){
        return $this->hasMany('App\Student','school_id','school_id')
                    ->where('session',$this->session)
                    ->where('course_id',$this->course_id)
                    ->where('section_id',$this->section_id);
    }
}
