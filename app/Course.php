<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'courses';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'title', 'aliase', 'order'];

    public function sections()
    {
        return $this->hasMany('App\Section', 'course_id', 'id');
    }

    public function subjects()
    {
        return $this->hasMany('App\Subject', 'course_id', 'id');
    }

    public function school()
    {
        return $this->belongsTo('App\Institute', 'user_id');
    }

}
