<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routineday extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routinedays';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'session', 'title', 'order'];

    public function acsession()
    {
        return $this->belongsTo('App\Acsession','session','id');
    }
}
