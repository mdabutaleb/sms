<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Subject;
use Illuminate\Http\Request;
use Session;
use App\Course;
use Auth;

class SubjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();

        if (!empty($keyword)) {
            $subject = Subject::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('course_id', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('code', 'LIKE', "%$keyword%")
				->orWhere('group', 'LIKE', "%$keyword%")
				->orWhere('marks', 'LIKE', "%$keyword%")
				->orWhere('mcq', 'LIKE', "%$keyword%")
				->orWhere('written', 'LIKE', "%$keyword%")
				->orWhere('practical', 'LIKE', "%$keyword%")
				->orWhere('added_by', 'LIKE', "%$keyword%")
				->orWhere('update_by', 'LIKE', "%$keyword%")
                ->where('school_id', $user)
                ->orWhere('school_id', 1)
				->paginate($perPage);
        } else {
            $subject = Subject::where('school_id', $user)->orWhere('school_id', 1)->paginate($perPage);
        }

        return view('subject.index', compact('subject'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $courses = Course::pluck('title','id');
        return view('subject.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required|integer',
            'title' => 'required|string'
        ]);        
        $requestData = $request->all();
        $requestData['school_id'] = Auth::id();
        Subject::create($requestData);

        Session::flash('flash_message', 'Subject added!');

        return redirect('dashboard/subject');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $subject = Subject::findOrFail($id);

        return view('subject.show', compact('subject'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $subject = Subject::findOrFail($id);
        $courses = Course::pluck('title','id');
        if($subject->school_id != Auth::id())
            return redirect('dashboard/subject');
        return view('subject.edit', compact('subject','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $subject = Subject::findOrFail($id);
        $subject->update($requestData);

        Session::flash('flash_message', 'Subject updated!');

        return redirect('dashboard/subject');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Subject::destroy($id);

        Session::flash('flash_message', 'Subject deleted!');

        return redirect('dashboard/subject');
    }
}
