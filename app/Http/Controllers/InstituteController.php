<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Institute;
use App\Teacher;
use Illuminate\Http\Request;
use Session;
use App\User;
use Hash;
use DB;
use Auth;
use App\Course;
use App\Section;
use App\District;
use App\Http\Controllers\HomeController;

class InstituteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $institute = Institute::where('name', 'LIKE', "%$keyword%")
                ->orWhere('alise', 'LIKE', "%$keyword%")
                ->orWhere('ins_code', 'LIKE', "%$keyword%")
                ->orWhere('clg_code', 'LIKE', "%$keyword%")
                ->orWhere('clg_code', 'LIKE', "%$keyword%")
                ->orWhere('ins_eiin', 'LIKE', "%$keyword%")
                ->orWhere('ins_vill', 'LIKE', "%$keyword%")
                ->orWhere('ins_post', 'LIKE', "%$keyword%")
                ->orWhere('ins_upazilla', 'LIKE', "%$keyword%")
                ->orWhere('ins_zilla', 'LIKE', "%$keyword%")
                ->orWhere('ins_mobile', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('website', 'LIKE', "%$keyword%")
                ->orWhere('facebook', 'LIKE', "%$keyword%")
                ->orWhere('twitter', 'LIKE', "%$keyword%")
                ->orWhere('youtube', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->orWhere('principle_name', 'LIKE', "%$keyword%")
                ->orWhere('principle_email', 'LIKE', "%$keyword%")
                ->orderBy('id', 'desc')
                ->paginate($perPage);
        } else {
            $institute = Institute::orderBy('id', 'desc')->paginate($perPage);
        }

        return view('institute.index', compact('institute'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $districts = District::pluck('name', 'id');
        return view('institute.create', compact('districts'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'ins_eiin' => 'required|unique:institutes',
            'email' => 'required|email|unique:users',
            'district_id' => 'required',
            'password' => 'required|min:8',
            'r_password' => 'same:password',
        ]);
        $requestData = $request->all();
        DB::transaction(function () use ($requestData, $request) {
            $requestData['password'] = Hash::make($requestData['password']);

            if (!file_exists(public_path('institute/' . $requestData['ins_eiin']))) {
                \File::makeDirectory(public_path('institute/' . $requestData['ins_eiin']), 0755, true);
            }
            if ($request->hasFile('logo')) {
                $uploadir = 'institute/' . $requestData['ins_eiin'];
                $file = $request['logo'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['logo'] = $uploadir . '/' . $fileName;
            }
            if ($request->hasFile('principle_img')) {
                $uploadir = 'institute/' . $requestData['ins_eiin'];
                $file = $request['principle_img'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['principle_img'] = $uploadir . '/' . $fileName;
            }
            $requestData['directory'] = 'institute/' . $requestData['ins_eiin'];

            $wordss = $requestData['name'];
            $words = explode(" ", $requestData['name']);
            $acronym = "";
            foreach ($words as $w) {
                $acronym .= $w[0];
            }
            $requestData['name'] = $acronym;
            $user = User::create($requestData);
            $user->attachRole(2);
            $user->attachRole(3);
            $requestData['name'] = $wordss;
            $requestData['user_id'] = $user->id;
//            dd($requestData);
            Institute::create($requestData);
            $principleOrTeacher = [
                'school_id' => $user->id,
                'user_id' => $user->id,
                'name' => $request->principle_name,
                'email' => $request->principle_email,
                'mobile' => $request->principle_phone,
                'added_by' => Auth::id(),

            ];
            Teacher::create($principleOrTeacher);
            $courses = Course::where('school_id', 1)->get();
            $input['school_id'] = $user->id;
            foreach ($courses as $course) {
                $input['course_id'] = $course->id;
                $input['title'] = 'A';
                Section::create($input);
            }

        });
        Session::flash('flash_message', 'Institute added!');

        return redirect(route('institute.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $institute = Institute::findOrFail($id);
        $object = new HomeController;
        return $object->getSchoolInformation($institute->user_id);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $institute = Institute::findOrFail($id);
        $districts = District::pluck('name', 'id');
        return view('institute.edit', compact('institute', 'districts'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        $requestData = $request->all();
        $school = Institute::findOrFail($id);
        DB::transaction(function () use ($requestData, $request, $school, $id) {

            $requestData['updated_by'] = Auth::id();
            $school->update($requestData);

            if (!empty($requestData['password'])) {
                $this->validate($request, [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $school->user->id,
                    'password' => 'required|min:8',
                    'r_password' => 'same:password',
                ]);
                $requestData['password'] = Hash::make($requestData['password']);
                $school->user()->update($requestData);

            } else {
                $this->validate($request, [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $school->user->id,
                ]);
                $school->user()->update(array(
                        'name' => $requestData['name'],
                        'email' => $requestData['email'],
                    )
                );
            }

        });

        if ($request->hasFile('logo')) {
            $uploadir = 'institute/' . $requestData['ins_eiin'];
            $file = $request['logo'];
            $uploadPath = public_path($uploadir);

            $name = $file->getClientOriginalName();
            $fileName = rand(11111, 99999) . $name;
            $file->move($uploadPath, $fileName);
            $requestData['logo'] = $uploadir . '/' . $fileName;
        }
        if ($request->hasFile('principle_img')) {
            $uploadir = 'institute/' . $requestData['ins_eiin'];
            $file = $request['principle_img'];
            $uploadPath = public_path($uploadir);

            $name = $file->getClientOriginalName();
            $fileName = rand(11111, 99999) . $name;
            $file->move($uploadPath, $fileName);
            $requestData['principle_img'] = $uploadir . '/' . $fileName;
        }

        $institute = Institute::findOrFail($id);
        $institute->update($requestData);

        Session::flash('flash_message', 'Institute updated!');

        return redirect(route('institute.index'));
    }


    public function destroy($id)
    {
        Institute::destroy($id);

        Session::flash('flash_message', 'Institute deleted!');

        return redirect(route('institute.index'));
    }
}
