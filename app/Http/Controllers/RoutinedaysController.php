<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Routineday;
use Illuminate\Http\Request;
use Session;
use App\Acsession;
use Auth;

class RoutinedaysController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();

        if (!empty($keyword)) {
            $routinedays = Routineday::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('session', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('order', 'LIKE', "%$keyword%")
                ->orderBy('session','desc')
                ->orderBy('order','asc')
                ->where('school_id', $user)
				->paginate($perPage);
        } else {
            $routinedays = Routineday::orderBy('session','desc')
                                    ->orderBy('order','asc')
                                    ->where('school_id', $user)
                                    ->paginate($perPage);
        }

        return view('routinedays.index', compact('routinedays'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sessions = Acsession::orderBy('id','desc')->pluck('title','id');
        return view('routinedays.create',compact('sessions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['school_id'] = Auth::id();
        Routineday::create($requestData);

        Session::flash('flash_message', 'Routineday added!');

        return redirect('dashboard/routinedays');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $routineday = Routineday::findOrFail($id);

        return view('routinedays.show', compact('routineday'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $routineday = Routineday::findOrFail($id);
        $sessions = Acsession::orderBy('id','desc')->pluck('title','id');

        return view('routinedays.edit', compact('routineday','sessions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $routineday = Routineday::findOrFail($id);
        $routineday->update($requestData);

        Session::flash('flash_message', 'Routineday updated!');

        return redirect('dashboard/routinedays');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Routineday::destroy($id);

        Session::flash('flash_message', 'Routineday deleted!');

        return redirect('dashboard/routinedays');
    }
}
