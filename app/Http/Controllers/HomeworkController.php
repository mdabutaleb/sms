<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Homework;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Routin;
use Carbon\Carbon;
use App\Routineday;
use App\Acsession;
use App\Atnd_date;
use DB;

class HomeworkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $qDate = $request->get('cDate');
        $user = Auth::id();
        $cDate = Carbon::now();
        if(empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())){
            $qDate = $cDate->toDateString();
        }
        $day = date('l',strtotime($qDate));     
        $cDay = Routineday::where('title',$day)->where('title',$day)->first();
        if(!count($cDay))
            return redirect(route('atnd_info.index'))->with('msg','This Day has no Class');
        $year = date('Y',strtotime($cDate->toDateString()));
        $cYear = Acsession::where('title',$year)->first();
        $atnDate = Atnd_date::where('date_str',strtotime($qDate))->first();

        if(!count($atnDate)){
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }
        $routines = Routin::where('teacher_id',$user)
                    ->where('day_id',$cDay->id)
                    ->where('session',$cYear->id)
                    ->orderBy('period_id','asc')
                    ->get();
        return view('homework.index', compact('routines','cDate','atnDate','qDate'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('homework.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'home_work' => 'required|string',
        ]);
        $input = $request->all();
        $routine = Routin::find($input['routine_id']);
        $input['date_text'] = date('Y-m-d',$input['date_str']);
        $input['school_id'] = Auth::user()->teacher->school_id;
        $input['added_by'] = Auth::id();
        $input['updated_by'] = Auth::id();
        $input['subject_id'] = $routine->subject_id;
        $atnd_date = Atnd_date::where('date_str',$input['date_str'])->first();
        if(!(count($atnd_date)))
            $atnd_date = Atnd_date::create($input);
        $input['date_id'] = $atnd_date->id;
        $input['status'] = 1;
        Homework::create($input);

        Session::flash('msg', 'Homework added!');

        return redirect('dashboard/homework');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $homework = Homework::findOrFail($id);

        return view('homework.show', compact('homework'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $homework = Homework::findOrFail($id);

        return view('homework.edit', compact('homework'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $homework = Homework::findOrFail($id);
        $homework->update($requestData);

        Session::flash('flash_message', 'Homework updated!');

        return redirect('dashboard/homework');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Homework::destroy($id);

        Session::flash('flash_message', 'Homework deleted!');

        return redirect('dashboard/homework');
    }


    public function addHomework($date,$routine_id)
    {
        $routine = Routin::find($routine_id);
        $user = Auth::id();
        $atnd_date = Atnd_date::where('date_str',$date)->first();
        $atndStatus = $routine->homeworks->where('date_id',$atnd_date->id)->count();
        if($atndStatus)
            return redirect(route('homework.index'))->with('msg','Attendance Already Added');
        if($routine->teacher_id != $user)
            return redirect(route('atnd_info.index'));
        return view('homework.create',compact('routine','date','routine_id'));
    }
}
