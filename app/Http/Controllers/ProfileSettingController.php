<?php

namespace App\Http\Controllers;

use App\Distadmin;
use App\District;
use App\Guardian;
use App\Institute;
use App\Student;
use App\Teacher;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Session;

class ProfileSettingController extends Controller {
	protected function show( Request $request ) {

		$user = Auth::user();
		if ( Auth::user()->hasRole( 'owner' ) ) {
			return view( 'profiles.owner.view', [ 'user' => $user ] );

		} elseif ( Auth::user()->hasRole( 'distadmin' ) ) {
			$dist_admin_profile = $user->distadmin;
			$data               = [
				'user'    => $user,
				'profile' => $dist_admin_profile,
			];

			return view( 'profiles.dist_admin.view', $data );

		} elseif ( Auth::user()->hasRole( 'school' ) ) {

			$institute_profile = $user->institute;
			$data['profile']   = $institute_profile;

			return view( 'profiles.institute.view', $data );


		} elseif ( Auth::user()->hasRole( 'teacher' ) ) {
			$profile         = $user->teacher;
			$data['profile'] = $profile;

			return view( 'profiles.teacher.view', $data );

		} elseif ( Auth::user()->hasRole( 'student' ) ) {
			$profile = $user->student;
			$data    = [
				'user'    => $user,
				'profile' => $profile,
			];

			return view( 'profiles.student.view', $data );
		} elseif ( Auth::user()->hasRole( 'parent' ) ) {
			$profile = $user->guardian;
			$data    = [
				'user'    => $user,
				'profile' => $profile,
			];

			return view( 'profiles.guardian.view', $data );
		}
	}

	public function edit( Request $request ) {
		$districts = District::pluck( 'name', 'id' );
		$user      = Auth::user();
		$data      = [
			'districts' => $districts,
			'user'      => $user,
		];
		if ( Auth::user()->hasRole( 'owner' ) ) {
			return view( 'profiles.owner.edit', [ 'user' => $user ] );
		} elseif ( Auth::user()->hasRole( 'distadmin' ) ) {
			$dist_admin_profile = $user->distadmin;
			$data               = [
				'user'    => $user,
				'profile' => $dist_admin_profile,
			];

			return view( 'profiles.dist_admin.edit', $data );
		} elseif ( Auth::user()->hasRole( 'school' ) ) {
			$institute_profile = $user->institute;
			$data['institute'] = $institute_profile;

			return view( 'profiles.institute.edit', $data );
		} elseif ( Auth::user()->hasRole( 'teacher' ) ) {
			$teacher         = $user->teacher;
			$data['teacher'] = $teacher;

			return view( 'profiles.teacher.edit', $data );
		} elseif ( Auth::user()->hasRole( 'student' ) ) {
			$profile = $user->student;
			$data    = [
				'user'    => $user,
				'profile' => $profile,
			];

			return view( 'profiles.student.edit', $data );
		} elseif ( Auth::user()->hasRole( 'parent' ) ) {
			$profile = $user->guardian;
			$data    = [
				'user'    => $user,
				'profile' => $profile,
			];

			return view( 'profiles.guardian.edit', $data );
		}
	}

	public function update( Request $request, $id = '' ) {

		if ( Auth::user()->hasRole( 'owner' ) ) {
			return $this->OwnerProfileUpdate( $request );

		} elseif ( Auth::user()->hasRole( 'distadmin' ) ) {
			return $this->DistrictAdminProfileUpdate( $request, $id );

		} else if ( Auth::user()->hasRole( 'school' ) ) {
			return $this->InstituteUpdate( $request );

		} elseif ( Auth::user()->hasRole( 'teacher' ) ) {
			return $this->TeacherUpdate( $request );

		} elseif ( Auth::user()->hasRole( 'student' ) ) {
			return $this->StudentUpdate( $request, $id );
		} elseif ( Auth::user()->hasRole( 'parent' ) ) {
			return $this->GuardianUpdate( $request, $id );
		}

	}

	public function GuardianUpdate( $request, $id = '' ) {

		$requestData = $request->only( [ 'name', 'mobile', 'email', 'password', 'address' ] );
		$guardian    = Guardian::whereUserId( $id )->first();
		$guardian->update( $requestData );
		$this->imageUpload($request, $guardian);
		$user = Auth::user();
		if ( ! empty( $requestData['password'] ) ) {
			$this->validate( $request, [
				'name'       => 'required|string',
				'email'      => 'required|email|unique:users,email,' . $user->id,
				'password'   => 'required|min:8',
				'r_password' => 'same:password',
			] );
			$requestData['password'] = Hash::make( $requestData['password'] );
			$user                    = User::find( $user->id );
			$user->update( $requestData );

		} else {
			$this->validate( $request, [
				'name'   => 'required|string',
				'email'  => 'required|email|unique:users,email,' . $user->id,
			] );
			$user = User::find( $user->id );
			$user->update( array(
					'name'  => $requestData['name'],
					'email' => $requestData['email'],
				)
			);
		}

		Session::flash( 'flash_message', 'Information updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );

	}

	public function StudentUpdate( $request, $id = "" ) {
		$user        = User::find( $id );
		$requestData = $request->all();
		$school      = Institute::whereUserId( $user->student->school_id )->first();
		DB::transaction( function () use ( $requestData, $request, $school, $id, $user ) {
			if ( $request->hasFile( 'image' ) ) {
				$uploadir   = $school->directory;
				$file       = $request['image'];
				$uploadPath = public_path( $uploadir );

				$name     = $file->getClientOriginalName();
				$fileName = rand( 11111, 99999 ) . $name;
				$file->move( $uploadPath, $fileName );
				$requestData['image'] = $uploadir . '/' . $fileName;
			}

			$requestData['updated_by'] = Auth::id();
			$student                   = Student::whereUserId( $user->id )->first();

			$student->update( $requestData );


			if ( ! empty( $requestData['password'] ) ) {
				$this->validate( $request, [
					'name'       => 'required|string',
					'gender'     => 'required|string',
					'email'      => 'required|email|unique:users,email,' . $user->id,
					'password'   => 'required|min:8',
					'r_password' => 'same:password',
				] );
				$requestData['password'] = Hash::make( $requestData['password'] );
				$user                    = User::find( $id );
				$user->update( $requestData );

			} else {
				$this->validate( $request, [
					'name'   => 'required|string',
					'gender' => 'required|string',
					'email'  => 'required|email|unique:users,email,' . $user->id,
				] );
				$user = User::find( $id );
				$user->update( array(
						'name'  => $requestData['name'],
						'email' => $requestData['email'],
					)
				);
			}

		} );

		Session::flash( 'flash_message', 'Student updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );
	}

	public function DistrictAdminProfileUpdate( $request, $id ) {
		$distAdmin   = Distadmin::whereUserId( $id )->first();
		$requestData = $request->all();

		DB::transaction( function () use ( $requestData, $request, $distAdmin, $id ) {
			$distAdmin->update( $requestData );
			$this->imageUpload( $request->all(), $distAdmin );
			if ( ! empty( $requestData['password'] ) ) {
				$this->validate( $request, [
					'name'       => 'required|string',
					'email'      => 'required|email|unique:users,email,' . $distAdmin->user->id,
					'password'   => 'required|min:8',
					'r_password' => 'same:password',

				] );
				$requestData['password'] = Hash::make( $requestData['password'] );
				$distAdmin->user()->update( $requestData );

			} else {
				$this->validate( $request, [
					'name'  => 'required|string',
					'email' => 'required|email|unique:users,email,' . $distAdmin->user->id,
				] );
				$distAdmin->user()->update( array(
						'name'  => $requestData['name'],
						'email' => $requestData['email'],
					)
				);
			}

		} );
		Session::flash( 'flash_message', 'Information updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );

	}

	public function TeacherUpdate( $request ) {

		$user = Auth::user();

		$requestData = $request->all();
		$school      = Institute::where( 'user_id', $user->teacher->school_id )->first();
		DB::transaction( function () use ( $requestData, $request, $school, $user ) {
			if ( $request->hasFile( 'image' ) ) {
				$uploadir   = $school->directory;
				$file       = $request['image'];
				$uploadPath = public_path( $uploadir );

				$name     = $file->getClientOriginalName();
				$fileName = rand( 11111, 99999 ) . $name;
				$file->move( $uploadPath, $fileName );
				$requestData['image'] = $uploadir . '/' . $fileName;
			}

			$requestData['updated_by'] = Auth::id();
			$teacher                   = Teacher::where( 'user_id', $user->id )->first();
			$teacher->update( $requestData );

			if ( ! empty( $requestData['password'] ) ) {
				$this->validate( $request, [
					'name'       => 'required|string',
					'gender'     => 'required|string',
					'email'      => 'required|email|unique:users,email,' . $teacher->user->id,
					'password'   => 'required|min:8',
					'r_password' => 'same:password',
				] );
				$requestData['password'] = Hash::make( $requestData['password'] );
				$teacher->user()->update( $requestData );

			} else {
				$this->validate( $request, [
					'name'   => 'required|string',
					'gender' => 'required|string',
					'email'  => 'required|email|unique:users,email,' . $teacher->user->id,
				] );
				$teacher->user()->update( array(
						'name'  => $requestData['name'],
						'email' => $requestData['email'],
					)
				);
			}

		} );

		Session::flash( 'flash_message', 'Information updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );

	}

	public function OwnerProfileUpdate( $request ) {
		$user        = Auth::user();
		$update      = User::find( $user->id );
		$requestData = $request->all();
		if ( ! empty( $requestData['password'] ) ) {
			$this->validate( $request, [
				'name'       => 'required|string',
				'email'      => 'required|email|unique:users,email,' . Auth::user()->id,
				'password'   => 'required|min:8',
				'r_password' => 'same:password',
			] );

			$requestData['password'] = Hash::make( $requestData['password'] );
			$update->update( $requestData );

		} else {
			$this->validate( $request, [
				'name'  => 'required|string',
				'email' => 'required|email|unique:users,email,' . $user->id,
			] );
			$requestData = [
				'name'  => $requestData['name'],
				'email' => $requestData['email'],
			];
			$update->update( $requestData );
		}
		Session::flash( 'flash_message', 'Information updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );
	}

	public function InstituteUpdate( $request ) {
		$requestData = $request->all();
		$school      = Institute::where( 'user_id', Auth::id() )->first();
		DB::transaction( function () use ( $requestData, $request, $school ) {


			$requestData['updated_by'] = Auth::id();
			$school->update( $requestData );
			if ( ! empty( $requestData['password'] ) ) {
				$this->validate( $request, [
					'name'       => 'required|string',
					'email'      => 'required|email|unique:users,email,' . $school->user->id,
					'password'   => 'required|min:8',
					'r_password' => 'same:password',
				] );

				$requestData['password'] = Hash::make( $requestData['password'] );
				$school->user()->update( $requestData );

			} else {
				$this->validate( $request, [
					'name'  => 'required|string',
					'email' => 'required|email|unique:users,email,' . $school->user->id,
				] );
				$school->user()->update( array(
						'name'  => $requestData['name'],
						'email' => $requestData['email'],
					)
				);
			}

		} );

		if ( $request->hasFile( 'logo' ) ) {

			$uploadir   = 'institute/' . $requestData['ins_eiin'];
			$file       = $request['logo'];
			$uploadPath = public_path( $uploadir );

			$name     = $file->getClientOriginalName();
			$fileName = rand( 11111, 99999 ) . $name;
			$file->move( $uploadPath, $fileName );
			$requestData['logo'] = $uploadir . '/' . $fileName;
		}
		if ( $request->hasFile( 'principle_img' ) ) {

			$uploadir   = 'institute/' . $requestData['ins_eiin'];
			$file       = $request['principle_img'];
			$uploadPath = public_path( $uploadir );

			$name     = $file->getClientOriginalName();
			$fileName = rand( 11111, 99999 ) . $name;
			$file->move( $uploadPath, $fileName );
			$requestData['principle_img'] = $uploadir . '/' . $fileName;
		}

		$institute = Institute::where( 'user_id', Auth::id() )->first();
		$institute->update( $requestData );

		Session::flash( 'flash_message', 'Information updated!' );

		return redirect( 'dashboard/profile/' . Auth::id() . '/' . str_slug( Auth::user()->name, '-' ) );

	}

	public function imageUpload( $request, $guardian ) {
		$school = Institute::whereUserId($guardian->school_id)->first();

		if ( Input::file( 'photo' ) ) {
			$file           = Input::file( 'photo' );
			$slug           = time() . 'photo';
			$New_image_Name = $slug . "." . $file->getClientOriginalExtension();
			$Dynamic_path   = $school->directory. '/';
			$file_path      = $Dynamic_path . $New_image_Name;
			move_uploaded_file( $file->getPathName(), $file_path );
			$requestData['photo'] = $file_path;

			$guardian->update( [ 'photo' => $file_path ] );
		}
	}

}
