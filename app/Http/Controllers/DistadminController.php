<?php

namespace App\Http\Controllers;

use App\District;
use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Distadmin;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Input;
use Session;

class DistadminController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $distadmin = Distadmin::where('district_id', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('photo', 'LIKE', "%$keyword%")
                ->paginate($perPage);
        } else {
            $distadmin = Distadmin::paginate($perPage);
        }

        return view('distadmin.index', compact('distadmin'));
    }


    public function create()
    {
        $districts = District::pluck('name', 'id');
        return view('distadmin.create', compact('districts'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email|unique:users',
            'district_id' => 'required',
            'password' => 'required|min:8',
            'r_password' => 'same:password',
        ]);

        $requestData = $request->all();
        DB::transaction(function () use ($requestData, $request) {
            $requestData['password'] = bcrypt($request->password);
            $user = User::create($requestData);
            $user->attachRole(6);

            $requestData['user_id'] = $user->id;
            $distAdmin = Distadmin::create($requestData);
            $this->imageUpload($request->all(), $distAdmin);

        });
        Session::flash('flash_message', 'District admin added successfully!');
        return redirect('dashboard/distadmin');
    }


    public function show($id)
    {
        $distadmin = Distadmin::findOrFail($id);

        return view('distadmin.show', compact('distadmin'));
    }


    public function edit($id)
    {
        $distadmin = Distadmin::findorfail($id);


        $districts = District::pluck('name', 'id');

        return view('distadmin.edit', compact('distadmin', 'districts'));
    }

    public function update($id, Request $request)
    {
        $distAdmin = Distadmin::findorfail($id);
        $requestData = $request->all();

        DB::transaction(function () use ($requestData, $request, $distAdmin, $id) {
            $distAdmin->update($requestData);
            $this->imageUpload($request->all(), $distAdmin);
            if (!empty($requestData['password'])) {
                $this->validate($request, [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $distAdmin->user->id,
                    'password' => 'required|min:8',
                    'r_password' => 'same:password',

                ]);
	            $requestData['password'] = Hash::make( $requestData['password'] );
                $distAdmin->user()->update($requestData);

            } else {
                $this->validate($request, [
                    'name' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $distAdmin->user->id,
                ]);
                $distAdmin->user()->update(array(
                        'name' => $requestData['name'],
                        'email' => $requestData['email'],
                    )
                );
            }

        });

        Session::flash('flash_message', 'Distadmin updated!');

        return redirect('dashboard/distadmin');
    }


    public function destroy($id)
    {
        $distAdmin = Distadmin::findorfail($id);
        User::destroy($distAdmin->user_id);
        Distadmin::destroy($id);

        Session::flash('flash_message', 'District Admin deleted Successfully!');

        return redirect('dashboard/distadmin');
    }

    public function imageUpload($request, $distAdmin)
    {
        if (Input::file('photo')) {
            $file = Input::file('photo');
            $slug = time() . 'photo';
            $New_image_Name = $slug . "." . $file->getClientOriginalExtension();
            $Dynamic_path = 'img/';
            $file_path = $Dynamic_path . $New_image_Name;
            move_uploaded_file($file->getPathName(), $file_path);
            $requestData['photo'] = $file_path;


            $Update = Distadmin::find($distAdmin->id);
            $Update->update(['photo' => $file_path]);
        }
    }
}
