<?php

/*
 * Taken from
 * https://github.com/laravel/framework/blob/5.3/src/Illuminate/Auth/Console/stubs/make/controllers/HomeController.stub
 */

namespace App\Http\Controllers;

use App\Atnd_info;
use App\District;
use App\Guardian;
use App\Http\Requests;
use App\Institute;
use App\Notice;
use App\Remark;
use App\Student;
use App\User;
use Illuminate\Http\Request;
use Auth;
use App\Acsession;
use App\Routin;
use App\Routineday;
use App\Routineperiod;
use App\Course;
use App\Teacher;
use App\Subject;
use DB;
use Carbon\Carbon;
use App\Atnd_date;
use App\Http\Controllers\Atnd_infoController;


class HomeController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
        $monitors = $this->monitor();
        $notices = $this->getNotice();
        $remarks = $this->getRemarks();
        $totalTeachers = $this->getNumberOfTeacher();
        $totalSchools = $this->getNumberOfInstitute();
        $totalDistricts = $this->getNumberOfDistrict();
        $myAttendence = $this->getMyAttendence();
        $instituteList = Auth::user()->hasRole('distadmin') ? $this->getInstituteListByDistrict() : [];
        $instituteInfo = Auth::user()->institute;
        if($instituteInfo == NULL){
            $instituteInfo = array();
        }

        $toDayReport = $this->getTodayAttendReport();
        $SevenDayReport = $this->get7DayAttendReport($toDayReport['totalStudents']);
        $oneMonthReport = $this->get30DayAttendReport($toDayReport['totalStudents']);
        $thisYearReport = $this->getThisYearAttendReport($toDayReport['totalStudents']);

        $data = [
            'monitors' => $monitors,
            'instituteInfo' => $instituteInfo,
            'notices' => $notices,
            'totalTeachers' => $totalTeachers,
            'totalSchools' => $totalSchools,
            'totalDistrict' => $totalDistricts,
            'myAttendence' => $myAttendence,
            'institutes' => $instituteList,
            'remarks' => $remarks,
            'todayReport' => $toDayReport,
            'SevenDayReport' => $SevenDayReport,
            'oneMonthReport' => $oneMonthReport,
            'thisYearReport' => $thisYearReport,
        ];
        return view('adminlte::home', $data);

    }

    protected function getTodayAttendReport($ins_Id = "")
    {
//        dd($ins_Id);
        $totalStudents = $this->getNumberOfStudent($ins_Id);
        if (empty($totalStudents)) {
            $totalStudents = 1;
        }
        $numOfAttend = $this->getTotalAttendenceToday();
        $Parcent = ceil(($numOfAttend * 100) / $totalStudents);
        $absent = $totalStudents - $numOfAttend;
        $data = [
            'totalStudents' => $totalStudents,
            'totalAttend' => $numOfAttend,
            'parcent' => $Parcent,
            'absent' => $absent,
        ];
        return $data;
    }

    protected function get7DayAttendReport($totalStudent = "")
    {

        $total7DayAttendAndWorkingDay = $this->get7DayAttend();
        $total7DayAttend = $total7DayAttendAndWorkingDay['totalAttend'];
        $numberOfLastWorkingDay = $total7DayAttendAndWorkingDay['workingDay'];

        $total7DayStudents = $totalStudent * $numberOfLastWorkingDay;

        $parcent = ceil(($total7DayAttend * 100) / $total7DayStudents);
        $absent = $total7DayStudents - $total7DayAttend;
        $data = [
            'total7DayStudents' => $total7DayStudents,
            'total7DayAttend' => $total7DayAttend,
            'parcent' => $parcent,
            'absent' => $absent,
        ];
        return $data;

    }

    protected function get30DayAttendReport($totalStudent = "")
    {

        $total30DayAttendAndWorkingDay = $this->get30DayAttend();
        $total30DayAttend = $total30DayAttendAndWorkingDay['totalAttend'];
        $numberOfWorkingDay = $total30DayAttendAndWorkingDay['workingDay'];
        $total30DayStudents = $totalStudent * $numberOfWorkingDay;

        $parcent = ceil(($total30DayAttend * 100) / $total30DayStudents);
        $absent = $total30DayStudents - $total30DayAttend;
        $data = [
            'total30DayStudents' => $total30DayStudents,
            'total30DayAttend' => $total30DayAttend,
            'absent' => $absent,
            'parcent' => $parcent,

        ];
        return $data;

    }

    protected function getThisYearAttendReport($totalStudent = "")
    {

        $totalThisYearAttendAndWorkingDay = $this->getThisYearAttend();
        $totalThisYearAttend = $totalThisYearAttendAndWorkingDay['totalAttend'];
        $numberOfWorkingDay = $totalThisYearAttendAndWorkingDay['workingDay'];
        $totalThisYearStudents = $totalStudent * $numberOfWorkingDay;

        $parcent = ceil(($totalThisYearAttend * 100) / $totalThisYearStudents);
        $absent = $totalThisYearStudents - $totalThisYearAttend;
        $data = [
            'totalThisYearStudents' => $totalThisYearStudents,
            'totalThisYearAttend' => $totalThisYearAttend,
            'absent' => $absent,
            'parcent' => $parcent,

        ];
        return $data;

    }

    protected function getNumberOfTeachersByDistrict()
    {
        $districtId = 1;
        $institutes = Institute::whereDistrictId($districtId)->count();
//        dd($institutes[4]->teachers);


    }

    protected function getInstituteListByDistrict()
    {

        $districtId = Auth::user()->distadmin->district->id;
        $institutes = Institute::whereDistrictId($districtId)->paginate(10);
        return $institutes;
    }

    protected function getMyAttendence()
    {
        $user = User::where('id', Auth::id())->first();
        $attendence = Atnd_info::whereUserId(Auth::id())
            ->whereDate('created_at', '=', Carbon::today()
                ->toDateString())
            ->get();
//        dd($attendence[0]);
        return $attendence;

    }

    protected function get7DayAttend($ins_Id = "")
    {
        if (empty($ins_Id)) {
            $ins_Id = $this->getMyInstituteId();
        }

        if (Auth::user()->hasRole('owner')) {
            $institutes = Institute::all();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 7 DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
                $all[$institute->user_id] = $perDayAttend;
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 7 DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                $all[$institute->user_id] = $perDayAttend;
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 7 DAY)
                AND ai.status = 1
                AND ai.school_id = $ins_Id
                GROUP BY DAY(ai.created_at)";

            $perDayAttend = DB::select(DB::raw($sql));
            $workingDay = 0;
            if (count($perDayAttend) > $workingDay) {
                $workingDay = count($perDayAttend);
            }
            $totalAttend = 0;

            if (!empty($perDayAttend)) {
                foreach ($perDayAttend as $item) {
                    $totalAttend = $totalAttend + $item->attends;
                }
            }
        }
        if (empty($totalAttend)) {
            $totalAttend = 1;
        }
        if (empty($workingDay)) {
            $workingDay = 1;
        }
        $data = [
            'workingDay' => $workingDay,
            'totalAttend' => $totalAttend,
        ];
        return $data;

    }

    protected function get30DayAttend($ins_Id = "")
    {
        if (empty($ins_Id)) {
            $ins_Id = $this->getMyInstituteId();
        }

        if (Auth::user()->hasRole('owner')) {
            $institutes = Institute::all();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 30 DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
                $all[$institute->user_id] = $perDayAttend;
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 30 DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                $all[$institute->user_id] = $perDayAttend;
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 30 DAY)
                AND ai.status = 1
                AND ai.school_id = $ins_Id
                GROUP BY DAY(ai.created_at)";
            $perDayAttend = DB::select(DB::raw($sql));
            $workingDay = 0;
            if (count($perDayAttend) > $workingDay) {
                $workingDay = count($perDayAttend);
            }
            $totalAttend = 0;

            if (!empty($perDayAttend)) {
                foreach ($perDayAttend as $item) {
                    $totalAttend = $totalAttend + $item->attends;
                }
            }
        }
        if (empty($totalAttend)) {
            $totalAttend = 1;
        }
        if (empty($workingDay)) {
            $workingDay = 1;
        }
        $data = [
            'workingDay' => $workingDay,
            'totalAttend' => $totalAttend,
        ];
        return $data;

    }

    protected function getThisYearAttend($ins_Id = "")
    {
        $dayNumber = date("z") + 1;
        if (empty($ins_Id)) {
            $ins_Id = $this->getMyInstituteId();
        }

        if (Auth::user()->hasRole('owner')) {
            $institutes = Institute::all();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL $dayNumber DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
                $all[$institute->user_id] = $perDayAttend;
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();
            $all = [];
            $totalAttend = 0;
            $workingDay = 0;
            foreach ($institutes as $institute) {
                $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL $dayNumber DAY)
                AND ai.status = 1
                AND ai.school_id = $institute->user_id
                GROUP BY DAY(ai.created_at)";

                $perDayAttend = DB::select(DB::raw($sql));
                $all[$institute->user_id] = $perDayAttend;
                if (count($perDayAttend) > $workingDay) {
                    $workingDay = count($perDayAttend);
                }
            }
            if (is_array($all)) {
                foreach ($all as $single) {
                    if (is_array($single)) {
                        foreach ($single as $item) {
                            $totalAttend = $totalAttend + $item->attends;
                        }
                    }

                }
            }

        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL $dayNumber DAY)
                AND ai.status = 1
                AND ai.school_id = $ins_Id
                GROUP BY DAY(ai.created_at)";
            $perDayAttend = DB::select(DB::raw($sql));
            $workingDay = 0;
            if (count($perDayAttend) > $workingDay) {
                $workingDay = count($perDayAttend);
            }
            $totalAttend = 0;

            if (!empty($perDayAttend)) {
                foreach ($perDayAttend as $item) {
                    $totalAttend = $totalAttend + $item->attends;
                }
            }
        }
        if (empty($totalAttend)) {
            $totalAttend = 1;
        }
        if (empty($workingDay)) {
            $workingDay = 1;
        }
        $data = [
            'workingDay' => $workingDay,
            'totalAttend' => $totalAttend,
        ];

        return $data;

    }

    protected function getTotalAttendenceToday($ins_Id = "")
    {
        if (empty($ins_Id)) {
            $ins_Id = $this->getMyInstituteId();
        }

        if (Auth::user()->hasRole('owner')) {
            $institutes = Institute::all();
            $attend = 0;
            foreach ($institutes as $institute) {
                $attendByInstitute = DB::table('atnd_infos')
                    ->where('school_id', $institute->user_id)
                    ->where('status', 1)
                    ->whereDate('created_at', '=', Carbon::today()->toDateString())
                    ->distinct()->get(['user_id'])->count();
                $attend = $attend + $attendByInstitute;
            }
        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();

            $attend = 0;
            foreach ($institutes as $institute) {
                $attendByInstitute = DB::table('atnd_infos')
                    ->where('school_id', $institute->user_id)
                    ->where('status', 1)
                    ->whereDate('created_at', '=', Carbon::today()->toDateString())
                    ->distinct()->get(['user_id'])->count();
                $attend = $attend + $attendByInstitute;
            }

        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $attend = DB::table('atnd_infos')
                ->where('school_id', $ins_Id)
                ->where('status', 1)
                ->whereDate('created_at', '=', Carbon::today()->toDateString())
                ->distinct()->get(['user_id'])->count();
        }
        return $attend;
    }

    protected function getNumberOfDistrict()
    {
        $totalDistrict = District::count();
        return $totalDistrict;
    }

    protected function getNumberOfInstitute()
    {
        if (Auth::user()->hasRole('owner')) {
            $institutes = Institute::all()->count();
        }
        if (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get()->count();
        }
        if (empty($institutes)) {
            $institutes = 0;
        }
        return $institutes;
    }

    protected function getNumberOfStudent($ins_Id = "")
    {
        if (empty($ins_Id)) {
            $ins_Id = $this->getMyInstituteId();
        }

        if (Auth::user()->hasRole('owner')) {
            $totalStudent = Student::all()->count();
        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();
            $totalStudent = 0;
            foreach ($institutes as $institute) {
                $totalStudent = $totalStudent + $institute->students->count();
            }
        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $totalStudent = Student::whereSchoolId($ins_Id)->count();
        }

        if (empty($totalStudent)) {
            $totalStudent = 1;
        }
        return $totalStudent;
    }

    protected function getNumberOfTeacher()
    {
        $ins_Id = $this->getMyInstituteId();
        if (Auth::user()->hasRole('owner')) {
            $totalTeachers = Teacher::all()->count();
        } elseif (Auth::user()->hasRole('distadmin')) {
            $MyDistrictId = Auth::user()->distadmin->district_id;
            $institutes = Institute::whereDistrictId($MyDistrictId)->get();
            $totalTeachers = 0;
            foreach ($institutes as $institute) {
                $totalTeachers = $totalTeachers + $institute->teachers->count();
            }
        } elseif (Auth::user()->hasRole('school') || Auth::user()->hasRole('student') || Auth::user()->hasRole('teacher') || Auth::user()->hasRole('parent')) {
            $totalTeachers = Teacher::whereSchoolId($ins_Id)->count();
        }
//        dd($totalTeachers);

        if (empty($totalTeachers)) {
            $totalTeachers = 1;
        }
        return $totalTeachers;;
    }

    protected function getNotice($ins_Id = "")
    {
        $ins_Id = $this->getMyInstituteId();
        $notices = Notice::where('school_id', $ins_Id)
            ->orderBy('created_at', 'desc')
            ->take(3)->get();
        return $notices;
    }

    protected function monitor()
    {
        //$requestData = $request->all();
        $error = 1;
        $qDate = '';
        $datas = array();
        $school_id = Auth::id();
        //$qDate = $request->get('cDate');
        $cDate = Carbon::now();

        if (empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())) {
            $qDate = $cDate->toDateString();
        }
        $qTime = $cDate->toTimeString();
        $session = Acsession::where('title', date('Y', strtotime($qDate)))->first();
        $year = $session->id;
        $day = date('l', strtotime($qDate));
        $cDay = Routineday::where('title', $day)
            ->where('school_id', $school_id)
            ->where('title', $day)
            ->where('session', $year)
            ->first();
        $atnDate = Atnd_date::where('date_str', strtotime($qDate))->first();

        if (!count($atnDate)) {
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }

        if (!empty($cDay)) {
            $error = 0;
            $msYr = Acsession::where('id', $year)->first();
            $msg = 'Class Monitoring For ' . $qDate . '';
            $days = $cDay;
            $period = Routineperiod::where('school_id', $school_id)
                ->where('session', $year)
                ->where('start_time', '<', $qTime)
                ->where('end_time', '>', $qTime)
                ->first();

            if (empty(count($period)))
                $period = Routineperiod::select('id')
                    ->orderBy('end_time', 'desc')
                    ->first();


            if (!empty($period)) {
                $routine = Routin::where('school_id', $school_id)
                    ->where('session', $year)
                    ->where('period_id', $period->id)
                    ->whereNotNull('teacher_id')
                    ->where('day_id', $cDay->id)
                    ->orderBy('course_id')
                    ->get();

                if (count($routine))
                    $error = 0;
                else
                    $error = 1;

                $datas['routine'] = $routine;
                $datas['atnDate'] = $atnDate;
            }
            // dd($period);
            // $teachers = Teacher::where('school_id',$school_id)->pluck('name','user_id');
            // $subjects = Subject::where('school_id',$school_id)->orWhere('school_id',1)->pluck('title','id');
            // $courses = Course::where('school_id',$school_id)->orWhere('school_id',1)->get();

            // $datas['error'] = $error;
            // $datas['msg'] = $msg;
            // $datas['year'] = $year;
            // $datas['days'] = $days;
            // $datas['period'] = $period;
            // $datas['courses'] = $courses;
            // $datas['teachers'] = $teachers;
            // $datas['subjects'] = $subjects;
            // $datas['cDate'] = $cDate;
            // $datas['atnDate'] = $atnDate;
            // $datas['qDate'] = $qDate;
        }
        $datas['error'] = $error;
        return $datas;
    }

    public function getSchoolInformation($ins_Id = '')
    {
//        dd($ins_Id);

        $teacher = Teacher::whereSchoolId($ins_Id)->paginate(7);
        $institute = Institute::whereUserId($ins_Id)->first();
        $remarks = Remark::whereSchoolId($ins_Id)->orderBy('created_at', 'desc')->paginate(5);

        $totalStudents = Student::whereSchoolId($ins_Id)->count();
        if (empty($totalStudents)) {
            $totalStudents = 1;
        }
        $attendToday = DB::table('atnd_infos')
            ->where('school_id', $ins_Id)
            ->where('status', 1)
            ->whereDate('created_at', '=', Carbon::today()->toDateString())
            ->distinct()->get(['user_id'])->count();
        $parcent = ceil(($attendToday * 100) / $totalStudents);

        $toDayReport = [
            'totalStudents' => $totalStudents,
            'totalAttend' => $attendToday,
            'absent' => $totalStudents - $attendToday,
            'parcent' => $parcent,
        ];
        $sql = "SELECT ai.created_at, COUNT(DISTINCT ai.user_id) AS attends
                FROM `atnd_infos` AS ai
                WHERE DATE_FORMAT(ai.created_at,'%Y-%m-%d') <= CURDATE() AND DATE_FORMAT(ai.created_at,'%Y-%m-%d') >= (CURDATE() - INTERVAL 7 DAY)
                AND ai.status = 1
                AND ai.school_id = $ins_Id
                GROUP BY DAY(ai.created_at)";

        $perDayAttend = DB::select(DB::raw($sql));
        $workingDay = 0;
        if (count($perDayAttend) > $workingDay) {
            $workingDay = count($perDayAttend);
        }
        $totalAttend7Day = 0;

        if (!empty($perDayAttend)) {
            foreach ($perDayAttend as $item) {
                $totalAttend7Day = $totalAttend7Day + $item->attends;
            }
        }
        $total7DayStudent = $totalStudents * $workingDay;
        if (empty($total7DayStudent)) {
            $total7DayStudent = 1;
        }
        $SevenDayReport = [
            'total7DayStudents' => $total7DayStudent,
            'total7DayAttend' => $totalAttend7Day,
            'parcent' => ceil(($totalAttend7Day * 100) / $total7DayStudent),
            'absent' => $total7DayStudent - $totalAttend7Day,
        ];

        $data = [
            'institute' => $institute,
            'teacher' => $teacher,
            'instituteName' => $institute->name,
            'school_id' => $ins_Id,
            'district_id' => $institute->district_id,
            'remarks' => $remarks,
            'todayReport' => $toDayReport,
            'SevenDayReport' => $SevenDayReport,
        ];

        return view('distadmin.view_institute', $data);
    }

    protected function getRemarks($ins_Id = "")
    {
        $ins_Id = $this->getMyInstituteId();
        $remarks = Remark::whereSchoolId($ins_Id)->take(3)->orderBy('created_at', 'desc')->get();
        return $remarks;

    }

    public function getMyInstituteId()
    {
        if (Auth::user()->hasRole('school')) {
            $ins_Id = Auth::user()->id;
        } elseif (Auth::user()->hasRole('teacher')) {
            $teacher = Teacher::whereUserId(Auth::id())->first();
            $ins_Id = $teacher->school_id;
        } elseif (Auth::user()->hasRole('student')) {
            $student = Student::whereUserId(Auth::id())->first();
            $ins_Id = $student->school_id;
        } elseif (Auth::user()->hasRole('parent')) {
            $guardian = Guardian::whereUserId(Auth::id())->first();
            $ins_Id = $guardian->school_id;
        } else {
            $ins_Id = Auth::id();
        }
//        dd($ins_Id);
        return $ins_Id;
    }


}