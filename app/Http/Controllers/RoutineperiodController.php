<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Routineperiod;
use Illuminate\Http\Request;
use Session;
use App\Acsession;
use Auth;

class RoutineperiodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();

        if (!empty($keyword)) {
            $routineperiod = Routineperiod::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('session', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('start_time', 'LIKE', "%$keyword%")
				->orWhere('end_time', 'LIKE', "%$keyword%")
				->orWhere('start_str', 'LIKE', "%$keyword%")
				->orWhere('end_str', 'LIKE', "%$keyword%")
                ->where('school_id', $user)
                ->orderBy('start_str','asc')
				->paginate($perPage);
        } else {
            $routineperiod = Routineperiod::where('school_id', $user)
                                            ->orderBy('created_at','asc')
                                            ->paginate($perPage);
        }

        return view('routineperiod.index', compact('routineperiod'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $sessions = Acsession::orderBy('id','desc')->pluck('title','id');
        return view('routineperiod.create',compact('sessions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'start_time' => 'required',
            'end_time' => 'required',
            'title'    => 'required'
        ]);
        $requestData = $request->all();
        $requestData['school_id'] = Auth::id();
        $requestData['start_str'] = strtotime($requestData['start_time']);
        $requestData['end_str'] = strtotime($requestData['end_time']);
        Routineperiod::create($requestData);

        Session::flash('flash_message', 'Routineperiod added!');

        return redirect('dashboard/routineperiod');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $routineperiod = Routineperiod::findOrFail($id);

        return view('routineperiod.show', compact('routineperiod'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $routineperiod = Routineperiod::findOrFail($id);
        $sessions = Acsession::orderBy('id','desc')->pluck('title','id');

        return view('routineperiod.edit', compact('routineperiod','sessions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $routineperiod = Routineperiod::findOrFail($id);
        $routineperiod->update($requestData);

        Session::flash('flash_message', 'Routineperiod updated!');

        return redirect('dashboard/routineperiod');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Routineperiod::destroy($id);

        Session::flash('flash_message', 'Routineperiod deleted!');

        return redirect('dashboard/routineperiod');
    }
}
