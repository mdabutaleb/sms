<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Guardian;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Session;
use App\Section;
use App\Course;
use Auth;
use App\Acsession;
use DB;
use Hash;
use App\User;
use App\Stdgurdian;

class GuardianController extends Controller {
	/**
	 * Display a listing of the resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function index( Request $request ) {
		$keyword = $request->get( 'search' );
		$perPage = 25;

		if ( ! empty( $keyword ) ) {
			$guardian = Guardian::where( 'school_id', 'LIKE', "%$keyword%" )
			                    ->orWhere( 'user_id', 'LIKE', "%$keyword%" )
			                    ->orWhere( 'mobile', 'LIKE', "%$keyword%" )
			                    ->orWhere( 'address', 'LIKE', "%$keyword%" )
			                    ->orWhere( 'added_by', 'LIKE', "%$keyword%" )
			                    ->orWhere( 'updated_by', 'LIKE', "%$keyword%" )
			                    ->paginate( $perPage );
		} else {
			$guardian = Guardian::where( 'school_id', Auth::id() )->paginate( $perPage );
		}

		return view( 'guardian.index', compact( 'guardian' ) );
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return \Illuminate\View\View
	 */
	public function create() {
		$courses  = Course::where( 'school_id', Auth::id() )->orWhere( 'school_id', 1 )->pluck( 'title', 'id' );
		$sections = Section::where( 'school_id', Auth::id() )->pluck( 'title', 'id' );
		$sessions = Acsession::pluck( 'title', 'id' );

		return view( 'guardian.create', compact( 'courses', 'sections', 'sessions' ) );
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @param \Illuminate\Http\Request $request
	 *
	 * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
	 */
	public function store( Request $request ) {
		$this->validate( $request, [
			'name'       => 'required|string',
			'email'      => 'required|email|unique:users',
			'password'   => 'required|min:8',
			'r_password' => 'same:password',
			'r_password' => 'same:password'
		] );
		$requestData = $request->all();
		$school      = Auth::user();
		DB::transaction( function () use ( $requestData, $request, $school ) {
			$requestData['password'] = Hash::make( $requestData['password'] );
			$user                    = User::create( $requestData );
			$user->attachRole( 5 );
			$requestData['school_id']   = $school->id;
			$requestData['user_id']     = $user->id;
			$requestData['added_by']    = $school->id;
			$requestData['updated_by']  = $school->id;
			$requestData['guardian_id'] = $requestData['user_id'];
			$guardian                   = Guardian::create( $requestData );
			$total                      = count( $requestData['student_ids'] );
			for ( $i = 0; $i < $total; $i ++ ) {
				$requestData['student_id']    = $requestData['student_ids'][ $i ];
				$requestData['relation_with'] = $requestData['relations_with'][ $i ];
				Stdgurdian::create( $requestData );
			}
			$this->imageUpload( $request, $guardian, $school );
		} );

		Session::flash( 'flash_message', 'Guardian added!' );

		return redirect( 'dashboard/guardian' );
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function show( $id ) {
		$guardian = Guardian::findOrFail( $id );

		return view( 'guardian.show', compact( 'guardian' ) );
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int $id
	 *
	 * @return \Illuminate\View\View
	 */
	public function edit( $id ) {
		$guardian = Guardian::findOrFail( $id );
		$courses  = Course::where( 'school_id', Auth::id() )->orWhere( 'school_id', 1 )->pluck( 'title', 'id' );
		$sections = Section::where( 'school_id', Auth::id() )->pluck( 'title', 'id' );
		$sessions = Acsession::pluck( 'title', 'id' );

		return view( 'guardian.edit', compact( 'guardian', 'courses', 'sections', 'sessions' ) );
	}

	public function update( $id, Request $request ) {

		$requestData = $request->all();

		$guardian = Guardian::findOrFail( $id );
		$guardian->update( $requestData );

		Session::flash( 'flash_message', 'Guardian updated!' );

		return redirect( 'dashboard/guardian' );
	}

	public function destroy( $id ) {
		Guardian::destroy( $id );

		Session::flash( 'flash_message', 'Guardian deleted!' );

		return redirect( 'dashboard/guardian' );
	}

	public function imageUpload( $request, $guardian, $school ) {

		if ( Input::file( 'photo' ) ) {
			$file           = Input::file( 'photo' );
			$slug           = time() . 'photo';
			$New_image_Name = $slug . "." . $file->getClientOriginalExtension();
			$Dynamic_path   = $school->institute->directory . '/';
			$file_path      = $Dynamic_path . $New_image_Name;
			move_uploaded_file( $file->getPathName(), $file_path );
			$requestData['photo'] = $file_path;
			$update = Guardian::find( $guardian->id );
			$update->update( [ 'photo' => $file_path ] );
		}
	}
}
