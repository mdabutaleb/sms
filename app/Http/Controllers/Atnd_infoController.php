<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Atnd_info;
use App\Teacher;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\Routin;
use Carbon\Carbon;
use App\Routineday;
use App\Acsession;
use App\Atnd_date;
use App\Homework;
use DB;

class Atnd_infoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request = null)
    {

        if (empty($request->get('cDate'))) {
            $qDate = date('Y-m-d');
        } else {
            $qDate = $request->get('cDate');
        }

        if (Auth::user()->hasRole('teacher')) {
            $teacher = Teacher::whereUserId(Auth::id())->first();
            $ins_Id = $teacher->school_id;
            $user = Auth::id();

        } else {
            $user = Auth::id();
            $ins_Id = Auth::id();
        }


        $cDate = Carbon::now();
        if (empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())) {
            $qDate = $cDate->toDateString();
        }

        $day = date('l', strtotime($qDate));
        $cDay = Routineday::where('title', $day)
            ->where('school_id', $ins_Id)
            ->first();
        if (is_null($cDay)) {
            $msg = "You have no class today";
            return view('atnd_info.index', compact('msg', 'qDate'));
        }

        $year = date('Y', strtotime($cDate->toDateString()));
        $cYear = Acsession::where('title', $year)->first();
        $atnDate = Atnd_date::where('date_str', strtotime($qDate))->first();

        if (!count($atnDate)) {
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }
        $routines = Routin::where('teacher_id', $user)
            ->where('day_id', $cDay->id)
            ->where('session', $cYear->id)
            ->orderBy('period_id', 'asc')
            ->get();

        if (empty(count($routines))) {
            $msg = "You have no class today";
            return view('atnd_info.index', compact('msg', 'qDate'));
        } else {
            return view('atnd_info.index', compact('routines', 'cDate', 'atnDate', 'qDate'));
        }

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $user = Auth::id();
        $routines = Routin::where('teacher_id', $user)->get();
        return view('atnd_info.create', compact('routines'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();

        Atnd_info::create($requestData);

        Session::flash('flash_message', 'Atnd_info added!');

        return redirect('dashboard/atnd_info');
    }


    public function show($id)
    {
        $atnd_info = Atnd_info::findOrFail($id);

        return view('atnd_info.show', compact('atnd_info'));
    }


    public function edit($id)
    {
        $atnd_info = Atnd_info::findOrFail($id);

        return view('atnd_info.edit', compact('atnd_info'));
    }


    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $atnd_info = Atnd_info::findOrFail($id);
        $atnd_info->update($requestData);

        Session::flash('flash_message', 'Atnd_info updated!');

        return redirect('dashboard/atnd_info');
    }


    public function destroy($id)
    {
        Atnd_info::destroy($id);

        Session::flash('flash_message', 'Atnd_info deleted!');

        return redirect('dashboard/atnd_info');
    }

    public function addAttendance($date, $routine_id)
    {
        $routine = Routin::find($routine_id);
        $user = Auth::id();
        $atnd_date = Atnd_date::where('date_str', $date)->first();
        $atndStatus = $routine->attendances->where('date_id', $atnd_date->id)->count();
        if ($atndStatus)
            return redirect(route('atnd_info.index'))->with('msg', 'Attendance Already Added');
        if ($routine->teacher_id != $user)
            return redirect(route('atnd_info.index'));
        return view('atnd_info.add', compact('routine', 'date', 'routine_id'));
    }

    public function storeAttendance(Request $request)
    {
        $input = $request->all();
        $input['date_text'] = date('Y-m-d', $input['date_str']);
        $input['school_id'] = Auth::user()->teacher->school_id;
        $input['added_by'] = Auth::id();
        $input['updated_by'] = Auth::id();
        $atnd_date = Atnd_date::where('date_str', $input['date_str'])->first();
        if (!(count($atnd_date)))
            $atnd_date = Atnd_date::create($input);
        $routine = Routin::find($input['routine_id']);
        $input['subject_id'] = $routine->subject_id;
        DB::transaction(function () use ($input, $atnd_date) {
            $total = count($input['user_ids']);
            $input['date_id'] = $atnd_date->id;
            for ($i = 0; $i < $total; $i++) {
                $input['user_id'] = $input['user_ids'][$i];
                $input['status'] = (isset($input['status' . $i])) ? $input['status' . $i] : 0;
                Atnd_info::create($input);
            }
            if (!empty($input['home_work'])) {
                $input['status'] = 1;
                Homework::create($input);
            }

        });
        return redirect(route('atnd_info.index'));
    }

    public function editAttendance($date, $routine_id)
    {
        $routine = Routin::find($routine_id);
        $user = Auth::id();
        $atnd_date = Atnd_date::where('date_str', $date)->first();
        $atndStatus = $routine->attendances->where('date_id', $atnd_date->id)->count();
        if (!$atndStatus)
            return redirect(route('atnd_info.index'))->with('msg', 'Attendance Not Yet Added');
        if ($routine->teacher_id != $user)
            return redirect(route('atnd_info.index'));
        return view('atnd_info.update', compact('routine', 'date', 'routine_id', 'atnd_date'));
    }

    public function updateAttendance(Request $request)
    {
        $input = $request->all();
        $input['date_text'] = date('Y-m-d', $input['date_str']);
        $input['school_id'] = Auth::user()->teacher->school_id;
        $input['added_by'] = Auth::id();
        $input['updated_by'] = Auth::id();
        $atnd_date = Atnd_date::where('date_str', $input['date_str'])->first();
        if (!(count($atnd_date)))
            $atnd_date = Atnd_date::create($input);
        DB::transaction(function () use ($input, $atnd_date) {
            $total = count($input['user_ids']);
            $input['date_id'] = $atnd_date->id;
            for ($i = 0; $i < $total; $i++) {
                $input['user_id'] = $input['user_ids'][$i];
                $input['status'] = (isset($input['status' . $i])) ? $input['status' . $i] : 0;
                $atndIfo = Atnd_info::where('date_id', $atnd_date->id)
                    ->where('routine_id', $input['routine_id'])
                    ->where('user_id', $input['user_id'])
                    ->first();
                if (count($atndIfo)) {
                    $atndIfo->status = $input['status'];
                    $atndIfo->save();
                } else {
                    Atnd_info::create($input);
                }

                $hWokr = Homework::where('date_id', $atnd_date->id)
                    ->where('routine_id', $input['routine_id'])
                    ->first();
                if (count($hWokr)) {
                    $hWokr->home_work = $input['home_work'];
                    $hWokr->save();
                } else {
                    if (!empty($input['home_work'])) {
                        $routine = Routin::find($input['routine_id']);
                        $input['subject_id'] = $routine->subject_id;
                        Homework::create($input);
                    }
                }

            }

            /*Update Homework*/

        });
        return redirect(route('atnd_info.index'));
    }
}
