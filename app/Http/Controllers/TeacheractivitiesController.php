<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Student;
use App\Teacher;
use App\Teacheractivity;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class TeacheractivitiesController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $teacheractivities = Teacheractivity::where('user_id', Auth::id())
                ->Where('content', 'LIKE', "%$keyword%")
                ->orderBy('created_at', 'desc')
                ->paginate($perPage);
        } else {
            $teacheractivities = Teacheractivity::whereUserId(Auth::id())
                ->orderBy('created_at', 'desc')
                ->paginate($perPage);
        }

        return view('teacheractivities.index', compact('teacheractivities'));
    }


    public function create()
    {
        return view('teacheractivities.create');
    }


    public function store(Request $request)
    {
        if (Auth::user()->hasRole('student')) {
            $student = Student::whereUserId(Auth::id())->first();
            $userId = $student->user_id;
            $schoolId = $student->school_id;
        } else {
            $teacher = Teacher::whereUserId(Auth::id())->first();
            $userId = $teacher->user_id;
            $schoolId = $teacher->school_id;
        }

        $requestData = $request->all();
        $requestData['user_id'] = $userId;
        $requestData['school_id'] = $schoolId;

        Teacheractivity::create($requestData);

        Session::flash('flash_message', 'Teacher activity added successfully!');

       return redirect('/dashboard/teacheractivities');
    }


    public function show($id)
    {
        $teacheractivity = Teacheractivity::findOrFail($id);

        return view('teacheractivities.show', compact('teacheractivity'));
    }


    public function edit($id)
    {
        $teacheractivity = Teacheractivity::findOrFail($id);

        return view('teacheractivities.edit', compact('teacheractivity'));
    }


    public function update($id, Request $request)
    {

        $requestData = $request->all();

        $teacheractivity = Teacheractivity::findOrFail($id);
        $teacheractivity->update($requestData);

        Session::flash('flash_message', 'Teacheractivity updated!');

        return redirect('dashboard/teacheractivities');
    }


    public function destroy($id)
    {
        Teacheractivity::destroy($id);

        Session::flash('flash_message', 'Teacheractivity deleted!');

        return redirect('dashboard/teacheractivities');
    }
}
