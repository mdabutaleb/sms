<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Section;
use Illuminate\Http\Request;
use Session;
use App\Course;
use Auth;
class SectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();

        if (!empty($keyword)) {
            $section = Section::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('course_id', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('aliase', 'LIKE', "%$keyword%")
				->orWhere('order', 'LIKE', "%$keyword%")
                ->where('school_id', $user)
                ->orderBy('course_id')
				->paginate($perPage);
        } else {
            $section = Section::where('school_id', $user)->orderBy('course_id')->paginate($perPage);
        }

        return view('section.index', compact('section'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $courses = Course::pluck('title','id');
        return view('section.create',compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        $requestData['school_id'] = Auth::id();
        Section::create($requestData);

        Session::flash('flash_message', 'Section added!');

        return redirect('dashboard/section');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $section = Section::findOrFail($id);

        return view('section.show', compact('section'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $section = Section::findOrFail($id);
        $courses = Course::pluck('title','id');
        if($section->school_id != Auth::id())
            return redirect('dashboard/section');

        return view('section.edit', compact('section','courses'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $section = Section::findOrFail($id);
        $section->update($requestData);

        Session::flash('flash_message', 'Section updated!');

        return redirect('dashboard/section');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Section::destroy($id);

        Session::flash('flash_message', 'Section deleted!');

        return redirect('dashboard/section');
    }

    //Get Section By Class Ajax
    public function sectionbyclass(Request $request)
    {
        $input  = $request->all();
        $course_id = $request->get('id');
        $sections = '';
        $classsections =  Section::wherecourse_id($course_id)->where('school_id',Auth::id())->get();
        $sections .= '<option value="">Select Section</option>';
        foreach($classsections as $classsection){
            $sections .= '<option value="'.$classsection->id.'">'.$classsection->title.'</option>';
        } 
        return $sections;
        exit;
    }
}
