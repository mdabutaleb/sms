<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Institute;
use App\Teacher;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\User;
use Hash;
use DB;
use App\Course;
use App\Section;
use App\Acsession;
use Excel;

class TeacherController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();

        if (!empty($keyword)) {
            $teacher = Teacher::where('school_id', 'LIKE', "%$keyword%")
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('bn_name', 'LIKE', "%$keyword%")
                ->orWhere('religion', 'LIKE', "%$keyword%")
                ->orWhere('blood_group', 'LIKE', "%$keyword%")
                ->orWhere('designition', 'LIKE', "%$keyword%")
                ->orWhere('teach_subject', 'LIKE', "%$keyword%")
                ->orWhere('nationality', 'LIKE', "%$keyword%")
                ->orWhere('nid', 'LIKE', "%$keyword%")
                ->orWhere('gender', 'LIKE', "%$keyword%")
                ->orWhere('dob', 'LIKE', "%$keyword%")
                ->orWhere('joining_date', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('mpo_index', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('staff_order', 'LIKE', "%$keyword%")
                ->orWhere('education', 'LIKE', "%$keyword%")
                ->orWhere('training', 'LIKE', "%$keyword%")
                ->orWhere('ex_school', 'LIKE', "%$keyword%")
                ->orWhere('others', 'LIKE', "%$keyword%")
                ->orWhere('added_by', 'LIKE', "%$keyword%")
                ->orWhere('updated_by', 'LIKE', "%$keyword%")
                ->where('school_id', $user)
                ->paginate($perPage);
        } else {
            $teacher = Teacher::where('school_id', $user)->paginate($perPage);
        }

        return view('teacher.index', compact('teacher'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('teacher.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'gender' => 'required|string',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'r_password' => 'same:password',
        ]);
        $requestData = $request->all();

        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school) {
            $requestData['password'] = Hash::make($requestData['password']);
            if ($request->hasFile('image')) {
                $uploadir = $school->ins->directory;
                $file = $request['image'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['image'] = $uploadir . '/' . $fileName;
            } else {
                $requestData['image'] = '';
            }
            $user = User::create($requestData);
            $user->attachRole(3);
            $requestData['school_id'] = $school->id;
            $requestData['user_id'] = $user->id;
            $requestData['added_by'] = $school->id;
            $requestData['updated_by'] = $school->id;
            Teacher::create($requestData);
        });
        //Teacher::create($requestData);
        Session::flash('flash_message', 'Teacher added!');

        return redirect('dashboard/teacher');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $teacher = Teacher::findOrFail($id);

        return view('teacher.show', compact('teacher'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $teacher = Teacher::findOrFail($id);

        return view('teacher.edit', compact('teacher'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school, $id) {
            if ($request->hasFile('image')) {
                $uploadir = $school->ins->directory;
                $file = $request['image'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['image'] = $uploadir . '/' . $fileName;
            }

            $requestData['updated_by'] = $school->id;
            $teacher = Teacher::findOrFail($id);
            $teacher->update($requestData);

            if (!empty($requestData['password'])) {
                $this->validate($request, [
                    'name' => 'required|string',
                    'gender' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $teacher->user->id,
                    'password' => 'required|min:8',
                    'r_password' => 'same:password',
                ]);
                $requestData['password'] = Hash::make($requestData['password']);
                $teacher->user()->update($requestData);

            } else {
                $this->validate($request, [
                    'name' => 'required|string',
                    'gender' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $teacher->user->id,
                ]);
                $teacher->user()->update(array(
                        'name' => $requestData['name'],
                        'email' => $requestData['email'],
                    )
                );
            }

        });

        Session::flash('flash_message', 'Teacher updated!');

        return redirect('dashboard/teacher');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Teacher::destroy($id);

        Session::flash('flash_message', 'Teacher deleted!');

        return redirect('dashboard/teacher');
    }

    public function import()
    {
        $courses = Course::pluck('title', 'id');
        $sections = Section::pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        $msg = 'Import Teacher From Excel File';
        return view('teacher.import', compact('courses', 'sections', 'sessions', 'msg'));
    }

    public function importstore(Request $request)
    {
        $this->validate($request, [
            'excel' => 'required|mimes:xlsx,xls,csv'
        ]);
        $input = $request->all();
        $courses = Course::pluck('title', 'id');
        $sections = Section::pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        $results = Excel::load($input['excel']);
        $results = $results->all();
        $user = Auth::user();
        $msg = 'Please review the following and make any change if needed the click upload';
        return view('teacher.import', compact('courses', 'sections', 'sessions', 'input', 'results', 'msg', 'user'));
    }

    public function importstoredb(Request $request)
    {
        $inputs = $request->all();
        DB::transaction(function () use ($inputs) {
            $total = count($inputs['names']);
            $input['school_id'] = Auth::id();
            for ($i = 0; $i < $total; $i++) {
                $input['name'] = $inputs['names'][$i];
                $input['mobile'] = $inputs['mobiles_'][$i];
                $input['email'] = $inputs['emails_'][$i];
                $input['password'] = Hash::make($inputs['passs_'][$i]);
                $user = User::create($input);
                $user->attachRole(3);
                $input['user_id'] = $user->id;
                $input['added_by'] = $input['school_id'];
                $input['updated_by'] = $input['school_id'];
                Teacher::create($input);
            }
        });
        return redirect(route('teacher.index'));
    }

}
