<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Coursestudent;
use Illuminate\Http\Request;
use Session;
use App\Course;
use App\Section;
use App\Acsession;
use Auth;
use App\User;
use Hash;
use DB;

class CoursestudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $coursestudent = Coursestudent::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('session', 'LIKE', "%$keyword%")
				->orWhere('course_id', 'LIKE', "%$keyword%")
				->orWhere('section_id', 'LIKE', "%$keyword%")
				->orWhere('total_student', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $coursestudent = Coursestudent::paginate($perPage);
        }

        return view('coursestudent.index', compact('coursestudent'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $courses = Course::pluck('title','id');
        $sections = Section::pluck('title','id');
        $sessions = Acsession::pluck('title','id');
        return view('coursestudent.create',compact('courses','sections','sessions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'total_student' => 'required',
            'course_id' => 'required',
            'section_id' => 'required',
            'session' => 'required|composite_unique:coursestudents,course_id,section_id',
        ],
        [
            'session.composite_unique' => 'Information Already Added!!!',
        ]);
        $requestData = $request->all();
        $requestData['school_id'] = Auth::id(); 

        Coursestudent::create($requestData);

        Session::flash('flash_message', 'Coursestudent added!');

        return redirect('dashboard/coursestudent');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $coursestudent = Coursestudent::findOrFail($id);

        return view('coursestudent.show', compact('coursestudent'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $coursestudent = Coursestudent::findOrFail($id);
        $courses = Course::pluck('title','id');
        $sections = Section::pluck('title','id');
        $sessions = Acsession::pluck('title','id');

        return view('coursestudent.edit', compact('coursestudent','courses','sections','sessions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $coursestudent = Coursestudent::findOrFail($id);
        $coursestudent->update(['total_student' =>  $requestData['total_student']]);

        Session::flash('flash_message', 'Coursestudent updated!');

        return redirect('dashboard/coursestudent');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Coursestudent::destroy($id);

        Session::flash('flash_message', 'Coursestudent deleted!');

        return redirect('dashboard/coursestudent');
    }
}
