<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Acsession;
use Illuminate\Http\Request;
use Session;

class AcsessionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $acsession = Acsession::where('school_id', 'LIKE', "%$keyword%")
				->orWhere('title', 'LIKE', "%$keyword%")
				->orWhere('start_date', 'LIKE', "%$keyword%")
				->orWhere('end_date', 'LIKE', "%$keyword%")
				->orWhere('added_by', 'LIKE', "%$keyword%")
				->orWhere('updated_by', 'LIKE', "%$keyword%")
				->paginate($perPage);
        } else {
            $acsession = Acsession::paginate($perPage);
        }

        return view('acsession.index', compact('acsession'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('acsession.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        Acsession::create($requestData);

        Session::flash('flash_message', 'Acsession added!');

        return redirect('dashboard/acsession');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $acsession = Acsession::findOrFail($id);

        return view('acsession.show', compact('acsession'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $acsession = Acsession::findOrFail($id);

        return view('acsession.edit', compact('acsession'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        
        $requestData = $request->all();
        
        $acsession = Acsession::findOrFail($id);
        $acsession->update($requestData);

        Session::flash('flash_message', 'Acsession updated!');

        return redirect('dashboard/acsession');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Acsession::destroy($id);

        Session::flash('flash_message', 'Acsession deleted!');

        return redirect('dashboard/acsession');
    }
}
