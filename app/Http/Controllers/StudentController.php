<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Stdgurdian;
use App\Student;
use Illuminate\Http\Request;
use Session;
use App\Course;
use App\Section;
use App\Acsession;
use Auth;
use App\User;
use Hash;
use DB;
use Excel;
use Carbon\Carbon;
use App\Atnd_date;
use App\Routineday;
use App\Routineperiod;
use App\Routin;
use App\Teacher;
use App\Subject;

use App\Http\Controllers\HomeController;

class StudentController extends Controller
{

    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;
        $user = Auth::id();
        if (!empty($keyword)) {
            $student = Student::where('school_id', $user)
                ->orWhere('user_id', 'LIKE', "%$keyword%")
                ->orWhere('name', 'LIKE', "%$keyword%")
                ->orWhere('bn_name', 'LIKE', "%$keyword%")
                ->orWhere('religion', 'LIKE', "%$keyword%")
                ->orWhere('blood_group', 'LIKE', "%$keyword%")
                ->orWhere('gender', 'LIKE', "%$keyword%")
                ->orWhere('dob', 'LIKE', "%$keyword%")
                ->orWhere('course_id', 'LIKE', "%$keyword%")
                ->orWhere('section_id', 'LIKE', "%$keyword%")
                ->orWhere('shift_id', 'LIKE', "%$keyword%")
                ->orWhere('session', 'LIKE', "%$keyword%")
                ->orWhere('roll_no', 'LIKE', "%$keyword%")
                ->orWhere('mobile', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('image', 'LIKE', "%$keyword%")
                ->orWhere('third_subject', 'LIKE', "%$keyword%")
                ->orWhere('fourth_subject', 'LIKE', "%$keyword%")
                ->orWhere('status', 'LIKE', "%$keyword%")
                ->orWhere('public_status', 'LIKE', "%$keyword%")
                ->orWhere('previous_school', 'LIKE', "%$keyword%")
                ->orWhere('others', 'LIKE', "%$keyword%")
                ->orWhere('parent', 'LIKE', "%$keyword%")
                ->orWhere('added_by', 'LIKE', "%$keyword%")
                ->orWhere('updated_by', 'LIKE', "%$keyword%")
                ->where('school_id', $user)
                ->paginate($perPage);
        } else {
            $student = Student::where('school_id', $user)->paginate($perPage);
        }

        return view('student.index', compact('student'));
    }

    public function create()
    {
        $courses = Course::where('school_id', Auth::id())->orWhere('school_id', 1)->pluck('title', 'id');
        $sections = Section::where('school_id', Auth::id())->pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        return view('student.create', compact('courses', 'sections', 'sessions'));
    }


    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'gender' => 'required|string',
            'course_id' => 'required',
            'section_id' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:8',
            'r_password' => 'same:password',
        ]);
        $requestData = $request->all();
        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school) {
            $requestData['password'] = Hash::make($requestData['password']);
            if ($request->hasFile('image')) {
                $uploadir = $school->ins->directory;
                $file = $request['image'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['image'] = $uploadir . '/' . $fileName;
            } else {
                $requestData['image'] = '';
            }
            $user = User::create($requestData);
            $user->attachRole(4);
            $requestData['school_id'] = $school->id;
            $requestData['user_id'] = $user->id;
            $requestData['added_by'] = $school->id;
            $requestData['updated_by'] = $school->id;
            Student::create($requestData);
        });
        //Student::create($requestData);

        Session::flash('flash_message', 'Student added!');

        return redirect('dashboard/student');
    }


    public function show($id)
    {
        $student = Student::findOrFail($id);

        return view('student.show', compact('student'));
    }

    public function edit($id)
    {
        $student = Student::findOrFail($id);
        $courses = Course::pluck('title', 'id');
        $sections = Section::pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        return view('student.edit', compact('student', 'courses', 'sections', 'sessions'));
    }


    public function update($id, Request $request)
    {
    	$user = User::find($id);
    //	dd($user);

        $requestData = $request->all();

        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school, $id) {
            if ($request->hasFile('image')) {
                $uploadir = $school->ins->directory;
                $file = $request['image'];
                $uploadPath = public_path($uploadir);

                $name = $file->getClientOriginalName();
                $fileName = rand(11111, 99999) . $name;
                $file->move($uploadPath, $fileName);
                $requestData['image'] = $uploadir . '/' . $fileName;
            }

            $requestData['updated_by'] = $school->id;
            $student = Student::findOrFail($id);
            $student->update($requestData);

            if (!empty($requestData['password'])) {
                $this->validate($request, [
                    'name' => 'required|string',
                    'gender' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $student->user->id,
                    'password' => 'required|min:8',
                    'r_password' => 'same:password',
                ]);
                $requestData['password'] = Hash::make($requestData['password']);
                $student->user()->update($requestData);

            } else {
                $this->validate($request, [
                    'name' => 'required|string',
                    'gender' => 'required|string',
                    'email' => 'required|email|unique:users,email,' . $student->user->id,
                ]);
                $student->user()->update(array(
                        'name' => $requestData['name'],
                        'email' => $requestData['email'],
                    )
                );
            }

        });

        Session::flash('flash_message', 'Student updated!');

        return redirect('dashboard/student');
    }


    public function destroy($id)
    {
        Student::destroy($id);

        Session::flash('flash_message', 'Student deleted!');

        return redirect('dashboard/student');
    }

    public function import()
    {
        $ins_Id = Auth::id();
        $courses = Course::where('school_id', $ins_Id)
            ->orWhere('school_id', 1)
            ->pluck('title', 'id');
        $sections = Section::pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        $msg = 'Import Student From Excel File';
        return view('student.import', compact('courses', 'sections', 'sessions', 'msg'));
    }

    public function importstore(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'session' => 'required',
            'section_id' => 'required',
            'excel' => 'required|mimes:xlsx,xls,csv'
        ]);
        $input = $request->all();
        $courses = Course::pluck('title', 'id');
        $sections = Section::pluck('title', 'id');
        $sessions = Acsession::pluck('title', 'id');
        $results = Excel::load($input['excel']);
        $results = $results->all();
        $user = Auth::user();
        $msg = 'Please review the following and make any change if needed the click upload';
        return view('student.import', compact('courses', 'sections', 'sessions', 'input', 'results', 'msg', 'user'));
    }

    public function importstoredb(Request $request)
    {
        $this->validate($request, [
            'course_id' => 'required',
            'session' => 'required',
            'section_id' => 'required',
        ]);
        $inputs = $request->all();
        DB::transaction(function () use ($inputs) {
            $total = count($inputs['names']);
            $input['school_id'] = Auth::id();
            $input['course_id'] = $inputs['course_id'];
            $input['section_id'] = $inputs['section_id'];
            $input['session'] = $inputs['session'];
            for ($i = 0; $i < $total; $i++) {
                $input['name'] = $inputs['names'][$i];
                $input['roll_no'] = $inputs['rolls'][$i];
                $input['mobile'] = $inputs['mobiles_'][$i];
                $input['email'] = $inputs['emails_'][$i];
                $input['password'] = Hash::make($inputs['passs_'][$i]);
                $user = User::create($input);
                $user->attachRole(4);
                $input['user_id'] = $user->id;
                $input['added_by'] = $input['school_id'];
                $input['updated_by'] = $input['school_id'];
                Student::create($input);
            }
        });
        return redirect(route('student.index'));
    }

    public function emailvalidation(Request $request)
    {
        $emails = $request->get('emails');
        $data = array();
        $data['error'] = 0;
        foreach ($emails as $email) {
            $user = User::where('email', $email)->first();
            if (count($user)) {
                $data['error'] = 1;
                $data['email'][] = $email;
            }
        }
        return $data;
        exit;
    }

    public function stdhomework(Request $request)
    {
        $requestData = $request->all();
        $error = 1;
        $msg = 'Select The Following To View Routine';
        $qDate = $request->get('cDate');
        $cDate = Carbon::now();
        if (empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())) {
            $qDate = $cDate->toDateString();
        }

        $session = Acsession::where('title', date('Y', strtotime($qDate)))->first();
        $year = $session->id;

        $user_id = Auth::id();
        $obj = new HomeController();
        $school_id = $obj->getMyInstituteId();
        if (Auth::user()->hasRole('parent')) {
            $studentGuardian = Stdgurdian::whereGuardianId(Auth::id())->first();
            $student = Student::whereUserId($studentGuardian->student_id)->first();

        } else {
            $student = Student::where('user_id', $user_id)->first();
        }
//        dd($student);
        $day = date('l', strtotime($qDate));
        $cDay = Routineday::where('title', $day)
            ->where('school_id', $school_id)
            ->where('title', $day)
            ->get();
        $atnDate = Atnd_date::where('date_str', strtotime($qDate))->first();
        if (!count($atnDate)) {
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }

        if (!empty(count($cDay))) {
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->where('day_id', $cDay[0]->id)
                ->where('course_id', $student->course_id)
                ->where('section_id', $student->section_id)
                ->get();
            if (!count($routine)) {
                $error = 1;
                $msg = 'No Class Available';

                return view('routin.homework', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::where('id', $year)->first();
                $msg = 'Homework For ' . $qDate . '';
                $days = $cDay;
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->orWhere('school_id', 1)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->orWhere('school_id', 1)->get();

                return view('student.homework', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects', 'cDate', 'atnDate', 'qDate', 'routine'));
            }
        } else {
            $msg = 'No Routine Available In Your Selecting Date';
        }

        return view('student.homework', compact('sessions', 'msg', 'days', 'courses', 'qDate', 'error'));
    }


    public function stdattendance(Request $request)
    {
        $requestData = $request->all();
        $msg = 'Select The Following To View Routine';
        $qDate = $request->get('cDate');
        $cDate = Carbon::now();
        if (empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())) {
            $qDate = $cDate->toDateString();
        }

        $session = Acsession::where('title', date('Y', strtotime($qDate)))->first();
        $year = $session->id;
        $user_id = Auth::id();

        $obj = new HomeController();
        $school_id = $obj->getMyInstituteId();
        if (Auth::user()->hasRole('parent')) {
            $studentGuardian = Stdgurdian::whereGuardianId(Auth::id())->first();
            $user_id =$studentGuardian->student_id;
            $student = Student::whereUserId($user_id)->first();

        } else {
            $student = Student::where('user_id', $user_id)->first();
        }
        $day = date('l', strtotime($qDate));
        $cDay = Routineday::where('title', $day)
            ->where('school_id', $school_id)
            ->where('title', $day)
            ->get();
        $atnDate = Atnd_date::where('date_str', strtotime($qDate))->first();
        if (!count($atnDate)) {
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }
        //dd($cDay[0]);
        if (!empty($cDay)) {
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->where('day_id', $cDay[0]->id)
                ->where('course_id', $student->course_id)
                ->where('section_id', $student->section_id)
                ->get();
            if (!count($routine)) {
                $error = 1;
                $msg = 'No Class Available';

                return view('routin.attendance', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::where('id', $year)->first();
                $msg = 'Attendance For ' . $qDate . '';
                $days = $cDay;
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->orWhere('school_id', 1)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->orWhere('school_id', 1)->get();

                return view('student.attendance', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects', 'cDate', 'atnDate', 'qDate', 'routine', 'user_id'));
            }
        }

        return view('student.attendance', compact('sessions', 'msg', 'days', 'courses', 'qDate'));
    }

    public function sectionstd(Request $request)
    {
        $input = $request->all();
        $stds = '';
        $students = Student::where('course_id', $input['course_id'])
            ->where('section_id', $input['section_id'])
            ->where('session', $input['session'])
            ->orderBy('roll_no', 'asc')
            ->get();
        $stds .= '<option value="">Select Student</option>';
        if (count($students)) {
            foreach ($students as $student) {
                $stds .= '<option value="' . $student->user_id . '">' . $student->roll_no . '-' . $student->name . '</option>';
            }
        }
        return $stds;
        exit;
    }
}
