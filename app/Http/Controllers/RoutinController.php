<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Routin;
use Illuminate\Http\Request;
use Session;
use App\Acsession;
use Auth;
use App\Routineday;
use App\Routineperiod;
use App\Course;
use App\Teacher;
use App\Subject;
use DB;
use Carbon\Carbon;
use App\Atnd_date;

class RoutinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $year = $request->get('year');
        $msg = 'Select Year To View Routine';
        $sessions = Acsession::orderBy('id', 'desc')->pluck('title', 'id');
        if (!empty($year)) {
            $school_id = Auth::id();
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            //dd($routine);
            if (!$routine) {
                $error = 1;
                $msg = 'No Routine Available. Please Add Routine First';

                return view('routin.index', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::where('id', $year)->first();
                $msg = 'Class Routine For The Year ' . $msYr->title . '';
                $days = Routineday::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->get();
                return view('routin.index', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects'));
            }
        }

        return view('routin.index', compact('sessions', 'msg'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create(Request $request)
    {
        $year = $request->get('year');
        $sessions = Acsession::orderBy('id', 'desc')->pluck('title', 'id');
        if (!empty($year)) {
            $school_id = Auth::id();
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            //dd($routine);
            if ($routine) {
                $error = 1;
                $msg = 'Routine Already Added You Can Modify';

                return view('routin.create', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msg = 'Routine Generation For The Year 2017';
                $days = Routineday::where('school_id', $school_id)
                    ->where('session', $year)
                    ->orderBy('order', 'asc')
                    ->get();
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->orWhere('school_id', 1)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->orWhere('school_id', 1)->get();
                return view('routin.create', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects'));
            }
        }

        return view('routin.create', compact('sessions'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {

        $requestData = $request->all();
        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school) {
            $requestData['school_id'] = $school->id;
            $requestData['added_by'] = $school->id;
            $requestData['updated_by'] = $school->id;
            $dTotal = count($requestData['day_ids']);
            for ($d = 0; $d < $dTotal; $d++) {
                $pTotal = count($requestData['day_ids'][$d]);
                for ($p = 0; $p < $pTotal; $p++) {
                    $cTotal = count($requestData['day_ids'][$d][$p]);
                    for ($c = 0; $c < $cTotal; $c++) {
                        $sTotal = count($requestData['day_ids'][$d][$p][$c]);
                        for ($s = 0; $s < $sTotal; $s++) {
                            $requestData['day_id'] = $requestData['day_ids'][$d][$p][$c][$s];
                            $requestData['period_id'] = $requestData['period_ids'][$d][$p][$c][$s];
                            $requestData['course_id'] = $requestData['course_ids'][$d][$p][$c][$s];
                            $requestData['section_id'] = $requestData['sections_ids'][$d][$p][$c][$s];
                            if (isset($requestData['teacher_ids' . $d . $p . $c . $s])) {
                                $requestData['teacher_id'] = $requestData['teacher_ids' . $d . $p . $c . $s];
                            } else {
                                $requestData['teacher_id'] = null;
                            }
                            if (isset($requestData['subject_ids' . $d . $p . $c . $s])) {
                                $requestData['subject_id'] = $requestData['subject_ids' . $d . $p . $c . $s];
                            } else {
                                $requestData['subject_id'] = null;
                            }
                            Routin::create($requestData);
                        }
                    }
                }
            }
        });
        Session::flash('flash_message', 'Routin added!');

        return redirect('dashboard/routin/filter/byparameter');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $routin = Routin::findOrFail($id);

        return view('routin.show', compact('routin'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $year = $id;
        $sessions = Acsession::orderBy('id', 'desc')->pluck('title', 'id');
        if (!empty($year)) {
            $school_id = Auth::id();
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            if (!$routine) {
                $error = 1;
                $msg = 'Routine Not Yet Added. Please Add Routine First';

                return view('routin.create', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::find($id)->first();
                $msg = 'Routine Upgration For The Year ' . $msYr['title'] . '';
                $days = Routineday::where('school_id', $school_id)
                    ->where('session', $year)
                    ->orderBy('order', 'asc')
                    ->get();
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->orWhere('school_id', 1)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->orWhere('school_id', 1)->get();
                return view('routin.edit', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects', 'msYr'));
            }
        }

        return view('routin.create', compact('sessions'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {

        $requestData = $request->all();
        $school = Auth::user();
        DB::transaction(function () use ($requestData, $request, $school) {
            $requestData['school_id'] = $school->id;
            $requestData['added_by'] = $school->id;
            $requestData['updated_by'] = $school->id;
            $dTotal = count($requestData['day_ids']);
            for ($d = 0; $d < $dTotal; $d++) {
                $pTotal = count($requestData['day_ids'][$d]);
                for ($p = 0; $p < $pTotal; $p++) {
                    $cTotal = count($requestData['day_ids'][$d][$p]);
                    for ($c = 0; $c < $cTotal; $c++) {
                        $sTotal = count($requestData['day_ids'][$d][$p][$c]);
                        for ($s = 0; $s < $sTotal; $s++) {
                            $requestData['day_id'] = $requestData['day_ids'][$d][$p][$c][$s];
                            $requestData['id'] = $requestData['ids'][$d][$p][$c][$s];
                            if (isset($requestData['teacher_ids' . $d . $p . $c . $s])) {
                                $requestData['teacher_id'] = $requestData['teacher_ids' . $d . $p . $c . $s];
                            } else {
                                $requestData['teacher_id'] = null;
                            }
                            if (isset($requestData['subject_ids' . $d . $p . $c . $s])) {
                                $requestData['subject_id'] = $requestData['subject_ids' . $d . $p . $c . $s];
                            } else {
                                $requestData['subject_id'] = null;
                            }
                            if (isset($requestData['id'])) {
                                $routine = Routin::findOrFail($requestData['id']);
                                $routine->update(array(
                                        'teacher_id' => $requestData['teacher_id'],
                                        'subject_id' => $requestData['subject_id']
                                    )
                                );
                            } else {
                                $requestData['period_id'] = $requestData['period_ids'][$d][$p][$c][$s];
                                $requestData['course_id'] = $requestData['course_ids'][$d][$p][$c][$s];
                                $requestData['section_id'] = $requestData['sections_ids'][$d][$p][$c][$s];
                                Routin::create($requestData);
                            }
                        }
                    }
                }
            }
        });
        Session::flash('flash_message', 'Routin Updated!');

        return redirect('dashboard/routin/filter/byparameter');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Routin::destroy($id);

        Session::flash('flash_message', 'Routin deleted!');

        return redirect('dashboard/routin');
    }


    public function modify(Request $request)
    {
        $year = $request->get('year');
        $sessions = Acsession::orderBy('id', 'desc')->pluck('title', 'id');
        if (!empty($year)) {
            $school_id = Auth::id();
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            if (!$routine) {
                $error = 1;
                $msg = 'Routine Not Yet Added. Please Add Routine First';

                return view('routin.modify', compact('sessions', 'error', 'msg'));
            } else {
                return redirect(route('routin.edit', ['id' => $year]));
            }


        }
        return view('routin.modify', compact('sessions'));

    }

    public function filter(Request $request)
    {
        $requestData = $request->all();
        if (isset($requestData['session']))
            $year = $requestData['session'];
        $school_id = Auth::id();
//        dd($school_id);
        $msg = 'Select The Following To View Routine';
        $sessions = Acsession::orderBy('id', 'desc')->pluck('title', 'id');
        $days = Routineday::orderBy('id', 'asc')->where('school_id', $school_id)->pluck('title', 'id');
        $courses = Course::orderBy('id', 'asc')
            ->where('school_id', $school_id)
            ->orWhere('school_id', 1)
            ->pluck('title', 'id');
        if (!empty($year)) {
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            if (!$routine) {
                $error = 1;
                $msg = 'No Routine Available. Please Add Routine First';

                return view('routin.index', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::where('id', $year)->first();
                $msg = 'Class Routine For The Year ' . $msYr->title . '';
                if (!isset($requestData['day_id'])) {
                    $days = Routineday::where('school_id', $school_id)
                        ->where('session', $year)
                        ->orderBy('order', 'asc')
                        ->get();
                } else {
                    $days = Routineday::where('school_id', $school_id)
                        ->where('session', $year)
                        ->where('id', $requestData['day_id'])
                        ->get();
                }

                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)
                    ->orWhere('school_id', 1)
                    ->pluck('title', 'id');
                if (!isset($requestData['course_id'])) {
                    $courses = Course::where('school_id', $school_id)
                        ->orWhere('school_id', 1)
                        ->get();
                } else {
                    $courses = Course::where('school_id', $school_id)
                        ->orWhere('school_id', 1)
                        ->where('id', $requestData['course_id'])
                        ->get();
                }
                return view('routin.index', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects'));
            }
        }

        return view('routin.filter', compact('sessions', 'msg', 'days', 'courses'));
    }

    public function monitor(Request $request)
    {
        $requestData = $request->all();
//        dd($requestData);
        $msg = 'Select The Following To View Routine';
        $qDate = $request->get('cDate');
        $cDate = Carbon::now();

        if (empty($qDate) || strtotime($qDate) > strtotime($cDate->toDateString())) {
            $qDate = $cDate->toDateString();
        }
        $session = Acsession::where('title', date('Y', strtotime($qDate)))->first();
        $year = $session->id;
        $school_id = Auth::id();
        $day = date('l', strtotime($qDate));
//        dd($day);
        $cDay = Routineday::where('title', $day)
            ->where('school_id', $school_id)
            ->get();

        $atnDate = Atnd_date::where('date_str', strtotime($qDate))->first();
        if (!count($atnDate)) {
            $atnDate = new Atnd_date;
            $atnDate->date_text = $qDate;
            $atnDate->date_str = strtotime($qDate);
            $atnDate->save();
        }

        if (!empty($cDay)) {
            $routine = Routin::where('school_id', $school_id)
                ->where('session', $year)
                ->count();
            if (!$routine) {
                $error = 1;
                $msg = 'No Routine Available. Please Add Routine First';
                return view('routin.index', compact('sessions', 'msg', 'error', 'year'));
            } else {
                $error = 0;
                $msYr = Acsession::where('id', $year)->first();
                $msg = 'Class Monitoring For ' . $qDate . '';
                $days = $cDay;
                $periods = Routineperiod::where('school_id', $school_id)
                    ->where('session', $year)
                    ->get();
                $teachers = Teacher::where('school_id', $school_id)->pluck('name', 'user_id');
                $subjects = Subject::where('school_id', $school_id)->orWhere('school_id', 1)->pluck('title', 'id');
                $courses = Course::where('school_id', $school_id)->orWhere('school_id', 1)->get();
//                dd($courses);

                return view('routin.monitoring', compact('sessions', 'msg', 'error', 'year', 'days', 'periods', 'courses', 'teachers', 'subjects', 'cDate', 'atnDate', 'qDate'));
            }
        }

        return view('routin.monitoring', compact('sessions', 'msg', 'days', 'courses', 'qDate'));
    }

}
