<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stdgurdian extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stdgurdians';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['guardian_id', 'student_id', 'relation_with', 'added_by', 'updated_by'];

    public function student()
    {
        return $this->belongsTo('App\User','student_id','id');
    }

}
