<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'subjects';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'course_id', 'title', 'code', 'sub_group', 'marks', 'mcq', 'written', 'practical', 'added_by', 'update_by'];

    public function course()
    {
        return $this->belongsTo('App\Course','course_id','id');
    }
}
