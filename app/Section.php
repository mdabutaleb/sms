<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Section extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sections';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'course_id', 'title', 'aliase', 'order'];

    public function course()
    {
        return $this->belongsTo('App\Course','course_id','id');
    }

    public function periodsubject()
    {
        return $this->hasMany('App\Routin','section_id','id');
    }
    public function students(){
        return $this->hasMany('App\Student','section_id','id')->orderBy('roll_no','asc');
    }

    public function totalStd(){
        return $this->hasOne('App\Coursestudent','section_id','id');
    }
}
