<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atnd_date extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'atnd_dates';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    protected $fillable = ['date_text', 'date_str'];
}
