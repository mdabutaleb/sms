<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teachers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'user_id', 'name', 'bn_name', 'religion', 'blood_group', 'designition', 'teach_subject', 'nationality', 'nid', 'gender', 'dob', 'joining_date', 'mobile', 'email', 'mpo_index', 'image', 'staff_order', 'education', 'training', 'ex_school', 'others', 'added_by', 'updated_by'];

    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    
}
