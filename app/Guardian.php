<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Guardian extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'guardians';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'user_id', 'mobile','photo',  'address', 'added_by', 'updated_by'];
    
    public function user()
    {
        return $this->belongsTo('App\User','user_id','id');
    }

    public function students()
    {
        return $this->hasMany('App\Stdgurdian','guardian_id','user_id');
    }    
}
