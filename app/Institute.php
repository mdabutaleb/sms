<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Institute extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'institutes';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'alise', 'ins_code', 'clg_code', 'clg_code', 'ins_eiin', 'ins_vill', 'ins_post', 'ins_upazilla', 'ins_zilla', 'directory', 'ins_mobile', 'email', 'website', 'facebook', 'twitter', 'youtube', 'user_id', 'district_id', 'logo', 'principle_name', 'principle_phone', 'principle_email', 'principle_img'];


    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function district()
    {
        return $this->belongsTo('App\District', 'district_id');
    }

    public function students()
    {
        return $this->hasMany('App\Student', 'school_id', 'user_id');
    }
    public function teachers()
    {
        return $this->hasMany('App\Teacher', 'school_id', 'user_id');
    }

    public function attends(){
        return $this->hasMany('App\Atnd_info', 'school_id', 'user_id');
    }


}
