<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Atnd_info extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'atnd_infos';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'user_id', 'routine_id', 'date_id', 'status', 'added_by', 'updated_by'];

    public function routine()
    {
        return $this->belongsTo('App\Routin', 'routine_id');
    }
}
