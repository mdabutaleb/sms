<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Acsession extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acsessions';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'title', 'start_date', 'end_date', 'added_by', 'updated_by'];

    
}
