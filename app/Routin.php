<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routins';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'session', 'day_id', 'period_id', 'teacher_id', 'subject_id', 'course_id', 'section_id', 'added_by', 'updated_by'];

    
    public function teacher()
    {
        return $this->belongsTo('App\Teacher','teacher_id','user_id');
    }

    public function subject()
    {
        return $this->belongsTo('App\Subject','subject_id','id');
    }

    public function course()
    {
        return $this->belongsTo('App\Course','course_id','id');
    }
    public function section()
    {
        return $this->belongsTo('App\Section','section_id','id');
    }

    public function period()
    {
        return $this->belongsTo('App\Routineperiod','period_id','id');
    }

    public function attendances(){
        return $this->hasMany('App\Atnd_info','routine_id','id');
    }

    public function homeworks(){
        return $this->hasMany('App\Homework','routine_id','id');
    }
}
