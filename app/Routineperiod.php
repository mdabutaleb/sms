<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Routineperiod extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'routineperiods';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['school_id', 'session', 'title', 'start_time', 'end_time', 'start_str', 'end_str'];

    public function acsession()
    {
        return $this->belongsTo('App\Acsession','session','id');
    }
}
