<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;

class User extends Authenticatable
{
    use Notifiable, EntrustUserTrait;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function userrole()
    {
        return $this->hasOne('App\Roleuser', 'user_id', 'id');
    }

    public function institute()
    {
        return $this->hasOne('App\Institute', 'user_id', 'id');
    }


    public function teacher()
    {
        return $this->hasOne('App\Teacher', 'user_id', 'id');
    }

    public function student()
    {
        return $this->hasOne('App\Student', 'user_id', 'id');
    }
    public function distadmin(){
        return $this->hasOne('App\Distadmin', 'user_id');
    }

    public function guardian(){
    	return $this->hasOne('App\Guardian', 'user_id');
    }



}
