<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roleuser extends Model
{
    protected $table = 'role_user';
    public $timestamps = false;
    protected $fillable = ['user_id','role_id'];

    public function rolename(){
        return $this->hasOne('App\Role','id','role_id');
    }
}
