<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Teacheractivity extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'teacher_activities';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['user_id', 'school_id', 'content', 'due_date', 'status'];

    
}
