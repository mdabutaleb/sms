<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/', function () {
	return view( 'welcome' );
	// return redirect('/login');
} );

Route::group( [ 'middleware' => 'auth', 'prefix' => 'dashboard' ], function () {
	Route::get( '/home', [ 'uses' => 'HomeController@index' ] );
	//Main Admin Acees Routes
	Route::group( [ 'middleware' => [ 'role:owner' ] ], function () {
		Route::resource( 'institute', 'InstituteController' );
		Route::resource( '/acsession', 'AcsessionController' );
		Route::resource( '/district', 'DistrictController' );
		Route::resource( '/distadmin', 'DistadminController' );
	} );
	//Routes For Combined School and Owner
	Route::group( [ 'middleware' => [ 'role:school|owner' ] ], function () {
		Route::resource( '/course', 'CourseController' );
		Route::resource( '/subject', 'SubjectController' );
	} );
	//Routes for School Admin
	Route::group( [ 'middleware' => [ 'role:school' ] ], function () {
		Route::resource( '/section', 'SectionController' );

		Route::resource( '/teacher', 'TeacherController' );
		Route::get( '/teacher/import/fromfile', [ 'as' => 'teacher.import', 'uses' => 'TeacherController@import' ] );
		Route::post( '/teacher/import/fromfile', [
			'as'   => 'teacher.import.store',
			'uses' => 'TeacherController@importstore'
		] );
		Route::post( '/teacher/import/store', [
			'as'   => 'teacher.import.storedb',
			'uses' => 'TeacherController@importstoredb'
		] );

		Route::resource( '/student', 'StudentController' );
		Route::get( '/student/import/fromfile', [ 'as' => 'student.import', 'uses' => 'StudentController@import' ] );
		Route::post( '/student/import/fromfile', [
			'as'   => 'student.import.store',
			'uses' => 'StudentController@importstore'
		] );
		Route::post( '/student/import/store', [
			'as'   => 'student.import.storedb',
			'uses' => 'StudentController@importstoredb'
		] );

		Route::resource( '/coursestudent', 'CoursestudentController' );

		/*routine Routes*/
		Route::resource( '/routinedays', 'RoutinedaysController' );
		Route::resource( '/routineperiod', 'RoutineperiodController' );
		Route::resource( '/routin', 'RoutinController' );
		Route::get( 'routin/modify/yearly', [ 'as' => 'routine.modify', 'uses' => 'RoutinController@modify' ] );
		Route::get( 'routin/filter/byparameter', [ 'as' => 'routine.filter', 'uses' => 'RoutinController@filter' ] );
		Route::post( 'routin/filter/byparameter', [
			'as'   => 'routine.filter.show',
			'uses' => 'RoutinController@filter'
		] );
		Route::get( 'routin/monitor/byparameter', [ 'as' => 'routine.monitor', 'uses' => 'RoutinController@monitor' ] );
		/*Guardians Routes*/
		Route::resource( 'guardian', 'GuardianController' );
		Route::resource( 'notice', 'NoticeController' );
	} );
	//Routes For Teacher
	Route::group( [ 'middleware' => [ 'role:teacher' ] ], function () {
		/*Attendance Route */
		Route::resource( '/atnd_info', 'Atnd_infoController' );
		Route::get( '/atnd_info/add/{date}/{routine}', [
			'as'   => 'atnd_info.add',
			'uses' => 'Atnd_infoController@addAttendance'
		] );
		Route::post( '/atnd_info/add/store', [
			'as'   => 'atnd_info.add.store',
			'uses' => 'Atnd_infoController@storeAttendance'
		] );
		Route::get( '/atnd_info/edit/{date}/{routine}', [
			'as'   => 'atnd_info.edit',
			'uses' => 'Atnd_infoController@editAttendance'
		] );
		Route::post( '/atnd_info/edit/store', [
			'as'   => 'atnd_info.update',
			'uses' => 'Atnd_infoController@updateAttendance'
		] );
		//Home Work
		Route::resource( '/homework', 'HomeworkController' );
		Route::get( '/homework/add/{date}/{routine}', [
			'as'   => 'homework.add',
			'uses' => 'HomeworkController@addHomework'
		] );
		Route::post( '/homework/add/store', [
			'as'   => 'atnd_info.add.store',
			'uses' => 'Atnd_infoController@storeAttendance'
		] );
		Route::resource( '/teacheractivities', 'TeacheractivitiesController' );
	} );

	//Routes For Student
	Route::group( [ 'middleware' => [ 'role:student|parent' ] ], function () {
		Route::get( '/student/admin/homework', [
			'as'   => 'student.dashboard.homework',
			'uses' => 'StudentController@stdhomework'
		] );
		Route::get( '/student/admin/attendance', [
			'as'   => 'student.dashboard.attendance',
			'uses' => 'StudentController@stdattendance'
		] );

	} );

	Route::group( [ 'middleware' => [ 'role:distadmin' ] ], function () {
		Route::get( 'school/{id}', 'HomeController@getSchoolInformation' );
		Route::resource( 'remarks', 'RemarksController' );
	} );
	Route::get( 'profile/{id}/{name}', 'ProfileSettingController@show' );
	Route::get( 'profile/{id}/edit/{name}', 'ProfileSettingController@edit' );
	Route::PATCH( 'profile/{id}/update/{name}', 'ProfileSettingController@update' );
} );

Route::get( '/sectionbyclass', [ 'as' => 'sectionbyclass', 'uses' => 'SectionController@sectionbyclass' ] );
Route::get( '/sectionstd', [ 'as' => 'sectionstd', 'uses' => 'StudentController@sectionstd' ] );
Route::get( '/emailvalidation', [ 'as' => 'emailvalidation', 'uses' => 'StudentController@emailvalidation' ] );