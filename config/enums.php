<?php
$timestamp = strtotime('next saturday');
$days = array();
for ($i = 0; $i < 7; $i++) {
    $days[strftime('%A', $timestamp)] = strftime('%A', $timestamp);
    $timestamp = strtotime('+1 day', $timestamp);
}
return [
	'religion' => [
        'Islam' => "Islam",
        'Hinduism' => "Hinduism",
        'Buddhism' => "Buddhism",
        'Christianity' => "Christianity",
        'Other' => "Other",
    ],
    'group' => [
        'general' => "General",
        'science' => "Science",
        'Humanities' => "Arts",
        'Business Studies' => "Commerce",
        'vocational' => "Vocational",
    ],
    'types' => [
        'head' => "Head Master/ Principal",
        'assistant_head' => "Assistant Head",
        'assistant' => "Assistant Teacher",
        'staffs' => "Staffs",
        'ex_head' => "Ex Head",
        'ex_teacher' => "Ex Teacher",
    ],

    'blood' => [
        'A+' => "A+",
        'A-' => "A-",
        'B+' => "B+",
        'B-' => "B-",
        'O+' => "O+",
        'O-' => "O-",
        'AB+' => "AB+",
        'AB-' => "AB-",
    ],

    'status' => [
        '1' => "Active",
        '0' => "Ex",
    ],

    'acyear' => [
        '2015' => "2015",
        '2016' => "2016",
        '2017' => "2017",
        '2018' => "2018",
        '2019' => "2019",
        '2020' => "2021",
        '2022' => "2022",
        '2023' => "2023",
        '2024' => "2014",
        '2025' => "2015",
    ],

    'sub_types' => [
        'compulsory' => "Compulsory",
        'obtional'   => "Obtional"
    ],

    'gender' => [
        'Male'     => "Male",
        'Female'   => "Female",
        'Others'   => "Others"
    ],

    'purpose_type' => [
        'debit'     => "Debit",
        'credit'   => "Credit"
    ],
    'shift' => [
        '0'     => "Morning",
        'Day'   => "Day",
    ],
    'days' => $days,

];
