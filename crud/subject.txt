php artisan crud:generate Subject --fields="school_id#integer#unsigned; course_id#integer#unsigned; title#string; code#string; group#string; marks#smallint#unsigned; mcq#smallint#unsigned; written#smallint#unsigned; practical#smallint; added_by#integer#unsigned;update_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard




//Teacher Crud

php artisan crud:generate Teacher --fields="school_id#integer#unsigned; user_id#integer#unsigned; name#string; bn_name#string; religion#text; blood_group#text; designition#string; teach_subject#string; nationality#string; nid#text; gender#string; dob#string; joining_date#string; mobile#string; email#string; mpo_index#string; image#string; staff_order#string; education#longtext; training#longtext; ex_school#string; others#string; added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard

//Student Crud

php artisan crud:generate Student --fields="school_id#integer#unsigned; user_id#integer#unsigned; name#string; bn_name#string; religion#text; blood_group#text; gender#string; dob#string; course_id#integer#unsigned; section_id#integer#unsigned; shift_id#integer#unsigned; session#integer#unsigned;  roll_no#mediumint#unsigned; mobile#string; email#string; image#string; third_subject#integer#unsigned;fourth_subject#integer#unsigned; status#boolean; public_status#boolean; previous_school#string; others#string; parent#integer#unsigned; added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard

//Seesion Crud
php artisan crud:generate Session --fields="school_id#integer#unsigned; title#string; start_date#string; end_date#string;  added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard

//Seesion Crud
php artisan crud:generate Coursestudent --fields="school_id#integer#unsigned; session#integer#unsigned; course_id#integer#unsigned; section_id#integer#unsigned; total_student#mediumint#unsigned;" --view-path= --controller-namespace= --route-group=dashboard

//Days Crud
php artisan crud:generate Routinedays --fields="school_id#integer#unsigned; session#integer#unsigned; title#string; order#mediumint#unsigned;" --view-path= --controller-namespace= --route-group=dashboard

//Periods Crud
php artisan crud:generate Routineperiod --fields="school_id#integer#unsigned; session#integer#unsigned; title#string; start_time#time; end_time#time; start_str#integer#unsigned; end_str#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard


//Routine Crud
php artisan crud:generate Routin --fields="school_id#integer#unsigned; session#integer#unsigned; day_id#integer#unsigned; period_id#integer#unsigned; teacher_id#integer#unsigned; subject_id#integer#unsigned;  added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard

//Home Work Crud
php artisan crud:generate Homework --fields="school_id#integer#unsigned; routine_id#integer#unsigned; date_id#integer#unsigned; status#boolean; subject_id#integer#unsigned; home_work#text; added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard

Guardian Crud
===========================

php artisan crud:generate Guardian --fields="school_id#integer#unsigned; user_id#integer#unsigned; mobile#string; address#longtext; added_by#integer#unsigned; updated_by#integer#unsigned" --view-path= --controller-namespace= --route-group=dashboard


District Crud
===========================

php artisan crud:generate District --fields="name#string" --view-path= --controller-namespace= --route-group=dashboard


District Admin Crud
===========================
php artisan crud:generate Distadmin --fields="district_id#mediumint#unsigned; user_id#integer#unsigned; photo#string;" --view-path= --controller-namespace= --route-group=dashboard


Notice Crud
===========================
php artisan crud:generate Notice --fields="school_id#integer#unsigned; content#text" --view-path= --controller-namespace= --route-group=dashboard


District Crud
===========================
php artisan crud:generate Remarks --fields="school_id#integer#unsigned; district_id#integer#unsigned; content#text" --view-path= --controller-namespace= --route-group=dashboard


Teacher Activites Crud
==========================
php artisan crud:generate Teacheractivities --fields="user_id#integer#unsigned; school_id#integer#unsigned; content#text; due_date#date" --view-path= --controller-namespace= --route-group=dashboard

