jQuery(document).ready(function(){
	jQuery('.datepicker').datepicker({format: 'yyyy-mm-dd'}
    );
    jQuery('#course_id').change(function() {
		//alert('I am Change');
		jQuery('#section_id').html('');
		var courseid = jQuery(this).val();
		//alert(courseid);
		$.ajax({
	     url: ajax_url,
	     data: {id : courseid},
	     type: "get",
	     success: function(data){
	       jQuery('#section_id').html(data);
	     }
	   });
	});
	$('.timepicker').timepicker({
		showSeconds: true,
		maxHours   : 24,
		showMeridian : false,
		minuteStep : 5
	});

	// A $( document ).ready() block.
	var windowWidth = $(window).width();
	if(windowWidth > 767){
		var heights = [];
	    $('.homeWork .attendance').each(function(){
			 	var height = $(this).height();
			 	heights.push(height); 
	    });
	    var max = Math.max.apply(Math,heights);
    }
    $('.homeWork .attendance').css("height",max);
	
});