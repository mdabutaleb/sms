<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class=" image">
	                <?php
	                $default_img = '/img/default.png';
	                $user = Auth::user();

	                if ( Auth::user()->hasRole( 'owner' ) ) {
		                $img = $default_img;
	                } elseif ( Auth::user()->hasRole( 'distadmin' ) ) {
		                $img = ( ! empty( $user->distadmin->photo ) ) ? $user->distadmin->photo : $default_img;

	                } elseif ( Auth::user()->hasRole( 'school' ) ) {
		                $img = ( ! empty( $user->institute->logo ) ) ? $user->institute->logo : $default_img;

	                } elseif ( Auth::user()->hasRole( 'teacher' ) ) {
		                $img = ( ! empty( $user->teacher->image ) ) ? $user->teacher->image : $default_img;

	                } elseif ( Auth::user()->hasRole( 'student' ) ) {
		                $img = ( ! empty( $user->student->image ) ) ? $user->student->image : $default_img;
	                }elseif ( Auth::user()->hasRole( 'parent' ) ) {
		                $img = ( ! empty( $user->guardian->photo ) ) ? $user->guardian->photo : $default_img;

	                } else {
		                $img = $default_img;
	                }
	                ?>

                    <img src="{{ url($img) }}" style="height: auto; width: 100%;" class="" alt=" Image"/>
                </div>
                <div class="info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('adminlte_lang::message.online') }}
                    </a> /
                    <a href="#"> Profile
                    </a>
                </div>
            </div>
    @endif

 

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            
            @if(Auth::user()->hasRole('owner'))
                <li class="treeview">
                    <a href="#"><i class='fa fa-university'></i> <span>Educational Institue </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('institute.create')}}"><i class='fa fa-plus-square'></i>Add Institute</a>
                        </li>
                        <li><a href="{{route('institute.index')}}"><i class='fa fa-edit'></i>Manage Institute</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-graduation-cap'></i> <span>Academic Year</span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('acsession.create')}}"><i class='fa fa-plus-square'></i>Add Year</a></li>
                        <li><a href="{{route('acsession.index')}}"><i class='fa fa-edit'></i>Manage Year</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-bell'></i> <span>Class </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('course.create')}}"><i class='fa fa-plus-square'></i>Add Class</a></li>
                        <li><a href="{{route('course.index')}}"><i class='fa fa-edit'></i>Manage Class</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-book'></i> <span>Subject </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('subject.create')}}"><i class='fa fa-plus-square'></i>Add Subject</a></li>
                        <li><a href="{{route('subject.index')}}"><i class='fa fa-edit'></i>Manage Subject</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="{{route('district.index')}}"><i class='fa fa-file-text'></i> <span> District </span> </a>
                </li>

                <li class="treeview">
                    <a href="{{route('distadmin.index')}}"><i class='fa fa-file-text'></i> <span> District Admin </span>
                    </a>
                </li>
            @endif
            @if(Auth::user()->hasRole('school'))
                <li class="treeview">
                    <a href="#"><i class='fa fa-bell'></i> <span>Class </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('course.create')}}"><i class='fa fa-plus-square'></i>Add Class</a></li>
                        <li><a href="{{route('course.index')}}"><i class='fa fa-edit'></i>Manage Class</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-certificate'></i> <span>Section </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('section.create')}}"><i class='fa fa-plus-square'></i>Add Section</a></li>
                        <li><a href="{{route('section.index')}}"><i class='fa fa-edit'></i>Manage Section</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-book'></i> <span>Subject </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('subject.create')}}"><i class='fa fa-plus-square'></i>Add Subject</a></li>
                        <li><a href="{{route('subject.index')}}"><i class='fa fa-edit'></i>Manage Subject</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-male'></i> <span>Teacher </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('teacher.create')}}"><i class='fa fa-plus-square'></i>Add Teacher</a></li>
                        <li><a href="{{route('teacher.index')}}"><i class='fa fa-edit'></i>Manage Teacher</a></li>
                        <li><a href="{{route('teacher.import')}}"><i class="fa fa-download" aria-hidden="true"></i></i>
                                Import Teacher</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa fa-address-book'></i> <span>Student </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('student.create')}}"><i class='fa fa-plus-square'></i>Add Student</a></li>
                        <li><a href="{{route('student.index')}}"><i class='fa fa-edit'></i>Manage Student</a></li>
                        <li><a href="{{route('student.import')}}"><i class="fa fa-download" aria-hidden="true"></i>Import
                                Student</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa fa-user-circle'></i> <span>Guardian </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('guardian.create')}}"><i class='fa fa-plus-square'></i>Add Guardian</a>
                        </li>
                        <li><a href="{{route('guardian.index')}}"><i class='fa fa-edit'></i>Manage Guardian</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="#"><i class='fa fa-hdd-o'></i> <span>Student Statistics </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('coursestudent.create')}}"><i class='fa fa-plus-square'></i>Add Statistics</a>
                        </li>
                        <li><a href="{{route('coursestudent.index')}}"><i class='fa fa-edit'></i>Statistics</a></li>
                    </ul>
                </li>
                <li class="treeview">
                    <a href="#"><i class='fa fa-clock-o'></i> <span>Class Routine </span> <i
                                class="fa fa-angle-left pull-right"></i></a>
                    <ul class="treeview-menu">
                        <li><a href="{{route('routinedays.create')}}"><i class='fa fa-plus-square'></i>Add Days</a></li>
                        <li><a href="{{route('routinedays.index')}}"><i class='fa fa-edit'></i>Manage Days</a></li>
                        <li><a href="{{route('routineperiod.create')}}"><i class='fa fa-plus-square'></i>Add Periods</a>
                        </li>
                        <li><a href="{{route('routineperiod.index')}}"><i class='fa fa-edit'></i>Manage Periods</a></li>

                        <li><a href="{{route('routin.create')}}"><i class='fa fa-edit'></i>Create Routine</a></li>
                        <li><a href="{{route('routine.modify')}}"><i class='fa fa-edit'></i>Update Routine</a></li>
                        <li><a href="{{route('routine.filter')}}"><i class='fa fa-edit'></i>Filter Routine</a></li>
                    </ul>
                </li>

                <li class="treeview">
                    <a href="{{route('routine.monitor')}}"><i class="fa fa-eye" aria-hidden="true"></i>
                        <span>Monitor Class</span> </a>
                </li>
                <li class="treeview">
                    <a href="{{route('notice.index')}}"><i class="fa fa-edit" aria-hidden="true"></i>
                        <span>Notice</span> </a>
                </li>
            @endif

            @if(Auth::user()->hasRole('teacher'))
                <li class="treeview">
                    <a href="{{route('atnd_info.index')}}"><i class='fa fa-bullhorn'></i> <span> Attendance </span> </a>
                </li>
                <li class="treeview">
                    <a href="{{route('homework.index')}}"><i class='fa fa-file-text'></i> <span> Home Work </span> </a>
                </li>
                <li class="treeview">
                    <a href="{{ url('/dashboard/teacheractivities') }}"><i class='fa fa-list'></i> <span> Upcoming Activity </span>
                    </a>
                </li>
            @endif

            @if(Auth::user()->hasRole('student') or Auth::user()->hasRole('parent'))
                <li class="treeview">
                    <a href="{{route('student.dashboard.attendance')}}"><i class='fa fa-bullhorn'></i> <span> Attendance </span>
                    </a>
                </li>
                <li class="treeview">
                    <a href="{{route('student.dashboard.homework')}}"><i class='fa fa-file-text'></i> <span> Home Work </span>
                    </a>
                </li>
            @endif
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
