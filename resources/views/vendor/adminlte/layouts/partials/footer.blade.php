<!-- Main Footer -->
<footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
        Maintained By <b><a href="http://www.brainstation-23.com/" target="_blank">Brainstaion-23 Ltd</a>
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; {{date('Y')}} <a href="http://acacha.org">LMS</a>
</footer>
