<!-- Main Header -->
<header class="main-header">

    <!-- Logo -->
    <a href="{{ url('/home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>L</b>MS</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>LMS</b> </span>
    </a>

    <!-- Header Navbar -->
    <nav class="navbar navbar-static-top" role="navigation">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">{{ trans('adminlte_lang::message.togglenav') }}</span>
        </a>
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (Auth::guest())
                    <li><a href="{{ url('/register') }}">{{ trans('adminlte_lang::message.register') }}</a></li>
                    <li><a href="{{ url('/login') }}">{{ trans('adminlte_lang::message.login') }}</a></li>
                @else
                <!-- User Account Menu -->
                    <li class="dropdown user user-menu" id="user_menu">
                        <!-- Menu Toggle Button -->
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <!-- The user image in the navbar-->
                        {{--<img src="{{ Gravatar::get($user->email) }}" class="user-image" alt="User Image"/>--}}
                        {{--<img src="{{ asset(Auth::user()->institute->logo) }}" class="user-image" alt="User Image"/>--}}
                        <!-- hidden-xs hides the username on small devices so only the image appears. -->
                            <span class="hidden-xs">{{ Auth::user()->name }}</span>
                        </a>
                        <ul class="dropdown-menu">
                            <!-- The user image in the menu -->
                            <li class="user-header">
								<?php
								$default_img = '/img/default.png';
								$user = Auth::user();

								if ( Auth::user()->hasRole( 'owner' ) ) {
									$img = $default_img;
								} elseif ( Auth::user()->hasRole( 'distadmin' ) ) {
									$img = ( ! empty( $user->distadmin->photo ) ) ? $user->distadmin->photo : $default_img;

								} elseif ( Auth::user()->hasRole( 'school' ) ) {
									$img = ( ! empty( $user->institute->logo ) ) ? $user->institute->logo : $default_img;

								} elseif ( Auth::user()->hasRole( 'teacher' ) ) {
									$img = ( ! empty( $user->teacher->image ) ) ? $user->teacher->image : $default_img;

								} elseif ( Auth::user()->hasRole( 'student' ) ) {
									$img = ( ! empty( $user->student->image ) ) ? $user->student->image : $default_img;
								} elseif ( Auth::user()->hasRole( 'parent' ) ) {
									$img = ( ! empty( $user->guardian->photo ) ) ? $user->guardian->photo : $default_img;

								} else {
									$img = $default_img;
								}
								?>

                                <img src="{{ url($img) }}" style="height: auto; width: 100%;" class="" alt=" Image"/>
                                <div class="pull-left">

                                    <a href="{{ url('dashboard/profile/'. $user->id. '/'.str_slug($user->name, '-')) }}">
                                        <button class="btn btn-default btn-flat"> Profile</button>
                                    </a>


                                    {{--<a href="{{ url('/profile/settings') }}"--}}
                                    {{--class="btn btn-default btn-flat">{{ trans('adminlte_lang::message.profile') }}</a>--}}
                                </div>
                                <div class="pull-right">
                                    <a href="{{ url('/logout') }}" class="btn btn-default btn-flat" id="logout"
                                       onclick="event.preventDefault();
                                                 document.getElementById('logout-form').submit();">
                                        {{ trans('adminlte_lang::message.signout') }}
                                    </a>

                                    <form id="logout-form" action="{{ url('/logout') }}" method="POST"
                                          style="display: none;">
                                        {{ csrf_field() }}
                                        <input type="submit" value="logout" style="display: none;">
                                    </form>

                                </div>

                            </li>
                            <br/>
                            <br/>
                            <li class="user-footer">

                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
        </div>
    </nav>
</header>
