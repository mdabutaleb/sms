@extends('layouts.app')
@section('style')
    <style>
        .table {
            margin-top: -30px !important;
        }

        .box.box-primary {
            border-top-color: #62cb31;
        }

        .temp {
            border: 2px solid red;
        }
    </style>
@endsection
@section('htmlheader_title')
    {{ Auth::user()->name}}
@endsection
@section('content')
    {{--This module for district admin--}}
    {{--top icon--}}
    @if($instituteInfo != null)
        {{--//////////////////--}}
        <div class="row">
            <div class="col-md-5">
                {{--Class Routine--}}
                <div class="box_custom ">
                    @if(isset($monitors) && !($monitors['error']))

                        <div class="box box-primary">
                            <h4 style="text-align: center;" class="box_custom_header">
                                Class
                                Routine<br/><span>(<b>{{$monitors['routine'][0]->period->title}}</b> - In-Progress)</span>
                                <br/>
                            </h4>
                        </div>
                        <div class="box_custom_body">

                            <!-- /.box-header -->
                            <div class="box-body no-padding">
                                <br/>
                                @if(!empty($monitors))

                                    <table class="table table-responsive table-striped">
                                        <tbody>
                                        <tr>
                                            <th style="width: 10px">Sts.</th>
                                            <th>Class</th>
                                            <th>Section</th>
                                            <th>Std.</th>
                                            <th>Subject</th>
                                            <th>Teacher</th>
                                        </tr>

                                        @foreach($monitors['routine'] as $routine)


											<?php

											$is_Attend = DB::table('atnd_infos')
                                                       ->whereRaw( 'Date(created_at) = CURDATE()' )
                                                        ->where('routine_id', $routine->id)
											           ->first();

											$totAtnd = count( $routine->attendances->where( 'date_id', $monitors['atnDate']->id )->where( 'status', 1 ) );
											$totStd = count( $routine->section->students );
//											$hWrk = $routine->homeworks->where( 'date_id', $monitors['atnDate']->id )->first();
//											//dd($hWrk);
//											$isTakeAttendence = count( $routine->homeworks ) > 0 ? true : false;
											if ($is_Attend) {
												$circle_color = '#7FBA00';
												$title        = "Attendence taken by " . $routine->teacher->name;
											} else {
												$circle_color = 'gray';
												$title        = "Attendence not taken yet";
											}
											if ( ! empty( $hWrk ) ) {
												$hw = $hWrk->home_work;
											} else {
												$hw = 'Not Provide';
											}
											?>
                                            <tr>
                                                <td>
                                                    <span style="background: {{ $circle_color }};display: block; height: 17px;width: 17px;  border-radius: 50%;"
                                                          title="{{ $title }}"></span>
                                                </td>

                                                <td>{{ $routine->course->title }}</td>
                                                <td>{{$routine->section->title}}</td>
                                                <td>{{ $totAtnd }}({{$totStd}})</td>
                                                <td>
                                                    {{$routine->subject->title}}
                                                </td>
                                                <td>{{$routine->teacher->name}}</td>
                                            </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                @endif
                            </div>
                            <!-- /.box-body -->
                        </div>
                    @else
                        <div class="box box-primary">
                            <h4 style="text-align: center;" class="box_custom_header">
                                No available class at this moment</span>
                                <br/>
                            </h4>
                        </div>
                    @endif
                </div>
                {{--attendence report--}}
                <div class="box_custom">
                    <div class="box box-primary">
                        <h4 style="text-align: center;" class="box_custom_header">
                            Today's Attendence
                        </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box_custom_body">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 text-center">
                                <input type="text" class="knob"
                                       value="{{ $todayReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                            </div>
                            <br/>

                            <div class="col-xs-12 col-sm-6 col-md-6 my-center-text ">
                                <div class="knob-label">
                                    <p class="inline" style="color: #ff931e">
                                        <b>
                                            {{ $todayReport['totalAttend'] }}
                                        </b>

                                    </p>
                                    <p class="inline" style="color: green;"><b>Present</b></p>
                                </div>
                                <p style="color: red;"><b>{{ $todayReport['absent']  }} Absent </b></p>
                            </div>

                        </div>
                        <div class="row">
                            <hr/>

                            <div class="box_custom_header">
                                <h5 style="text-align: center">
                                    Avg. Daily Attendence<br/><br/>
                                </h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                                <input type="text" class="knob"
                                       value="{{ $SevenDayReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">Last 7 Days <b>{{ $SevenDayReport['parcent'] }}%</b>
                                </h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                                <input type="text" class="knob"
                                       value="{{ $oneMonthReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">Last 30 Days<b> {{ $oneMonthReport['parcent'] }}
                                        %</b></h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 text-center">

                                <input type="text" class="knob"
                                       value="{{ $thisYearReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">This Year <b>{{ $thisYearReport['parcent'] }}%</b>
                                </h5>
                            </div>

                        </div>
                        <div class="row">
                            <hr/>
                            <div class="box_custom_header">
                                <h5 style="text-align: center">
                                    Avg. Daily Attendence (District)<br/><br/>
                                </h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                                <input type="text" class="knob"
                                       value="{{ $SevenDayReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">Last 7 Days <b>{{ $SevenDayReport['parcent'] }}%</b>
                                </h5>
                            </div>
                            <div class="col-xs-12 col-sm-4 col-md-4 text-center">
                                <input type="text" class="knob"
                                       value="{{ $oneMonthReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">Last 30 Days<b> {{ $oneMonthReport['parcent'] }}
                                        %</b></h5>
                            </div>
                            <div class="col-xs-12 col-sm-4  col-md-4 text-center">

                                <input type="text" class="knob"
                                       value="{{ $thisYearReport['parcent'] }}"
                                       data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                       data-readonly="true">
                                <h5 style="text-align: center;">This Year <b>{{ $thisYearReport['parcent'] }}%</b>
                                </h5>
                            </div>
                        </div>
                    </div>
                    <!-- /.box-body -->

                </div>
            </div>
            <div class="col-md-4">

                {{--message from distict admin--}}
                <div class="box_custom ">
                    <div class="box box-primary">
                        <h4 style="text-align: center;" class="box_custom_header">
                            Message from District Admin
                        </h4>
                    </div>
                    <div class="box_custom_body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <ul class="todo-list" style="margin-top:15px;">
                            @foreach($remarks as $remark)
                                <li>
                                    <span class="handle"> </span>
                                    <span class="text"> <i class="fa fa-calendar-check-o"
                                                           aria-hidden="true"></i>
                                        {{ $remark->content }}
                                        <small class="label label-danger"><i class="fa fa-clock-o"></i>
                                            {{ $remark->created_at->diffForHumans() }}
                            </small>
                        </span>
                                </li>
                            @endforeach
                        </ul>
                    </div>
                    <!-- /.box-body -->
                    <!-- /.box-body -->
                </div>
                {{--principle--}}
                <div class="box_custom ">
                    <div class="box box-primary">
                        <h4 style="text-align: center;" class="box_custom_header">
                            Notice from the Principle
                        </h4>
                    </div>
                    <div class="box_custom_body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <ul class="todo-list" style="margin-top:15px;">
                            @if(!empty($notices))
                                @foreach($notices as $notice)
                                    <li>
                                        <span class="handle">
                                        </span>
                                        <span class="text">
                                                {{--<i class="fa fa-calendar-check-o" aria-hidden="true"></i>--}}
                                            {{--{{ $notice->created_at->format('') }}--}}
                                            <small class="label label-primary"><i class="fa fa-clock-o"></i>
                                                {{ date_format($notice->created_at, 'jS F Y:') }}
                                                </small>&nbsp;&nbsp;
                                            {{ $notice->content }}

                                            {{--<small class="label label-danger"><i class="fa fa-clock-o"></i>--}}
                                            {{--{{ $notice->created_at->diffForHumans() }}--}}
                                            {{--</small>--}}

                                        </span>

                                    </li>
                                @endforeach

                            @endif
                        </ul>
                    </div>
                </div>
                {{--multi media class status--}}
                <div class="box_custom">
                    <div class="box box-primary">
                        <h4 style="text-align: center;" class="box_custom_header">
                            MULTIMEDIA CLASS CONTRIBUTION
                        </h4>
                    </div>
                    <!-- /.box-header -->
                    <div class="box_custom_body">
                        <div class="">
                            <div class="box-body">

                                <table class="table  multimedia-class">
                                    <tr>
                                        <td><b>My Institute View</b></td>
                                        <td></td>
                                        <td><b>AVG. DISTRICT VIEW</b></td>
                                    </tr>
                                    <tr>
                                        <td class="label-default">12</td>
                                        <td><b>Last Week</b></td>
                                        <td class="label-warning">12</td>
                                    </tr>
                                    <tr>
                                        <td class="label-primary">75</td>
                                        <td><b>Last Month</b></td>
                                        <td class="label-warning">12</td>
                                    </tr>
                                    <tr>
                                        <td class="label-success">112</td>
                                        <td><b>Last Year</b></td>
                                        <td class="label-primary">55</td>
                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            <div class="col-md-3">
                {{--scholl information--}}
                <div class="box_custom ">
                    <div class="box box-primary">
                        <h4 style="text-align: center;" class="box_custom_header">
                            {{ $instituteInfo->name }}
                        </h4>
                    </div>
                    <div class="box_custom_header ">
                        {{--<h5 style="text-align: center;">--}}
                        {{--{{ $instituteInfo->name }}--}}
                        {{--</h5>--}}
                        <span>
                            <i class="fa fa-phone"></i> :
                            {{ $instituteInfo->ins_mobile }}
                        </span> &nbsp; &nbsp; &nbsp;
                        <span>
                        <i class="fa fa-envelope"></i> :
                            {{ $instituteInfo->email }}
                        </span>
                    </div>
                    <!-- /.box-header -->
                    <div class="box_custom_body">
                        <div class="row">
                            <div class="col-xs-4 inner_box border_right">
                                <h5>{{ $todayReport['totalStudents'] }}</h5>
                                <p>STUDENTS</p>
                            </div>
                            <div class="col-xs-4 inner_box border_right">
                                <h5>{{ $totalTeachers }}</h5>
                                <p>TEACHERS</p>
                            </div>
                            <div class="col-xs-4 inner_box">
                                <h5>{{ $todayReport['totalStudents'] }}</h5>
                                <p>PARENTS</p>
                            </div>
                        </div>


                    </div>
                    <!-- /.box-body -->

                </div>
                {{--best performer--}}
                <div class="box_custom ">

                    <p class="inline" style="color: red;"><b> Gazipur Cant board boys school & collage</b></p>
                    <p class="inline">
                        best performer of the week.<br/>
                        <button class="btn btn-default congrats">Say congratulation</button>
                    </p>
                </div>
                {{--best contributor--}}
                <div class="box_custom ">

                    <p class="inline" style="color: red;"><b> RANI BILASH MONI BOYS SCHOOL & COLLAGE</b></p>
                    <p class="inline">
                        best contributor <b>(Multi-Media Class)</b> of the week.<br/>
                        <button class="btn btn-default congrats">Say congratulation</button>
                    </p>
                </div>
            </div>
        </div>

        {{--////////////--}}



    @endif
    <div class="row">
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-aqua"><i class="fa fa-users"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><strong>Total Students</strong></span>
                    <span class="info-box-number"><h1
                                style="text-align: center!important; margin: 0 auto">{{ $todayReport['totalStudents'] }}</h1></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-md-3 col-sm-6 col-xs-12">
            <div class="info-box">
                <span class="info-box-icon bg-green"><i class="fa fa-user"></i></span>

                <div class="info-box-content">
                    <span class="info-box-text"><strong>Total Teachers</strong></span>
                    <span class="info-box-number"><h1
                                style="text-align: center!important; margin: 0 auto">{{ $totalTeachers }}</h1></span>
                </div>
                <!-- /.info-box-content -->
            </div>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        @if(Auth::user()->hasRole('owner') || Auth::user()->hasRole('distadmin'))
            <div class="col-md-3 col-sm-6 col-xs-12">
                <div class="info-box">
                    <span class="info-box-icon bg-yellow"><i class="fa fa-university"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text"><strong>Total Schools</strong></span>
                        <span class="info-box-number"><h1
                                    style="text-align: center!important; margin: 0 auto">{{ $totalSchools }}</h1></span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
                <!-- /.info-box -->
            </div>
            <!-- /.col -->
            @if(Auth::user()->hasRole('owner'))
                <div class="col-md-3 col-sm-6 col-xs-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-red"><i class="fa fa-arrows"></i></span>

                        <div class="info-box-content">
                            <span class="info-box-text"><strong>Total Districts</strong></span>
                            <span class="info-box-number"><h1
                                        style="text-align: center!important; margin: 0 auto">{{ $totalDistrict }}</h1></span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                </div>
        @endif
    @endif
    <!-- /.col -->
    </div>
    <div class="row">
        <div class="box box-primary">
            <div class="box-header">
                <h4 style="text-align: center;">Students Attendence Report</h4>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <div class="row">

                    <div class="col-xs-6 col-md-3 text-center">
                        <h4 style="text-align: center;">Today</h4>
                        <input type="text" class="knob"
                               value="{{ $todayReport['parcent'] }}"
                               data-width="90" data-height="90" data-fgColor="#3c8dbc"
                               data-readonly="true">
                        <div class="knob-label">Total Students : {{ $todayReport['totalStudents'] }}</div>

                        <div class="knob-label"> Todays Present : {{ $todayReport['totalAttend'] }}
                            ( {{ $todayReport['parcent'] }}% )
                        </div>
                        <div class="knob-label"> Absent Today : {{ $todayReport['absent']  }}</div>
                    </div>
                    <div class="col-xs-6 col-md-3 text-center">
                        <h4 style="text-align: center;">Last 7 Days</h4>
                        <input type="text" class="knob"
                               {{--                                   {{ dd($totalAttend7Days) }}--}}
                               value="{{ $SevenDayReport['parcent'] }}"
                               data-width="90" data-height="90" data-fgColor="#3c8dbc"
                               data-readonly="true">
                        <div class="knob-label">7 Days Students : {{ $SevenDayReport['total7DayStudents'] }}</div>
                        <div class="knob-label"> 7 Days Present : {{ $SevenDayReport['total7DayAttend'] }}
                            ( {{ $SevenDayReport['parcent'] }}% )
                        </div>
                        <div class="knob-label"> Absent last 7 days
                            : {{ $SevenDayReport['absent'] }}</div>
                    </div>
                    <div class="col-xs-6 col-md-3 text-center">
                        <h4 style="text-align: center;">Last 30 Days</h4>
                        <input type="text" class="knob"
                               value="{{ $oneMonthReport['parcent'] }}"
                               data-width="90" data-height="90" data-fgColor="#3c8dbc"
                               data-readonly="true">
                        <div class="knob-label">Total Students : {{ $oneMonthReport['total30DayStudents'] }}</div>

                        <div class="knob-label"> Todays Present : {{ $oneMonthReport['total30DayAttend'] }}
                            ( {{ $todayReport['parcent'] }}% )
                        </div>
                        <div class="knob-label"> Absent Today : {{ $oneMonthReport['absent']  }}</div>
                    </div>
                    <div class="col-xs-6 col-md-3 text-center">
                        <h4 style="text-align: center;">This Year</h4>
                        <input type="text" class="knob"
                               value="{{ $thisYearReport['parcent'] }}"
                               data-width="90" data-height="90" data-fgColor="#3c8dbc"
                               data-readonly="true">
                        <div class="knob-label">Total Students : {{ $thisYearReport['totalThisYearStudents'] }}</div>

                        <div class="knob-label"> Todays Present : {{ $thisYearReport['totalThisYearAttend'] }}
                            ( {{ $thisYearReport['parcent'] }}% )
                        </div>
                        <div class="knob-label"> Absent Today : {{ $thisYearReport['absent']  }}</div>
                    </div>
                </div>

            </div>
            <!-- /.box-body -->
        </div>

    </div>
    @if(Auth::user()->hasRole('distadmin'))
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i class="ion ion-clipboard"></i>
                            <h3 class="box-title"> School & Collage List Under
                                <strong>{{ Auth::user()->distadmin->district->name }} </strong></h3>
                        </div>
                        <!-- /.box-header -->
                        <div class="panel-body table-responsive">
                            <table class="table table-hover ">
                                <tr>
                                    <th>ID</th>
                                    <th>Name</th>
                                    <th>Principle Name</th>
                                    <th>Code</th>
                                    <th>Phone</th>
                                    <th> Students</th>
                                    <th>Total Teachers</th>
                                    <th>Address</th>
                                    <th>Action</th>
                                </tr>

                                @if(!empty($institutes))
                                    @foreach($institutes as $institute)
                                        <tr>
                                            <td>{{ $institute->user_id }}</td>
                                            <td>{{ $institute->name }}</td>
                                            <td>
                                                {{ $institute->principle_name }}
                                                <br/>
                                                <label class="label label-default">({{ $institute->principle_phone }}
                                                    )</label>
                                            </td>
                                            <td>{{ $institute->alise }}</td>
                                            {{--<td>{{ $institute->ins_code }}</td>--}}
                                            <td>{{ $institute->ins_mobile }}</td>
                                            <td>
                                                <label class="label label-success"> {{ DB::table('students')->where('school_id', $institute->user_id)->count() }}</label>
                                            </td>
                                            <td>
                                                <label class="label label-primary">{{ DB::table('teachers')->where('school_id', $institute->user_id)->count() }}</label>
                                            </td>
                                            <td> {{ $institute->ins_vill . ", ". $institute->ins_upazilla. ", ". $institute->district->name. '-'. $institute->ins_post}}</td>
                                            <td>
                                                <a href="{{ url('/dashboard/school/'.$institute->user_id)  }}"
                                                   class="label label-success">View Details</a>
                                            </td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                            <div class="pull-right">
                                {{ $institutes->links() }}
                            </div>
                        </div>

                        <!-- /.box-body -->

                    </div>
                </div>
            </div>
        </div>
    @endif
    @if((Auth::user()->hasRole('school')) or (Auth::user()->hasRole('student')) or (Auth::user()->hasRole('teacher')) or (Auth::user()->hasRole('parent')))
        <div class="row">
            @if(Auth::user()->hasRole('school'))
                @if(isset($monitors) && !($monitors['error']))
                    <div class="col-md-12">
                        <div class="row">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    <h3 class="box-title">Time:
                                        {{$monitors['routine'][0]->period->start_time}}
                                        - {{$monitors['routine'][0]->period->end_time}} | Period
                                        : {{$monitors['routine'][0]->period->title}}
                                    </h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="panel-body">
                                    @if(!empty($monitors))
                                        @foreach($monitors['routine'] as $routine)

                                            @php
                                                $totAtnd = count($routine->attendances->where('date_id',$monitors['atnDate']->id)->where('status', 1));

                                                $totStd = count($routine->section->students);

                                                $hWrk = $routine->homeworks->where('date_id',$monitors['atnDate']->id)->first();
                                                //dd($hWrk);
                                                if(!empty($hWrk)){
                                                $hw = $hWrk->home_work;
                                                }else{
                                                $hw = 'Not Provide';
                                                }



                                            @endphp
                                            <div class="box-body col-md-3 currentRoutineMonitoring">
                                                <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                                                <div class="small-box bg-aqua">
                                                    <div class="inner " style="min-height: 175px">
                                                        <h4>{{$routine->course->title}} - {{$routine->section->title}}
                                                            ({{$totAtnd}} / {{$totStd}})
                                                        </h4>
                                                        <p>{{$routine->subject->title}}
                                                            by {{$routine->teacher->name}}
                                                        </p>
                                                        <p>Lession : {{$hw}}</p>
                                                    </div>
                                                    <a href="#" class="small-box-footer">
                                                        More info <i class="fa fa-arrow-circle-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        @endforeach
                                    @endif
                                </div>
                                <!-- /.box-body -->

                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-md-12">
                        <div class="row">
                            <div class="box box-primary">
                                <div class="box-header">
                                    <i class="ion ion-clipboard"></i>
                                    @php
                                        $time = time();
                                    @endphp
                                    <h3 class="box-title">Time:
                                        {{  date("h:i:sa") }} </h3>
                                </div>
                                <!-- /.box-header -->
                                <div class="panel-body">
                                    <h3>No running class at this moment</h3>
                                </div>
                                <!-- /.box-body -->

                            </div>
                        </div>
                    </div>
                @endif
            @endif

            <div class="col-md-6">
                <div class="box box-primary">
                    <div class="box-header">
                        <i class="ion ion-clipboard"></i>

                        <h3 class="box-title">Notice Board</h3>
                    </div>
                    <div class="box-body">
                        <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                        <ul class="todo-list" style="margin-top:15px;">
                            @if(!empty($notices))
                                @foreach($notices as $notice)
                                    <li>
                        <span class="handle">
                        </span>
                                        <span class="text"> <i class="fa fa-calendar-check-o"
                                                               aria-hidden="true"></i>
                                            {{ $notice->content }}
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i>
                                                {{ $notice->created_at->diffForHumans() }}
                            </small>

                        </span>

                                    </li>
                                @endforeach

                            @endif
                        </ul>

                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
            @if(!empty($remarks))
                <div class="col-md-6">
                    <div class="box box-primary">
                        <div class="box-header">
                            <i class="ion ion-clipboard"></i>

                            <h3 class="box-title">Message from District Admin</h3>
                        </div>
                        <div class="box-body" style="margin-top: -20px!important;">
                            <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                            <ul class="todo-list" style="margin-top:15px;">
                                @foreach($remarks as $remark)
                                    <li>
                                        <span class="handle"> </span>
                                        <span class="text"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                            {{ $remark->content }}
                                            <small class="label label-danger"><i class="fa fa-clock-o"></i>
                                                {{ $remark->created_at->diffForHumans() }}
                                            </small>
                                        </span>
                                    </li>
                                @endforeach
                            </ul>
                        </div>
                        <!-- /.box-body -->
                    </div>
                </div>
            @endif
        </div>
    @endif

    @if(Auth::user()->hasRole('student') || Auth::user()->hasRole('parent'))

        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title"> Attendence Information</h3>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body table-responsive no-padding">
                                <table class="table table-hover">
                                    <tr>
                                        <th>ID</th>
                                        <th>Name</th>
                                        <th>Subject</th>
                                        <th>Attendence Status</th>
                                        <th>Home work</th>
                                    </tr>
                                    @if(!empty($myAttendence))
                                        @foreach($myAttendence as $attendence)
                                            {{--                                    {{ dd($attendence->status=='null') }}--}}
                                            @php
                                                if($attendence->status==0){
                                                $status = "No";
                                                $label = 'label-danger';
                                                }else{
                                                $status = 'Yes';
                                                $label = 'label-success';
                                                }
                                            @endphp
                                            <tr>
                                                <td></td>
                                                <td>{{ Auth::user()->name}}</td>
                                                <td>{{ $attendence->routine->subject->title }}</td>
                                                <td><span class="label {{ $label }}">{{ $status }}</span></td>
                                                <td>
                                                    @if(is_object($attendence->routine->homeworks))
                                                        @foreach($attendence->routine->homeworks as $homework)

                                                            {{ $homework->home_work }}
                                                        @endforeach
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endif
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </div>
        </div>
    @endif

@endsection
@section('other_script')
    <script>
        jQuery(document).ready(function () {
            $(".knob").knob({
                /*change : function (value) {
                 //console.log("change : " + value);
                 },
                 release : function (value) {
                 console.log("release : " + value);
                 },
                 cancel : function () {
                 console.log("cancel : " + this.value);
                 },*/
                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.value);
                            this.o.cursor
                            && (sa = ea - 0.3)
                            && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.previousColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                    }
                }
            })
//            var windowWidth = $(window).width();
//            if(windowWidth > 767){
//                var heights = [];
//                $('.currentRoutineMonitoring .bg-aqua').each(function(){
//                    var height = $(this).height();
//                    heights.push(height);
//                });
//                var max = Math.max.apply(Math,heights);
//            }
//            $('.currentRoutineMonitoring .bg-aqua').css("height",max);
//            alert(max)
        });

    </script>
    <script src="{{ asset('plugins/jquery.knob.js') }}"></script>

    {{--javascript time--}}
@endsection