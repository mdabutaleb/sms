@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Student Statistics {{ $coursestudent->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/coursestudent') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/coursestudent/' . $coursestudent->id . '/edit') }}" title="Edit Coursestudent"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/coursestudent', $coursestudent->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Coursestudent',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $coursestudent->id }}</td>
                        </tr>
                        <tr><th> Session </th><td> {{ $coursestudent->sessionInfo->title }} </td></tr>
                        <tr><th> Class </th><td> {{ $coursestudent->course->title }} </td></tr>
                        <tr><th> Section </th><td> {{ $coursestudent->section->title }} </td></tr>
                        <tr><th> Total Student </th><td> {{ $coursestudent->total_student }} </td></tr>
                        <tr><th> Total Student In Database </th><td> {{ $coursestudent->student->count() }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
