@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">Edit Student Statistics #{{ $coursestudent->id }}</div>
            <div class="panel-body">
                <a href="{{ url('/dashboard/coursestudent') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($coursestudent, [
                    'method' => 'PATCH',
                    'url' => ['/dashboard/coursestudent', $coursestudent->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('coursestudent.form-edit', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
@endsection
