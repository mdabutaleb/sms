<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('session', $sessions, null , ['class'=>'form-control', 'placeholder' => 'Select Year','disabled' => 'disabled'])!!}
        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('course_id', $courses, null , ['class'=>'form-control', 'placeholder' => 'Select Class','disabled' => 'disabled'])!!}
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
    {!! Form::label('section_id', 'Section', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
         {!! Form::select('section_id', $sections, null , ['class'=>'form-control', 'placeholder' => 'Select Section','disabled' => 'disabled'])!!}
        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('total_student') ? 'has-error' : ''}}">
    {!! Form::label('total_student', 'Total Student', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('total_student', null, ['class' => 'form-control']) !!}
        {!! $errors->first('total_student', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('total_student') ? 'has-error' : ''}}">
    {!! Form::label('Current_student', 'Added In Database', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('Current_student', $coursestudent->student->count(), ['class' => 'form-control','disabled' => 'disabled']) !!}
        {!! $errors->first('Current_student', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
