@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Teacheractivities</div>
        <div class="panel-body">
            {{--<a href="{{ url('/dashboard/teacheractivities/create') }}" class="btn btn-success btn-sm"--}}
            {{--title="Add New Teacheractivity">--}}
            {{--<i class="fa fa-plus" aria-hidden="true"></i> Add New--}}
            {{--</a>--}}
            <div class="col-md-6 ">
                {!! Form::open(['url' => '/dashboard/teacheractivities', 'class' => 'form-horizontal', 'files' => true]) !!}

                @include ('teacheractivities.form')

                {!! Form::close() !!}
            </div>
            <div class="col-md-6 ">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/teacheractivities', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
                        <div class="input-group">
                            <input type="text" class="form-control" name="search" placeholder="Search...">
                            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
                        </div>
                        {!! Form::close() !!}
                    </div>
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-borderless">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Content</th>
                                    <th>Due Date</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($teacheractivities as $item)
                                    <tr>
                                        <td>{{ $item->id }}</td>
                                        <td>{{ $item->content }}</td>
                                        <td>{{ date("F jS, Y", strtotime($item->due_date)) }}</td>
                                        <td>
                                            <a href="{{ url('/dashboard/teacheractivities/' . $item->id) }}"
                                               title="View Teacheractivity">
                                                {{--<button class="btn btn-info btn-xs">--}}
                                                {{--<i class="fa fa-eye" aria-hidden="true"></i>--}}
                                                {{--View--}}
                                                {{--</button>--}}
                                            </a>
                                            <a href="{{ url('/dashboard/teacheractivities/' . $item->id . '/edit') }}"
                                               title="Edit Teacheractivity">
                                                <button class="btn btn-primary btn-xs">
                                                    <i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                                                </button>
                                            </a>
                                            {!! Form::open([
                                                'method'=>'DELETE',
                                                'url' => ['/dashboard/teacheractivities', $item->id],
                                                'style' => 'display:inline'
                                            ]) !!}
                                            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                    'type' => 'submit',
                                                    'class' => 'btn btn-danger btn-xs',
                                                    'title' => 'Delete Teacheractivity',
                                                    'onclick'=>'return confirm("Confirm delete?")'
                                            )) !!}
                                            {!! Form::close() !!}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="pagination-wrapper"> {!! $teacheractivities->appends(['search' => Request::get('search')])->render() !!} </div>
                        </div>
                    </div>
                </div>


            </div>


        </div>
    </div>
@endsection
