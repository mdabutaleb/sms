@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Teacheractivity {{ $teacheractivity->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/teacheractivities') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/teacheractivities/' . $teacheractivity->id . '/edit') }}" title="Edit Teacheractivity"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/teacheractivities', $teacheractivity->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Teacheractivity',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $teacheractivity->id }}</td>
                        </tr>
                        <tr><th> User Id </th><td> {{ $teacheractivity->user_id }} </td></tr><tr><th> School Id </th><td> {{ $teacheractivity->school_id }} </td></tr><tr><th> Content </th><td> {{ $teacheractivity->content }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
