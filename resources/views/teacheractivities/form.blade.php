<div class="form-group {{ $errors->has('content') ? 'has-error' : ''}}">
    {{--{!! Form::label('content', 'Task Name', ['class' => 'col-md-6 control-label']) !!}--}}
    <div class="col-md-12">
        {!! Form::textarea('content', null, ['class' => 'form-control', 'placeholder'=> 'task name']) !!}
        {!! $errors->first('content', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('due_date') ? 'has-error' : ''}}">
    {{--{!! Form::label('due_date', 'Due Date', ['class' => 'col-md-3 control-label']) !!}--}}
    <div class="col-md-12">
        {!! Form::text('due_date', null, ['class' => 'form-control datepicker', 'placeholder' => 'Due Date']) !!}
        {!! $errors->first('due_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class=" col-md-3">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
