@extends('layouts.app')
@section('htmlheader_title')
My Attendance
@endsection

@section('content')

<div class="panel panel-default">
    <div class="panel-heading"><div class="col-md-9"><h3>{{$msg}}</h3></div><div class="col-md-3 text-right"><label>See Previous :</label> <input type="text" class="datepicker" Placeholder="{{$qDate}}"></div><div class="clearfix"></div></div>
    <div class="panel-body">
        @if($error==1)
            <h3>{{$msg}}</h3>
        @else
        <div class="col-md-12 table-responsive homeWork">
          <div class="row">
            @php
              $count = 0;
            @endphp
            @foreach($routine as $r)
              @php
                $hwork = $r->homeworks->where('date_id',$atnDate->id)->first();
                if(isset($hwork->status)){
                  if($hwork->status == 1){
                    $status = 'btn success';
                    $msg = $hwork->home_work;
                    $bg = 'bg-success';
                  }else{
                    $status = 'btn danger';
                    $msg = 'No Homework In This Class';
                    $bg = 'bg-danger';
                  }
                }else{
                  $status = 'btn warning';
                  $msg = 'No Homework In This Class';
                  $bg = 'bg-warning';
                }
                $count++;
              @endphp
              <div class="col-md-4 table-responsive {{$bg}} attendance">
                <table class="table table-bordered">
                  <tr><th width="30%">Period</th><th>{{(isset($r->period->title))?$r->period->title:''}} ({{$r->period->start_time}} - {{$r->period->end_time}})</th></tr>
                  <tr><td>Subject</td><td>{{(isset($r->subject->title))?$r->subject->title:''}}</td></tr>
                  <tr><td>Teacher</td><td>{{(isset($r->teacher->name))?$r->teacher->name:''}}</td></tr>
                  <tr><td>Home Work</td><td><b>{!!$msg!!}</b></td></tr>
                </table>
              </div>
            @endforeach
          </div>
        </div>
        @endif
        @section('other_script')
        <script>
            jQuery(document).ready(function(){
                $('#session').change(function() {
                     document.location.href = location.href + '?year=' + $(this).val();
                });
            })
            setInterval(function() {
              window.location.reload();
            }, 300000);

            $('.datepicker').blur(function(){
              var inpDate = $(this).val();
              if(inpDate){
                document.location.href = location.href + '?&cDate=' + inpDate;
              }
            });
        </script>
        @endsection

    </div>
</div>
@endsection
