@extends('layouts.app')
@section('htmlheader_title')
    My Attendance
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="col-md-9"><h3>{{$msg}}</h3></div>
            <div class="col-md-3 text-right"><label>See Previous :</label> <input type="text" class="datepicker"
                                                                                  Placeholder="{{$qDate}}"></div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            @if($error==1)
                <h3>{{$msg}}</h3>
            @else
                <div class="col-md-12 table-responsive">
                    <div class="row">
                        @php
                            $count = 0;
                        @endphp
                        @foreach($routine as $r)
                            @php
                                //$user_id coming from controller
                                 $atnd = $r->attendances->where('user_id', $user_id)->where('date_id',$atnDate->id)->first();
                                  if(isset($atnd->status)){
                                    if($atnd->status == 1){
                                      $status = 'btn success';
                                      $msg = 'Attend';
                                      $bg = 'bg-success';
                                    }else{
                                      $status = 'btn danger';
                                      $msg = 'Absent';
                                      $bg = 'bg-danger';
                                    }
                                  }else{
                                    $status = 'btn warning';
                                    $msg = 'Not Inserted';
                                    $bg = 'bg-warning';
                                  }
                                  $count++;
                            @endphp
                            <div class="col-md-4 table-responsive {{$bg}} attendance">
                                <table class="table table-bordered">
                                    <tr>
                                        <th width="30%">Period</th>
                                        <th>{{(isset($r->period->title))?$r->period->title:''}}
                                            ({{$r->period->start_time}} - {{$r->period->end_time}})
                                        </th>
                                    </tr>
                                    <tr>
                                        <td>Subject</td>
                                        <td>{{(isset($r->subject->title))?$r->subject->title:''}}</td>
                                    </tr>
                                    <tr>
                                        <td>Teacher</td>
                                        <td>{{(isset($r->teacher->name))?$r->teacher->name:''}}</td>
                                    </tr>

                                    <tr>
                                        <td>Attendance Status</td>
                                        <td>
                                            <button class="{{$status}}">{{$msg}}</button>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        @endforeach
                    </div>
                </div>
            @endif
            @section('other_script')
                <script>
                    jQuery(document).ready(function () {
                        $('#session').change(function () {
                            document.location.href = location.href + '?year=' + $(this).val();
                        });
                    })
                    setInterval(function () {
                        window.location.reload();
                    }, 300000);

                    $('.datepicker').blur(function () {
                        var inpDate = $(this).val();
                        if (inpDate) {
                            document.location.href = location.href + '?&cDate=' + inpDate;
                        }
                    });
                </script>
            @endsection

        </div>
    </div>
@endsection
