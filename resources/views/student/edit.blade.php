@extends('layouts.app')

@section('htmlheader_title')
Edit Student
@endsection
@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">Edit Student #{{ $student->id }}</div>
            <div class="panel-body">
                <a href="{{ url('/dashboard/student') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($student, [
                    'method' => 'PATCH',
                    'url' => ['/dashboard/student', $student->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('student.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
@endsection
