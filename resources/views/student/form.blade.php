<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('bn_name') ? 'has-error' : ''}}">
    {!! Form::label('bn_name', 'Bn Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bn_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bn_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('religion') ? 'has-error' : ''}}">
    {!! Form::label('religion', 'Religion', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('religion', Config::get('enums.religion'), null , ['class'=>'form-control', 'placeholder' => 'Choose Religion',])!!}
        {!! $errors->first('religion', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('blood_group') ? 'has-error' : ''}}">
    {!! Form::label('blood_group', 'Blood Group', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('blood_group', Config::get('enums.blood'), null , ['class'=>'form-control', 'placeholder' => 'Choose Blood Group...',])!!}
        {!! $errors->first('blood_group', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('gender', Config::get('enums.gender'), null , ['class'=>'form-control', 'placeholder' => 'I am',])!!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
    {!! Form::label('dob', 'Dob', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dob', null, ['class' => 'form-control datepicker']) !!}
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('course_id', $courses, null , ['class'=>'form-control', 'placeholder' => 'Select Class',])!!}
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
    {!! Form::label('section_id', 'Section', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
         {!! Form::select('section_id', $sections, null , ['class'=>'form-control', 'placeholder' => 'Select Section',])!!}
        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('session', $sessions, null , ['class'=>'form-control', 'placeholder' => 'Select Year',])!!}
        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('shift_id') ? 'has-error' : ''}}">
    {!! Form::label('shift_id', 'Shift Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
         {!! Form::select('shift_id', Config::get('enums.shift'), null , ['class'=>'form-control', 'placeholder' => 'Select Shift',])!!}
        {!! $errors->first('shift_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('shift_id') ? 'has-error' : ''}}">
    {!! Form::label('std_group', 'Shift Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
         {!! Form::select('std_group', Config::get('enums.group'), null , ['class'=>'form-control', 'placeholder' => 'Select Group',])!!}
        {!! $errors->first('std_group', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('roll_no') ? 'has-error' : ''}}">
    {!! Form::label('roll_no', 'Roll No', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('roll_no', null, ['class' => 'form-control']) !!}
        {!! $errors->first('roll_no', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('r_password', ['class' => 'form-control']) !!}
        {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('third_subject') ? 'has-error' : ''}}">
    {!! Form::label('third_subject', 'Third Subject', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('third_subject', null, ['class' => 'form-control']) !!}
        {!! $errors->first('third_subject', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('fourth_subject') ? 'has-error' : ''}}">
    {!! Form::label('fourth_subject', 'Fourth Subject', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('fourth_subject', null, ['class' => 'form-control']) !!}
        {!! $errors->first('fourth_subject', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('previous_school') ? 'has-error' : ''}}">
    {!! Form::label('previous_school', 'Previous School', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('previous_school', null, ['class' => 'form-control']) !!}
        {!! $errors->first('previous_school', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('others') ? 'has-error' : ''}}">
    {!! Form::label('others', 'Others', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('others', null, ['class' => 'form-control']) !!}
        {!! $errors->first('others', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('parent') ? 'has-error' : ''}}">
    {!! Form::label('parent', 'Parent', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('parent', null, ['class' => 'form-control']) !!}
        {!! $errors->first('parent', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('status', '1', true) !!} Active</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('status', '0') !!} Deactive</label>
</div>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('public_status') ? 'has-error' : ''}}">
    {!! Form::label('public_status', 'Public Status', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('public_status', '1', true) !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('public_status', '0') !!} No</label>
</div>
        {!! $errors->first('public_status', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-6">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
