@extends('layouts.app')



@section('content')



    <div class="panel panel-default">

        <div class="panel-heading">Student {{ $student->id }}</div>

        <div class="panel-body">



            <a href="{{ url('/dashboard/student') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

            <a href="{{ url('/dashboard/student/' . $student->id . '/edit') }}" title="Edit Student"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

            {!! Form::open([

                'method'=>'DELETE',

                'url' => ['dashboard/student', $student->id],

                'style' => 'display:inline'

            ]) !!}

            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(

                    'type' => 'submit',

                    'class' => 'btn btn-danger btn-xs',

                    'title' => 'Delete Student',

                    'onclick'=>'return confirm("Confirm delete?")'

            ))!!}

            {!! Form::close() !!}

            <br/>

            <br/>



            <div class="table-responsive">

                <table class="table table-borderless">

                    <tbody>

                    <tr>

                        <th></th><td><img src="{{ url('/'.$student->image) }}"/></td>

                    </tr>


                    <tr>

                        <th>ID</th><td>{{ $student->id }}</td>

                    </tr>

                    <tr><th> User Id </th><td> {{ $student->user_id }} </td></tr><tr><th> Name </th><td> {{ $student->name }} </td></tr>

                    <tr><th> Name (Bangla)</th><td> {{ $student->bn_name }} </td></tr>

                    <tr><th> Religion</th><td> {{ $student->religion }} </td></tr>

                    <tr><th> Blood group</th><td> {{ $student->blood_group }} </td></tr>


                    <tr><th> Gender </th><td> {{ $student->gender }} </td></tr>


                    <tr><th> Date of Birth </th><td> {{ $student->dob }} </td></tr>

                    <tr><th> Class </th><td> {{ $student->course->title }} </td></tr>


                    @if(!empty($student->section->title))
                        <tr>
                            <th> Section </th>
                            <td> {{ $student->section->title }} </td>
                        </tr>
                    @endif

                    @if(!empty($student->acsession->title))

                        <tr><th> Session </th><td> {{ $student->acsession->title }} </td></tr>
                    @endif



                    <tr><th> Roll </th><td> {{ $student->roll_no }} </td></tr>


                    <tr><th> Mobile </th><td> {{ $student->mobile }} </td></tr>

                    <tr><th> Email </th><td> {{ $student->email }} </td></tr>

                    </tbody>

                </table>

            </div>



        </div>

    </div>

@endsection

