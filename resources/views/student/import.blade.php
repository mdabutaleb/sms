@extends('layouts.app')

@section('htmlheader_title')
    Import Student
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">{{$msg}}</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/student') }}" title="Back">
                <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
            </a>
            <br/>
            <br/>
            @isset($input)
                {!! Form::open(['url' => route('student.import.storedb'), 'class' => 'form-horizontal', 'files' => true]) !!}
                <div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
                    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('session', $sessions, $input['session'] , ['class'=>'form-control', 'placeholder' => 'Select Year',])!!}
                        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
                    {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('course_id', $courses, $input['course_id'] , ['class'=>'form-control', 'placeholder' => 'Select Class',])!!}
                        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
                    {!! Form::label('section_id', 'Section', ['class' => 'col-md-2 control-label']) !!}
                    <div class="col-md-6">
                        {!! Form::select('section_id', $sections, $input['section_id'] , ['class'=>'form-control', 'placeholder' => 'Select Section',])!!}
                        {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <table class="table-borderd table table-striped">
                    <tr>
                        <th>Name</th>
                        <th>Roll</th>
                        <th>Mobile</th>
                        <th>Email</th>
                        <th>Pass</th>
                    </tr>
                    @php
                        $i = 0;
                    @endphp
                    @foreach($results as $r)
                        <tr>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('names['.$i.']', $r->name, ['class' => 'form-control','required' => 'required']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('rolls['.$i.']', $r->roll, ['class' => 'form-control','required' => 'required']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('mobiles.['.$i.']', $r->mobile, ['class' => 'form-control']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('emails.['.$i.']', (empty($r->email))?$user->institute->ins_eiin.$input['session'].$input['course_id'].$input['section_id'].sprintf("%03d", $r->roll).'@lms.com':$r->email, ['class' => 'form-control stdEmail','required' => 'required']) !!}
                                </div>
                            </td>
                            <td>
                                <div class="form-group">
                                    {!! Form::text('passs.['.$i.']', (empty($r->pass))?bin2hex(openssl_random_pseudo_bytes(5)):$r->pass, ['class' => 'form-control','required' => 'required']) !!}
                                </div>
                            </td>
                        </tr>
                        @php
                            $i++;
                        @endphp
                    @endforeach
                </table>

                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Import', ['class' => 'btn btn-primary stExcell']) !!}
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                @else
                    {!! Form::open(['url' => route('student.import.store'), 'class' => 'form-horizontal', 'files' => true]) !!}
                    <div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
                        {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('session', $sessions, null , ['class'=>'form-control', 'placeholder' => 'Select Year',])!!}
                            {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
                        {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('course_id', $courses, null , ['class'=>'form-control', 'placeholder' => 'Select Class',])!!}
                            {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
                        {!! Form::label('section_id', 'Section', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::select('section_id', $sections, null , ['class'=>'form-control', 'placeholder' => 'Select Section',])!!}
                            {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group {{ $errors->has('excel') ? 'has-error' : ''}}">
                        {!! Form::label('Import File', 'excel', ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-8">
                            {!! Form::file('excel', null, ['class' => 'form-control']) !!}
                            {!! $errors->first('excel', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-2 col-md-6">
                            {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Import', ['class' => 'btn btn-primary']) !!}
                        </div>
                    </div>

                    {!! Form::close() !!}
                    @endisset
        </div>
    </div>
@endsection
@section('other_script')
    <script>
        jQuery(document).ready(function () {
            jQuery('.stExcell').click(function () {
                var submit = true;
                var emails = [];
                jQuery('.stdEmail').each(function () {
                    emails.push(jQuery(this).val());
                });
                var valid_url = '{{ url("emailvalidation") }}';
                $.ajax({
                    url: valid_url,
                    data: {emails: emails},
                    type: "get",
                    async: false,
                    success: function (data) {
                        if (data.error == 0) {
                            submit = true;
                        }
                        else {
                            submit = false;
                            totEr = data.email.length;
                            for (i = 0; i < totEr; i++) {
                                $(':input[value="' + data.email[i] + '"]').each(function () {
                                    $(this).parent().append("<span class='valError'>This email already Used!!</span>")
                                });
                                //alert(data.email[i]);
                            }
                        }
                    }
                });
                return submit;

            });
        })
    </script>
@endsection