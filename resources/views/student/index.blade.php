@extends('layouts.app')

@section('htmlheader_title')
Manage Student
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Student</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/student/create') }}" class="btn btn-success btn-sm" title="Add New Student">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/student', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th>Class</th><th>Section</th><th>Roll</th><th>Name</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($student as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->course->title }}</td><td>{{ $item->section->title }}</td><td>{{ $item->roll_no }}</td><td>{{ $item->name }}</td>
                        <td>
                            <a href="{{ url('/dashboard/student/' . $item->id) }}" title="View Student"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/dashboard/student/' . $item->id . '/edit') }}" title="Edit Student"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/student', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Student',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $student->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
