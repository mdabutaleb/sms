@extends('layouts.app')
@section('htmlheader_title')
Create Student
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Create New Student</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/student') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            {!! Form::open(['url' => '/dashboard/student', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('student.form')

            {!! Form::close() !!}

        </div>
    </div>
@endsection
