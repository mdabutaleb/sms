@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Routin Modification</div>
    <div class="panel-body">
        @isset($error)
        <div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
            <div class="col-md-12">
                <h3>{{$msg}}</h3>
            </div>
        </div>
        <br>
        @endisset
        <div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('session', $sessions, null , ['class'=>'form-control','placeholder' => 'select a Year'])!!}
                {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        @section('other_script')
        <script>
            jQuery(document).ready(function(){
                $('#session').change(function() {
                     document.location.href = location.href + '?year=' + $(this).val();
                });
            })
        </script>
        @endsection

    </div>
</div>
@endsection
