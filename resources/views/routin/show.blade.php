@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Routin {{ $routin->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/routin') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/routin/' . $routin->id . '/edit') }}" title="Edit Routin"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/routin', $routin->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Routin',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $routin->id }}</td>
                        </tr>
                        <tr><th> School Id </th><td> {{ $routin->school_id }} </td></tr><tr><th> Session </th><td> {{ $routin->session }} </td></tr><tr><th> Day Id </th><td> {{ $routin->day_id }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
