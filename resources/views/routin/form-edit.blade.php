@if(!isset($year))
<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('session', $sessions, null , ['class'=>'form-control','placeholder' => 'select a Year'])!!}
        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
    </div>
</div>
@else
@if($error==1)
    <h3>{{$msg}}</h3>
@else
<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    <div class="col-md-6">
        <h3>{{$msg}}</h3>
    </div>
</div>
<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    <div class="col-md-6">
        {!! Form::hidden('session', $year, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="col-md-12 table-responsive">
    <table class="table table-bordered table-striped routineTable text-center">
        <tr><th colspan="2"></th>@foreach($periods as $period)<th>{{$period->title}}<br> ({{$period->start_time}} - {{$period->end_time}})</th>@endforeach</tr>
        
        @php
            $i = 0;
        @endphp
        @foreach($days as $day)
            <tr>
                <td>{{$day->title}}</td>
                <td class="courseSection">
                    @foreach($courses as $course)
                        <table class="table table-bordered">
                            @foreach($course->sections->where('school_id',Auth::id()) as $section)
                                <tr>@if($loop->first)
                                    <td rowspan="{{$loop->count}}" class="courseTitle">
                                        <b>{{$course->title}}</b>
                                    </td>
                                    @endif
                                    <td>{{$section->title}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @endforeach
                </td>
                @php
                    $j = 0;
                @endphp
                @foreach($periods as $period)
                <td>
                    @php
                        $c = 0;
                    @endphp
                    @foreach($courses as $course)
                        <table class="table table-bordered">
                            @php
                                $s = 0;
                                $sections = $course->sections->where('school_id',Auth::id());
                            @endphp
                            @foreach($sections as $section)
                                <tr>
                                    <td>
                                        @php
                                        $pInfo = $section->periodsubject->where('day_id',$day->id)->where('period_id',$period->id)->where('session',$msYr->id)->first();
                                        @endphp
                                        <div class="col-md-12"> 
                                            {!! Form::select('subject_ids'.$i.$j.$c.$s, $course->subjects->pluck('title','id'), (isset($pInfo->subject->id))?$pInfo->subject->id:null , ['class'=>'form-control','placeholder' => 'select a Subject'])!!}
                                        </div>
                                        <div class="col-md-12"> 
                                            {!! Form::select('teacher_ids'.$i.$j.$c.$s, $teachers, (isset($pInfo->teacher->user_id))?$pInfo->teacher->user_id:null , ['class'=>'form-control','placeholder' => 'select a Teacher'])!!}
                                        </div>
                                        @if(!isset($pInfo->id))
                                        {!! Form::hidden('period_ids['.$i.']['.$j.']['.$c.']['.$s.']', $period->id, ['class' => 'form-control']) !!}
                                        {!! Form::hidden('course_ids['.$i.']['.$j.']['.$c.']['.$s.']', $course->id, ['class' => 'form-control']) !!}
                                        {!! Form::hidden('sections_ids['.$i.']['.$j.']['.$c.']['.$s.']', $section->id, ['class' => 'form-control']) !!}
                                        @endif
                                        {!! Form::hidden('day_ids['.$i.']['.$j.']['.$c.']['.$s.']', $day->id, ['class' => 'form-control']) !!}
                                        {!! Form::hidden('ids['.$i.']['.$j.']['.$c.']['.$s.']', (isset($pInfo->id))?$pInfo->id:null, ['class' => 'form-control']) !!}
                                    </td>
                                </tr>
                                @php
                                    $s++;
                                @endphp
                            @endforeach
                        </table>
                    @php
                        $c++;
                    @endphp
                    @endforeach
                </td>
                @php
                    $j++;
                @endphp
                @endforeach
            </tr>
            @php
                $i++;
            @endphp
        @endforeach
    </table>
</div>
<div class="form-group">
    <div class="col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
@endif
@endif
@section('other_script')
<script>
    jQuery(document).ready(function(){
        $('#session').change(function() {
             document.location.href = location.href + '?year=' + $(this).val();
        });
    })
</script>
@endsection