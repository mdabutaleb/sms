@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">Edit Routin #{{ $msYr['title'] }}</div>
            <div class="panel-body">
                <a href="{{ url('/dashboard/routin') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif
                {!! Form::open(['url'=> route('routin.update',$msYr->id) , 'method'=> 'PUT']) !!}
                @include ('routin.form-edit', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
@endsection
