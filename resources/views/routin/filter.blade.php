@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading"><h3>{{$msg}}</h3></div>
    <div class="panel-body">
        @if(!isset($year))
        {!! Form::open(['url'=> route('routine.filter.show') , 'method'=> 'POST']) !!}
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('session', $sessions, null , ['class'=>'form-control'])!!}
                {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('course_id', $courses, null , ['class'=>'form-control','placeholder' => 'select a Class'])!!}
                {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('day_id', 'Day', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('day_id', $days, null , ['class'=>'form-control','placeholder' => 'select a Day'])!!}
                {!! $errors->first('day_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-10 pull-right">
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Show Routine', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>
        {!! Form::close() !!}
        @else
        {!! Form::open(['url'=> route('routine.filter.show') , 'method'=> 'POST']) !!}
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('session', $sessions, null , ['class'=>'form-control'])!!}
                {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('course_id', 'Class', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('course_id', $courses, null , ['class'=>'form-control','placeholder' => 'select a Class'])!!}
                {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group col-md-4 {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('day_id', 'Day', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-10">
                {!! Form::select('day_id', $days, null , ['class'=>'form-control','placeholder' => 'select a Day'])!!}
                {!! $errors->first('day_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group">
            <div class="col-md-2 col-md-offset-10 pull-right">
                {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Show Routine', ['class' => 'btn btn-primary']) !!}
            </div>
        </div>

        @if($error==1)
            <h3>{{$msg}}</h3>
        @else
        <div class="col-md-12 table-responsive">
            <table class="table table-bordered table-striped routineTable text-center table table-responsive">
                <tr><th colspan="2"></th>@foreach($periods as $period)<th>{{$period->title}}<br> ({{$period->start_time}} - {{$period->end_time}})</th>@endforeach</tr>
                
                @php
                    $i = 0;
                @endphp
                @foreach($days as $day)
                    <tr>
                        <td>{{$day->title}}</td>
                        <td class="courseSection">
                            @foreach($courses as $course)
                                <table class="table table-bordered">
                                    @foreach($course->sections as $section)
                                        <tr>@if($loop->first)
                                            <td rowspan="{{$loop->count}}" class="courseTitle">
                                                <b>{{$course->title}}</b>
                                            </td>
                                            @endif
                                            <td>{{$section->title}}</td>
                                        </tr>
                                    @endforeach
                                </table>
                            @endforeach
                        </td>
                        @php
                            $j = 0;
                        @endphp
                        @foreach($periods as $period)
                        <td>
                            @php
                                $c = 0;
                            @endphp
                            @foreach($courses as $course)
                                <table class="table table-bordered">
                                    @php
                                        $s = 0;
                                    @endphp
                                    @foreach($course->sections as $section)
                                        <tr>
                                            <td>
                                                <div class="col-md-12"> 
                                                    @php
                                                    $pInfo = $section->periodsubject->where('day_id',$day->id)->where('session',$year)->where('period_id',$period->id)->first();
                                                    if(isset($pInfo) AND $pInfo->count()){
                                                        if($pInfo->subject_id){
                                                            echo $pInfo->subject->title.'<br>';
                                                        }
                                                        if($pInfo->teacher_id){
                                                             echo $pInfo->teacher->name;
                                                        }   
                                                    }
                                                    @endphp
                                                </div>
                                            </td>
                                        </tr>
                                        @php
                                            $s++;
                                        @endphp
                                    @endforeach
                                </table>
                            @php
                                $c++;
                            @endphp
                            @endforeach
                        </td>
                        @php
                            $j++;
                        @endphp
                        @endforeach
                    </tr>
                    @php
                        $i++;
                    @endphp
                @endforeach
            </table>
        </div>
        @endif
        @endif
    </div>
</div>
@endsection
