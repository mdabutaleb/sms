@extends('layouts.app')
@section('htmlheader_title')
    Class Monitoring
@endsection

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">
            <div class="col-md-9"><h3>{{$msg}}</h3></div>
            <div class="col-md-3 text-right"><label>See Previous :</label> <input type="text" class="datepicker"
                                                                                  Placeholder="{{$qDate}}"></div>
            <div class="clearfix"></div>
        </div>
        <div class="panel-body">
            @if($error==1)

                <h3>{{ $msg }}</h3>
            @else
                <div class="col-md-12 table-responsive">
                    <table class="table table-bordered table-striped routineTable text-center">
                        <tr>
                            <th colspan="2">

                            </th>
                            @foreach($periods as $period)
                                <th>
                                    {{ !empty($period->title) ? $period->title : ''}}<br>
                                    ({{ !empty($period->start_time) ? $period->start_time : ''}}
                                    - {{!empty($period->end_time) ? $period->end_time : ''}})
                                </th>
                            @endforeach
                        </tr>

                        @php

                            $i = 0;
                        @endphp
                        @foreach($days as $day)
                            <tr>
                                <td>{{$day->title}}</td>
                                <td class="courseSection">
                                    {{--{{ dd($courses) }}--}}
                                    @foreach($courses as $course)
                                        <table class="table table-bordered">
                                            @php
                                                $mycourseSection = $course->sections->where('course_id',$course->id)->where('school_id', Auth::id());
                                            @endphp
                                            @foreach($mycourseSection as $section)
                                                <tr>@if($loop->first)
                                                        <td rowspan="{{$loop->count}}" class="courseTitle">
                                                            <b>{{$course->title}}</b>
                                                        </td>
                                                    @endif
                                                    <td>{{$section->title}}</td>
                                                </tr>
                                            @endforeach
                                        </table>
                                    @endforeach
                                </td>
                                @php
                                    $j = 0;
                                @endphp
                                @foreach($periods as $period)
                                    <td>
                                        @php
                                            $c = 0;
                                        @endphp
                                        @foreach($courses as $course)
                                            <table class="table table-bordered">
                                                @php
                                                    $s = 0;
                                                @endphp

                                                @foreach($course->sections as $section)
                                                    @php
                                                        $pInfo = $section->periodsubject->where('day_id',$day->id)->where('session',$year)->where('period_id',$period->id)->first();
                                                        $routine = $pInfo;
                                                       if($routine!=null){
                                                        $hstatus = $routine->homeworks->where('date_id',$atnDate->id);
                                                        if(count($hstatus)){
                                                          $hinStatus = 1;
                                                          $msg = 'Homeworks For This Class Been Added';
                                                        }else{
                                                          $hinStatus = 0;
                                                          $msg = 'Homeworks For This Class Not Yet Added';
                                                        }
                                                        $astatus = $routine->attendances->where('date_id',$atnDate->id);

                                                        $attend = count($astatus->where('status',1));
                                                        $absent = count($astatus->where('status',0));


                                                        if(count($astatus)){
                                                          $ainStatus = 1;
                                                          $msg = 'Attendance For This Class Been Added';
                                                        }else{
                                                          $ainStatus = 0;
                                                          $msg = 'Attendance For This Class Not Yet Added';
                                                        }

                                                        $currenTime = strtotime($cDate->toTimeString());

                                                        $periodStart = strtotime($routine->period->start_time);
                                                        $periodEnd = strtotime($routine->period->end_time);


                                                        if($hinStatus == 1 || $ainStatus == 1){
                                                            $boxClass = 'bg-aqua';
                                                            $icon = 'ion ion-ios-pricetag-outline';
                                                        }

                                                        elseif($currenTime > $periodStart &&  $currenTime > $periodEnd){
                                                            $boxClass = 'bg-red';
                                                            $icon = 'ion ion-ios-pricetag-outline';
                                                        }elseif($currenTime < $periodStart){
                                                            $boxClass = 'bg-yellow';
                                                            $icon = 'ion ion-ios-pricetag-outline';
                                                        }elseif($currenTime > $periodStart && $currenTime < $periodStart){
                                                            $boxClass = 'bg-aqua';
                                                            $icon = 'ion ion-ios-heart-outline';
                                                        }
                                                        else{
                                                            $boxClass = 'bg-green';
                                                            $icon = 'ion ion-ios-heart-outline';
                                                        }
                                                    @endphp
                                                    <tr class="{{$boxClass}} monitorRow">
                                                        <td class="monitorCell">
                                                            <div class="col-md-12">
                                                                @php
                                                                    if(isset($pInfo) AND $pInfo->count()){
                                                                        echo "<div class='infoPart'>";
                                                                        if($pInfo->subject_id){
                                                                            echo $pInfo->subject->title.'<br>';
                                                                        }
                                                                        if($pInfo->teacher_id){
                                                                             echo $pInfo->teacher->name;
                                                                         echo "</div>";
                                                                @endphp
                                                                <div class="viewPart">
                                                                    <a href="#" class="btn btn-default btn-xs"
                                                                       data-toggle="modal"
                                                                       data-target="#myModal1{{$i}}{{$j}}{{$c}}{{$s}}">View
                                                                        Attendance <i aria-hidden="true"
                                                                                      class="fa fa-eye"></i></a><br>
                                                                    <a href="#" class="btn btn-default btn-xs"
                                                                       data-toggle="modal"
                                                                       data-target="#myModal{{$i}}{{$j}}{{$c}}{{$s}}">View
                                                                        Homework <i aria-hidden="true"
                                                                                    class="fa fa-eye"></i></a>
                                                                </div>
                                                                @php
                                                                    }
                                                                    }
                                                                @endphp
                                                            </div>
                                                            <!-- Modal -->
                                                            @if(isset($routine->teacher))
                                                                <div class="modal fade atndStatus"
                                                                     id="myModal{{$i}}{{$j}}{{$c}}{{$s}}" role="dialog">

                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal">&times;
                                                                                </button>
                                                                                <h4 class="modal-title">Homework</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                <table class="table-bordered tblFullwidth tblCenter">
                                                                                    <tr>
                                                                                        <td>Period Name</td>
                                                                                        <td>
                                                                                            <b>{{$routine->period->title}}</b>

                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Period Time</td>
                                                                                        <td>
                                                                                            <b>{{$routine->period->start_time.' - '.$routine->period->end_time}}</b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Subject</td>
                                                                                        <td>
                                                                                            <b>{{$routine->subject->title}}</b>
                                                                                        </td>
                                                                                    </tr>

                                                                                    @if($hinStatus == 1)
                                                                                        <tr>
                                                                                            <td colspan="2">{!!$hstatus->first()->home_work!!}</td>
                                                                                        </tr>
                                                                                    @endif
                                                                                </table>
                                                                                </p>
                                                                            </div>

                                                                            <!--
                                                                            <div class="modal-footer">
                                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            </div>
                                                                            -->
                                                                        </div>

                                                                    </div>
                                                                </div>


                                                                <!-- Modal -->
                                                                <div class="modal fade atndStatus"
                                                                     id="myModal1{{$i}}{{$j}}{{$c}}{{$s}}"
                                                                     role="dialog">
                                                                    <div class="modal-dialog">

                                                                        <!-- Modal content-->
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <button type="button" class="close"
                                                                                        data-dismiss="modal">&times;
                                                                                </button>
                                                                                <h4 class="modal-title">Attendance
                                                                                    Report</h4>
                                                                            </div>
                                                                            <div class="modal-body">
                                                                                <p>
                                                                                <table class="table-bordered tblFullwidth tblCenter">
                                                                                    <tr>
                                                                                        <td>Period Name</td>
                                                                                        <td>
                                                                                            <b>{{$routine->period->title}}</b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Period Time</td>
                                                                                        <td>
                                                                                            <b>{{$routine->period->start_time.' - '.$routine->period->end_time}}</b>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>Subject</td>
                                                                                        <td>
                                                                                            <b>{{$routine->subject->title}}</b>
                                                                                        </td>
                                                                                    </tr>

                                                                                    @if($ainStatus == 1)
                                                                                        <tr>
                                                                                            <td>Status</td>
                                                                                            <td><b>Attendance
                                                                                                    Inserted</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Total Student</td>
                                                                                            <td>
                                                                                                <b>@isset($routine->section->totalStd->total_student){{$routine->section->totalStd->total_student}}@endisset</b>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Total Student(DB)</td>
                                                                                            <td>
                                                                                                <b>{{count($routine->section->students)}}</b>
                                                                                            </td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Today Attend</td>
                                                                                            <td><b>{{$attend}}</b></td>
                                                                                        </tr>
                                                                                        <tr>
                                                                                            <td>Today Absent</td>
                                                                                            <td><b>{{$absent}}</b></td>
                                                                                        </tr>
                                                                                    @else
                                                                                        <tr>
                                                                                            <td>Status</td>
                                                                                            <td><b>Attendance Not
                                                                                                    Inserted</b></td>
                                                                                        </tr>
                                                                                    @endif

                                                                                </table>
                                                                                </p>
                                                                            </div>
                                                                            <!--
                                                                            <div class="modal-footer">
                                                                              <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                                            </div>
                                                                            -->
                                                                        </div>

                                                                    </div>
                                                                </div>
                                                            @endif
                                                        </td>
                                                    </tr>

                                                    @php
                                                        }
                                                                                                                $s++;

                                                    @endphp

                                                @endforeach
                                            </table>

                                            @php
                                                $c++;
                                            @endphp

                                        @endforeach
                                    </td>
                                    @php
                                        $j++;
                                    @endphp

                                @endforeach
                            </tr>

                            @php
                                $i++;
                            @endphp

                        @endforeach
                    </table>
                </div>
            @endif

            @section('other_script')
                <script>
                    jQuery(document).ready(function () {
                        $('#session').change(function () {
                            document.location.href = location.href + '?year=' + $(this).val();
                        });
                    })
                    setInterval(function () {
                        window.location.reload();
                    }, 300000);

                    $('.datepicker').blur(function () {
                        var inpDate = $(this).val();
                        if (inpDate) {
                            document.location.href = location.href + '?&cDate=' + inpDate;
                        }
                    });
                </script>
            @endsection

        </div>
    </div>
@endsection
