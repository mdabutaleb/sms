<div class="form-group {{ $errors->has('school_id') ? 'has-error' : ''}}">
    {!! Form::label('school_id', 'School Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('school_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('school_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('user_id') ? 'has-error' : ''}}">
    {!! Form::label('user_id', 'User Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('user_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('user_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('routine_id') ? 'has-error' : ''}}">
    {!! Form::label('routine_id', 'Routine Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('routine_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('routine_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('date_id') ? 'has-error' : ''}}">
    {!! Form::label('date_id', 'Date Id', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('date_id', null, ['class' => 'form-control']) !!}
        {!! $errors->first('date_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        <div class="checkbox">
    <label>{!! Form::radio('status', '1') !!} Yes</label>
</div>
<div class="checkbox">
    <label>{!! Form::radio('status', '0', true) !!} No</label>
</div>
        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('added_by') ? 'has-error' : ''}}">
    {!! Form::label('added_by', 'Added By', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('added_by', null, ['class' => 'form-control']) !!}
        {!! $errors->first('added_by', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('updated_by') ? 'has-error' : ''}}">
    {!! Form::label('updated_by', 'Updated By', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('updated_by', null, ['class' => 'form-control']) !!}
        {!! $errors->first('updated_by', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
