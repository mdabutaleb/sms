@extends('layouts.app')

@section('content')
@include('atnd_info.take-attendence')
@endsection

@section('other_script')
    <script>
        jQuery(document).ready(function () {
            $('.datepicker').blur(function () {
                var inpDate = $(this).val();
                if (inpDate) {
                    document.location.href = location.href + '?&cDate=' + inpDate;
                }
            });
        })
    </script>
@endsection
