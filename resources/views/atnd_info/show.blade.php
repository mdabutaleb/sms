@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Atnd_info {{ $atnd_info->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/atnd_info') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/atnd_info/' . $atnd_info->id . '/edit') }}" title="Edit Atnd_info"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/atnd_info', $atnd_info->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Atnd_info',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $atnd_info->id }}</td>
                        </tr>
                        <tr><th> School Id </th><td> {{ $atnd_info->school_id }} </td></tr><tr><th> User Id </th><td> {{ $atnd_info->user_id }} </td></tr><tr><th> Routine Id </th><td> {{ $atnd_info->routine_id }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
