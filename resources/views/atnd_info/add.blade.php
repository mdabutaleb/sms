@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Attendance Information</div>
        <div class="panel-body atndPanel">
            <div class="table-responsive col-md-6">
                {!! Form::open(['url' => route('atnd_info.add.store'), 'class' => 'form-horizontal', 'files' => false]) !!}
                <table class="table table-borderless " id="attendence">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Roll</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    @isset($routine->section->students)
                        {!! Form::hidden('date_str', $date, ['class' => 'form-control']) !!}
                        {!! Form::hidden('routine_id', $routine_id, ['class' => 'form-control']) !!}
                        <tbody>
                        @php
                            $i = 0;
                        @endphp
                        @foreach($routine->section->students as $student)
                            <tr>
                                <td>
                                    {{$student->name}}
                                </td>
                                <td>{{$student->roll_no}}</td>
                                <td>
                                    {!! Form::label('status', ' ', ['id' => $i ]) !!}
                                    {!! Form::checkbox('status'.$i, 1, true, ['id' =>  $i])!!}
                                </td>
                                {!! Form::hidden('user_ids['.$i.']', $student->user_id, ['class' => 'form-control']) !!}
                            </tr>
                            @php
                                $i++;
                            @endphp
                        @endforeach
                        <tr>
                            <td colspan="3">
                                <div class="form-group col-md-10 col-xs-12">
                                    <label><b>HomeWorks</b></label>
                                    <textarea class="form-control" rows="2" placeholder="Enter Homeworks..."
                                              name="home_work"></textarea>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3">{!! Form::submit('SAVE', ['class' => 'btn btn-primary']) !!}</td>

                        </tr>
                        </tbody>
                    @endisset
                </table>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('other_script')
    <script>
        $(document).ready(function () {
            $(document).on('click', 'tbody tr', function () {
                var checked = $(this).find('input[type="checkbox"]');
                checked.prop('checked', !checked.is(':checked'));
                $(this).toggleClass('selected'); // or anything else for highlighting purpose
            });
            $(document).on('click', 'input[type="checkbox"]', function () {
                $(this).prop('checked', !$(this).is(':checked'));
                $(this).parent('tr').toggleClass('selected'); // or anything else for highlighting purpose
            });
        });
    </script>
@endsection


