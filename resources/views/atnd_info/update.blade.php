@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Attendance Information</div>
    <div class="panel-body atndPanel">
        <div class="table-responsive">
          {!! Form::open(['url' => route('atnd_info.update'), 'class' => 'form-horizontal', 'files' => false]) !!}
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Roll</th>
                        <th>Status</th>
                    </tr>
                </thead>
                @isset($routine->section->students)
                {!! Form::hidden('date_str', $date, ['class' => 'form-control']) !!}
                {!! Form::hidden('routine_id', $routine_id, ['class' => 'form-control']) !!}
                <tbody>
                    @php
                      $i = 0;
                    @endphp
                    @foreach($routine->section->students as $student)
                      <tr>
                      @php
                        $atndStatus = $student->attends->where('routine_id',$routine->id)->where('date_id',$atnd_date->id)->first();
                        $status = ($atndStatus->status == 1)?true:false;
                      @endphp
                          <td>{{$student->name}}</td>
                          <td>{{$student->roll_no}}</td>
                          <td>{!! Form::checkbox('status'.$i, 1, $status)!!}</td>
                          {!! Form::hidden('ids'.$i, $atndStatus->id, ['class' => 'form-control']) !!}
                          {!! Form::hidden('user_ids['.$i.']', $student->user_id, ['class' => 'form-control']) !!}
                      </tr>
                      @php
                        $i++;
                      @endphp
                    @endforeach
                    @php
                    $homework = $routine->homeworks->where('date_id',$atnd_date->id)->first();
                    @endphp
                    <tr>
                      <td colspan="3">
                        <div class="form-group col-md-10 col-xs-12">
                            <label><b>HomeWorks</b></label>
                            <textarea class="form-control" rows="2"  name="home_work">{{isset($homework->home_work)?$homework->home_work:null}}</textarea>
                          </div>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="3">{!! Form::submit('Update', ['class' => 'btn btn-primary']) !!}</td>
                        
                    </tr>
                </tbody>
                @endisset
            </table>
            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
