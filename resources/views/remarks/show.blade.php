@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Remark {{ $remark->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/remarks') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/remarks/' . $remark->id . '/edit') }}" title="Edit Remark"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/remarks', $remark->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Remark',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $remark->id }}</td>
                        </tr>
                        <tr><th> School Id </th><td> {{ $remark->school_id }} </td></tr><tr><th> District Id </th><td> {{ $remark->district_id }} </td></tr><tr><th> Content </th><td> {{ $remark->content }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
