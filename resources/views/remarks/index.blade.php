@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Remarks</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/remarks/create') }}" class="btn btn-success btn-sm" title="Add New Remark">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/remarks', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th>School Id</th><th>District Id</th><th>Content</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($remarks as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->school_id }}</td><td>{{ $item->district_id }}</td><td>{{ $item->content }}</td>
                        <td>
                            <a href="{{ url('/dashboard/remarks/' . $item->id) }}" title="View Remark"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/dashboard/remarks/' . $item->id . '/edit') }}" title="Edit Remark"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/remarks', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Remark',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $remarks->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
