@extends('layouts.app')

@section('htmlheader_title')
Create Teacher
@endsection
@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Create New Teacher</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/teacher') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => '/dashboard/teacher', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('teacher.form')

            {!! Form::close() !!}

        </div>
    </div>
@endsection
