<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('bn_name') ? 'has-error' : ''}}">
    {!! Form::label('bn_name', 'Bn Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('bn_name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('bn_name', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('religion') ? 'has-error' : ''}}">
    {!! Form::label('religion', 'Religion', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('religion', Config::get('enums.religion'), null , ['class'=>'form-control', 'placeholder' => 'Choose Religion...',])!!}
        {!! $errors->first('religion', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('blood_group') ? 'has-error' : ''}}">
    {!! Form::label('blood_group', 'Blood Group', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('blood_group', Config::get('enums.blood'), null , ['class'=>'form-control', 'placeholder' => 'Choose Blood Group...',])!!}
        {!! $errors->first('blood_group', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('designition') ? 'has-error' : ''}}">
    {!! Form::label('designition', 'Designition', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('designition', null, ['class' => 'form-control']) !!}
        {!! $errors->first('designition', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('teach_subject') ? 'has-error' : ''}}">
    {!! Form::label('teach_subject', 'Teach Subject', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('teach_subject', null, ['class' => 'form-control']) !!}
        {!! $errors->first('teach_subject', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nationality') ? 'has-error' : ''}}">
    {!! Form::label('nationality', 'Nationality', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nationality', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nationality', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('nid') ? 'has-error' : ''}}">
    {!! Form::label('nid', 'Nid', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('nid', null, ['class' => 'form-control']) !!}
        {!! $errors->first('nid', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('gender') ? 'has-error' : ''}}">
    {!! Form::label('gender', 'Gender', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('gender', Config::get('enums.gender'), null , ['class'=>'form-control', 'placeholder' => 'I AM',])!!}
        {!! $errors->first('gender', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('dob') ? 'has-error' : ''}}">
    {!! Form::label('dob', 'Dob', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('dob', null, ['class' => 'form-control datepicker']) !!}
        {!! $errors->first('dob', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('joining_date') ? 'has-error' : ''}}">
    {!! Form::label('joining_date', 'Joining Date', ['class' => 'col-md-2 control-label ']) !!}
    <div class="col-md-6">
        {!! Form::text('joining_date', null, ['class' => 'form-control datepicker']) !!}
        {!! $errors->first('joining_date', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('r_password', ['class' => 'form-control']) !!}
        {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mpo_index') ? 'has-error' : ''}}">
    {!! Form::label('mpo_index', 'Mpo Index', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mpo_index', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mpo_index', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    {!! Form::label('image', 'Image', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::file('image', null, ['class' => 'form-control']) !!}
        {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('staff_order') ? 'has-error' : ''}}">
    {!! Form::label('staff_order', 'Staff Order', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('staff_order', null, ['class' => 'form-control']) !!}
        {!! $errors->first('staff_order', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('education') ? 'has-error' : ''}}">
    {!! Form::label('education', 'Education', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('education', null, ['class' => 'form-control']) !!}
        {!! $errors->first('education', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('training') ? 'has-error' : ''}}">
    {!! Form::label('training', 'Training', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('training', null, ['class' => 'form-control']) !!}
        {!! $errors->first('training', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('ex_school') ? 'has-error' : ''}}">
    {!! Form::label('ex_school', 'Ex School', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('ex_school', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ex_school', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('others') ? 'has-error' : ''}}">
    {!! Form::label('others', 'Others', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('others', null, ['class' => 'form-control']) !!}
        {!! $errors->first('others', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-6">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
