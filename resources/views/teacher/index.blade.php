@extends('layouts.app')

@section('htmlheader_title')
Manage Teacher
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Teacher</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/teacher/create') }}" class="btn btn-success btn-sm" title="Add New Teacher">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/teacher', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th>Name</th><th>User Id</th><th> MPO</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($teacher as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->name }}</td><td>{{ $item->email }}</td><td>{{ $item->mpo_index }}</td>
                        <td>
                            <a href="{{ url('/dashboard/teacher/' . $item->id) }}" title="View Teacher"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            {{--<a href="{{ url('dashboard/profile/'. $user->id. '/'.str_slug($user->name, '-')) }}" title="View Teacher"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>--}}
                            <a href="{{ url('/dashboard/teacher/' . $item->id . '/edit') }}" title="Edit Teacher"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/teacher', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Teacher',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $teacher->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
