@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Teacher {{ $teacher->name }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/teacher') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/teacher/' . $teacher->id . '/edit') }}" title="Edit Teacher"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/teacher', $teacher->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Teacher',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th></th><td><img src="{{ url('/'.$teacher->image) }}"/></td>
                        </tr>
                        <tr>
                            <th>ID</th><td>{{ $teacher->id }}</td>
                        </tr>
                        <tr><th> Name </th><td> {{ $teacher->name }} </td></tr><tr><th> User Id </th><td> {{ $teacher->email }} </td></tr><tr><th> MPO </th><td> {{ $teacher->mpo_index }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
