<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Name *', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', (isset($guardian->user->name)) ? $guardian->user->name : '', ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('mobile') ? 'has-error' : ''}}">
    {!! Form::label('mobile', 'Mobile', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('mobile', (isset($guardian->user->mobile)) ? $guardian->user->mobile : '', ['class' => 'form-control']) !!}
        {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email Address *', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', (isset($guardian->user->email)) ? $guardian->user->email : '', ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('r_password', ['class' => 'form-control']) !!}
        {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('address') ? 'has-error' : ''}}">
    {!! Form::label('address', 'Address', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
        {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<h4 class="col-md-offset-2">Select Associate Student</h4>
<div class="gurdianStd">
    <div class="studBlock">
        <div class="studGroup">
        <div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
            {!! Form::label('session', 'Session *', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('session[]', $sessions, null , ['class'=>'form-control session', 'placeholder' => 'Select Year',])!!}
                {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
            {!! Form::label('course_id', 'Student Class', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                {!! Form::select('course_id[]', $courses, null , ['class'=>'form-control course_id', 'placeholder' => 'Select Class',])!!}
                {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div><div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
            {!! Form::label('section_id', 'Student Section', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                 {!! Form::select('section_id[]', $sections, null , ['class'=>'form-control section_id', 'placeholder' => 'Select Section',])!!}
                {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div><div class="form-group {{ $errors->has('section_id') ? 'has-error' : ''}}">
            {!! Form::label('student_ids', 'Student Info *', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                 {!! Form::select('student_ids[]', ['Please Select Any'],null, ['class'=>'form-control student_id', 'placeholder' => 'Select Student',])!!}
                {!! $errors->first('section_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
        <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
            {!! Form::label('relations_with', 'Relationship *', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                {!! Form::text('relations_with[]', null, ['class' => 'form-control']) !!}
                {!! $errors->first('relations_with', '<p class="help-block">:message</p>') !!}
            </div>
        </div>

            <div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
            {!! Form::label('photo', 'Guardian Photo', ['class' => 'col-md-2 control-label']) !!}
            <div class="col-md-6">
                {!! Form::file('photo', null, ['class' => 'form-control']) !!}
                {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
    </div>
</div>
<div class="col-md-6 col-md-offset-2 text-right">
    <a class="btn btn-info btn-xs addMore" href="javascript:void(0);">Add More Student</a>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
