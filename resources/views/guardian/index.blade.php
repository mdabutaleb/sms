@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Guardian</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/guardian/create') }}" class="btn btn-success btn-sm" title="Add New Guardian">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/guardian', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Name</th><th>Email</th><th>Mobile</th><th>Students</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($guardian as $item)
                    <tr>
                        <td>{{ $item->user->name }}</td>
                        <td>{{ $item->user->email }}</td><td>{{ $item->mobile }}</td>
                        <td>
                        @foreach($item->students as $std)
                        <a href="{{ url('/dashboard/student/' . $std->student->student->id) }}" title="View Student"> {{$std->student->name}}</a>- {!!$std->relation_with.'<br>'!!}
                        @endforeach</td>
                        <td>
                            <a href="{{ url('/dashboard/guardian/' . $item->id) }}" title="View Guardian"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/dashboard/guardian/' . $item->id . '/edit') }}" title="Edit Guardian"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/guardian', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Guardian',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $guardian->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
