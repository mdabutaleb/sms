@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Create New Guardian</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/guardian') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::open(['url' => '/dashboard/guardian', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('guardian.form')

            {!! Form::close() !!}

        </div>
    </div>
@endsection

@section('other_script')
<script>
    jQuery(document).ready(function(){
        jQuery(document).on('change', '.course_id', function() {
            var sectionInfo = jQuery(this).parents().eq(2).find('.section_id');
            jQuery(sectionInfo).html('');
            var courseid = jQuery(this).val();
            $.ajax({
             url: ajax_url,
             data: {id : courseid},
             type: "get",
             success: function(data){
               jQuery(sectionInfo).html(data);
             }
           });
        });

        jQuery(document).on('change', '.section_id', function() {
            var stdInfo = jQuery(this).parents().eq(2).find('.student_id');
            jQuery(stdInfo).html('');
            var courseid = jQuery(this).parents().eq(2).find('.course_id').val();
            var sectionid = jQuery(this).parents().eq(2).find('.section_id').val();
            var session = jQuery(this).parents().eq(2).find('.session').val();
            $.ajax({
             url: sectionstd,
             data: {course_id : courseid,section_id:sectionid,session:session},
             type: "get",
             success: function(data){
               jQuery(stdInfo).html(data);
             }
           });
        });
        jQuery('.addMore').click(function() {  
            var newElem = $('.studGroup').eq(0).clone();
            //console.log(newElem);
            newElem.find('input[type="text"]').val('');
            $('.studBlock').append(newElem);
        });  
    })
</script>
@endsection
