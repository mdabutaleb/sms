@extends('layouts.app')

@section('content')
    <div class="panel panel-default">
        <div class="panel-heading">Create New Homework</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/homework') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            {!! Form::open(['url' => '/dashboard/homework', 'class' => 'form-horizontal', 'files' => true]) !!}

            @include ('homework.form')

            {!! Form::close() !!}

        </div>
    </div>
@endsection
@section('other_script')
<script type="text/javascript" src="{{ url ('/js/tinymce/js/tinymce/tinymce.min.js') }}"></script>
<script>
    jQuery(document).ready(function(){
        $('.datepicker').blur(function(){
          var inpDate = $(this).val();
          if(inpDate){
            document.location.href = location.href + '?&cDate=' + inpDate;
          }
        });
    tinymce.init({
      selector: 'textarea',  // change this value according to your HTML
      auto_focus: 'element1'
    });

    })
</script>
@endsection
