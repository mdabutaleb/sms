@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading"><div class="col-md-9">HomeWork Information</div><div class="col-md-3 text-right"><lable>See Previous :</lable> <input type="text" class="datepicker" Placeholder="{{$qDate}}"></div><div class="clearfix"></div></div>
    <div class="panel-body atndPanel">
      @if (Session::has('msg'))
        <div class="col-md-12">
          <div class="alert alert-warning fade in alert-dismissable">
              <strong> {{ Session::get('msg') }}</strong>
          </div>
        </div>
      @endif
        <div class="col-md-12">
          <p>Date: <b>{{$qDate}}</b></p>
          <p>Day: <b>{{date('l',strtotime($qDate))}}</b></p>
        </div>
        @php
            $i = 1;
        @endphp
        @foreach($routines as $routine)
        <div class="col-md-6">
        @php
            $status = $routine->homeworks->where('date_id',$atnDate->id);
            $attend = count($status->where('status',1));
            $absent = count($status->where('status',0));
            if(count($status)){
              $inStatus = 1;
              $msg = 'Homeworks For This Class Been Added';
            }else{
              $inStatus = 0;
              $msg = 'Homeworks For This Class Not Yet Added';
            }
            $currenTime = strtotime($cDate->toTimeString());
            $periodStart = strtotime($routine->period->start_time);
            $periodEnd = strtotime($routine->period->end_time);
            if($inStatus == 1){
                $boxClass = 'bg-aqua';
                $icon = 'glyphicon glyphicon-ok';
            }
            elseif($currenTime >$periodStart &&  $currenTime >$periodEnd){
                $boxClass = 'bg-red';
                $icon = 'glyphicon glyphicon-warning-sign';
            }elseif($currenTime <$periodStart){
                $boxClass = 'bg-yellow';
                $icon = 'glyphicon glyphicon-adjust';
            }elseif($currenTime >$periodStart && $currenTime <$periodStart){
                $boxClass = 'bg-aqua';
                $icon = 'ion ion-ios-heart-outline';
            }
            else{
                $boxClass = 'bg-green';
                $icon = 'ion ion-ios-heart-outline';
            }
        @endphp
          <!-- Info Boxes Style 2 -->
          <div class="info-box {{$boxClass}}">
            <span class="info-box-icon"><i class="{{$icon}}"></i></span>

            <div class="info-box-content">
              <span class="info-box-text">Class Name : <b>{{$routine->course->title}}</b>  &nbsp;&nbsp;&nbsp;Section Name : <b>{{$routine->section->title}}</b>&nbsp;&nbsp;&nbsp;Period : <b>{{$routine->period->title.' ('.$routine->period->start_time.' - '.$routine->period->end_time.')'}}</b></span>
              <span class="info-box-number">{{$msg}}</span>

              <div class="progress">
                <div class="progress-bar" style="width: 100%"></div>
              </div>
              <span class="progress-description">
                @if($inStatus != 1)
                  <a href="{{route('homework.add',['date'=>strtotime($qDate),'routine'=>$routine->id])}}" class="btn btn-default btn-xs">Add Homework</a>
                @endif
                <a href="#" class="btn btn-default btn-xs" data-toggle="modal" data-target="#myModal{{$i}}">View Homework</a>
                @if($inStatus != 0)
                <a href="{{route('homework.edit',$status->first()->id)}}" class="btn btn-default btn-xs">Edit Homework</a>
                @endif
              </span>
              <!-- Modal -->
              <div class="modal fade atndStatus" id="myModal{{$i}}" role="dialog">
                <div class="modal-dialog">
                
                  <!-- Modal content-->
                  <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Homework</h4>
                    </div>
                    <div class="modal-body">
                      <p>
                        <table class="table-bordered tblFullwidth tblCenter">
                          <tr>
                            <td>Period Name</td><td><b>{{$routine->period->title}}</b></td>
                          </tr>
                          <tr>
                            <td>Period Time</td><td><b>{{$routine->period->start_time.' - '.$routine->period->end_time}}</b></td>
                          </tr>
                          <tr>
                            <td>Subject</td><td><b>{{$routine->subject->title}}</b></td>
                          </tr>
                          @if($inStatus == 1)
                          <tr>
                            <td colspan="2">{!!$status->first()->home_work!!}</td>
                          </tr>
                          @endif
                        </table>
                      </p>
                    </div>
                    <!--
                    <div class="modal-footer">
                      <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                    -->
                  </div>
                  
                </div>
              </div>
            </div>
            <!-- /.info-box-content -->
          </div>
        </div>
        @php
            $i++;
        @endphp
        @endforeach
    </div>
</div>
@endsection

@section('other_script')
<script>
    jQuery(document).ready(function(){
        $('.datepicker').blur(function(){
          var inpDate = $(this).val();
          if(inpDate){
            document.location.href = location.href + '?&cDate=' + inpDate;
          }
        });
    })
</script>
@endsection
