<div class="form-group {{ $errors->has('home_work') ? 'has-error' : ''}}">
    {!! Form::label('home_work', 'Home Work', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::textarea('home_work', null, ['class' => 'form-control']) !!}
        {!! $errors->first('home_work', '<p class="help-block">:message</p>') !!}
        {!! Form::hidden('date_str', $date, ['class' => 'form-control']) !!}
        {!! Form::hidden('routine_id', $routine_id, ['class' => 'form-control']) !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
