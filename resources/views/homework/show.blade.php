@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Homework {{ $homework->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/homework') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/homework/' . $homework->id . '/edit') }}" title="Edit Homework"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/homework', $homework->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Homework',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $homework->id }}</td>
                        </tr>
                        <tr><th> School Id </th><td> {{ $homework->school_id }} </td></tr><tr><th> Routine Id </th><td> {{ $homework->routine_id }} </td></tr><tr><th> Date Id </th><td> {{ $homework->date_id }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
