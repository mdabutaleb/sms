@extends('layouts.app')
@section('style')
    <style>
        .box.box-primary {
            border-top-color: #62cb31;
        }
    </style>
@endsection
@section('htmlheader_title')
    {{ Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px; margin-top: -15px">
            <a href="{{ url('/dashboard/profile/'.Auth::id(). '/edit/'. str_slug(Auth::user()->name, '-')) }}">
                <button class="btn btn-success"><i class="fa  fa-pencil-square-o"></i> Edit</button>
            </a>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-user margin-r-5"></i> {{$user->name }} </h3>
                </div>
                @php
                    $photo =  (!empty($profile->photo)) ? $profile->photo : '/img/default.png';
                @endphp
                <img class="profile-user-img img-responsive" src="{{ asset($photo) }}" alt="Logo">

            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-info margin-r-5"></i> Personal Details </h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <p class="text-center"><b>Email : </b>{{ $user->email }}</p>
                        </li>  <li class="list-group-item">
                            <p class="text-center"><b>Mobile : </b>{{ !empty($profile->mobile) ? $profile->mobile : '' }}</p>
                        </li>
                        </li>  <li class="list-group-item">
                            <p class="text-center"><b>Address : </b>{{ !empty($profile->address) ? $profile->address : '' }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-clock-o"></i> Member Since </h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            @if(!is_null($user->created_at))
                                <p class="text-center"> {{ date_format($user->created_at, 'jS F Y') }}</p>
                            @else
                                <p class="text-center"> {{$user->created_at }}</p>
                            @endif
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-clock-o"></i> Last Update </h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            @php
                                if(!is_null($user->updated_at))
                                 {
                                    $updated_at = ($user->created_at == $user->updated_at) ? "Not Updated Yet" : date_format($user->updated_at, 'jS F Y');
                                }else{
                                $updated_at = 'Not Updated Yet';
                                }
                            @endphp
                            <p class="text-center"> {{ $updated_at }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
@endsection