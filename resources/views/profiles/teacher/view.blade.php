@extends('layouts.app')
@section('style')
    <style>
        .box.box-primary {
            border-top-color: #62cb31;
        }
    </style>
@endsection
@section('htmlheader_title')
    {{ Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px; margin-top: -15px">

            <a href="{{ url('/dashboard/profile/'.Auth::id(). '/edit/'. str_slug(Auth::user()->name, '-')) }}">
                <button class="btn btn-success"><i class="fa  fa-pencil-square-o"></i> Edit</button>
            </a>


        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa margin-r-5"></i>{{ $profile->name }}</h3>
                </div>
                <div class="box-body box-profile">
                    @php
                        $photo =  (!empty($profile->image)) ? $profile->image : '/img/default.png';
                    @endphp
                    <img class="profile-user-img img-responsive" src="{{ asset($photo) }}" alt="Logo">

                    <ul class="list-group list-group-unbordered">
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}
                        <li class="list-group-item">
                            <b>Email : </b> <a class="pull-right">{{ (!empty($profile->email)) ? $profile->email : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Phone : </b> <a class="pull-right">{{ (!empty($profile->mobile)) ? $profile->mobile : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Bangla Name : </b> <a
                                    class="pull-right">{{ (!empty($profile->bn_name)) ? $profile->bn_name : '' }}</a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-user margin-r-5"></i>Personal  Information</h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}

                        <li class="list-group-item">
                            <b>Blood Group : </b> <a
                                    class="pull-right">{{ (!empty($profile->blood_group)) ? $profile->blood_group : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Religion : </b> <a
                                    class="pull-right">{{ (!empty($profile->religion)) ? $profile->religion : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Nationality : </b> <a
                                    class="pull-right">{{ (!empty($profile->nationality)) ? $profile->nationality : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>NID :</b> <a class="pull-right">{{ (!empty($profile->nid)) ? $profile->nid : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>User ID: </b> <a class="pull-right">{{ (!empty($profile->user_id)) ? $profile->user_id : '' }}</a>
                        </li>

                        <li class="list-group-item">
                            <b>Gender : </b> <a
                                    class="pull-right">{{ (!empty($profile->gender)) ? $profile->gender : '' }}</a>
                        </li>

                        <li class="list-group-item">
                            <b>Date of Birth : </b> <a
                                    class="pull-right">{{ (!empty($profile->dob)) ? $profile->dob : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Other's : </b> <a
                                    class="pull-right">{{ (!empty($profile->others)) ? $profile->others : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Member Since : </b> <a class="">{{ (!empty($profile->created_at)) ? date_format($user->created_at, 'jS F Y') : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Last Update: </b>
                            @php
                                $updated_at = ($user->created_at == $user->updated_at) ? "Not Updated Yet" : date_format($user->updated_at, 'jS F Y');
                            @endphp
                            <a class="pull-right">{{ $updated_at }}</a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-briefcase margin-r-5"></i>Professional Information</h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}
                        <li class="list-group-item">
                            <b>Designation : </b> <a class="pull-right">{{ (!empty($profile->designition)) ? $profile->designition : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Teaching Subject : </b> <a class="">{{ (!empty($profile->teach_subject)) ? $profile->teach_subject : '' }}</a>
                        </li>

                        <li class="list-group-item">
                            <b>Joining Date : </b> <a
                                    class="">{{ (!empty($profile->joining_date)) ? $profile->joining_date : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>MPO Index : </b> <a
                                    class="pull-right">{{ (!empty($profile->mpo_index)) ? $profile->mpo_index : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Ex Job :</b><a class="">{{ (!empty($profile->ex_school)) ? $profile->ex_school : '' }}</a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border text-center">
                    <h3 class="box-title"><i class="fa fa-info margin-r-5"></i>Education & Training</h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}
                        <li class="list-group-item">
                            <b>Height Education : </b> <a
                                    class="">{{ (!empty($profile->education)) ? $profile->education : '' }}</a>
                        </li>
                        <li class="list-group-item">
                            <b>Training : </b> <a
                                    class="">{{ (!empty($profile->training)) ? $profile->training : '' }}</a>
                        </li>

                    </ul>

                </div>
            </div>
        </div>

    </div>
@endsection