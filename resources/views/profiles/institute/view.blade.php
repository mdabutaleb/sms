@extends('layouts.app')
@section('style')
    <style>
        .box.box-primary {
            border-top-color: #62cb31;
        }
    </style>
@endsection
@section('htmlheader_title')
    {{ Auth::user()->name}}
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12" style="margin-bottom: 10px; margin-top: -15px">

            <a href="{{ url('/dashboard/profile/'.Auth::id(). '/edit/'. str_slug(Auth::user()->name, '-')) }}">
                <button class="btn btn-success"> <i class="fa  fa-pencil-square-o"></i> Edit</button>
            </a>


        </div>
        <div class="col-md-3">
            <!-- Profile Image -->
            <div class="box box-primary">
                <div class="box-body box-profile">
                    @php
                        $logo =  (!empty($profile->logo)) ? $profile->logo : '/img/default.png';
                    @endphp
                    <img class="profile-user-img img-responsive" src="{{ asset($logo) }}" alt="Logo">

                    @if(empty($profile->logo))
                        <h3 class="profile-username text-center">{{ (!empty($profile->name) ? $profile->name : 'Name Not Found') }}</h3>
                    @endif
                    {{--<p class="text-muted text-center">Software Engineer</p>--}}

                    <ul class="list-group list-group-unbordered">

                        @php
                            $upazilla = (!empty($profile->ins_upazilla)) ? $profile->ins_upazilla : '';
                            $village =  (!empty($profile->ins_vill)) ? $profile->ins_vill : '';
                            $post =  (!empty($profile->ins_post)) ? $profile->ins_post : '';
                            $address = $upazilla . ", ". $village . "-". $post;
                        @endphp
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}

                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-map-marker margin-r-5"></i> <b>Location</b></p>
                            <p class="text-center">{{ $address }}</p>
                        </li>

                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-university margin-r-5"></i> <b>Name: </b></p>
                            <p class="text-center">{{ (!empty($profile->name)) ? $profile->name : '' }}</p>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-envelope margin-r-5"></i> <b>Email: </b></p>
                            <p class="text-center">{{ (!empty($profile->email)) ? $profile->email : '' }}</p>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-phone margin-r-5"></i> <b>Phone: </b></p>
                            <p class="text-center">{{ (!empty($profile->ins_mobile)) ? $profile->ins_mobile : '' }}</p>
                        </li>
                    </ul>

                </div>
                <!-- /.box-body -->
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-user margin-r-5"></i> Principple / Head Master</h3>
                </div>
                <div class="box-body box-profile">
                    @php
                        $photo =  (!empty($profile->principle_img)) ? $profile->principle_img : '/img/default.png';
                    @endphp
                    <img class="profile-user-img img-responsive" src="{{ asset($photo) }}" alt="Logo">

                    <ul class="list-group list-group-unbordered">
                        {{--<b>Address : </b> <a class="pull-right">{{ $address }}</a>--}}
                        <li class="list-group-item">
                            <b>Email : </b>
                            <a class="pull-right">{{ (!empty($profile->principle_email)) ? $profile->principle_email : '' }}</a>
                        </li>


                        <li class="list-group-item">
                            <b>Phone : </b> <a
                                    class="pull-right">{{ (!empty($profile->principle_phone)) ? $profile->principle_phone : '' }}</a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"><i class="fa fa-info-circle margin-r-5"></i> Social Media Information</h3>
                </div>
                <div class="box-body box-profile">
                    <ul class="list-group list-group-unbordered">
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-external-link margin-r-5"></i> <b>Website</b></p>
                            <a href="{{ (!empty($profile->website)) ? $profile->website : '#' }}">
                                <p class="text-center">{{ (!empty($profile->website)) ? $profile->website : 'Not Provided Yet' }}</p>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-facebook-official margin-r-5"></i> <b>Facebook </b>
                            </p>
                            <a href="{{ (!empty($profile->facebook)) ? $profile->facebook : '#' }}">
                                <p class="text-center">{{ (!empty($profile->facebook)) ? $profile->facebook : 'Not Provided Yet' }}</p>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-twitter margin-r-5"></i> <b>Twitter</b></p>
                            <a href="{{ (!empty($profile->facebook)) ? $profile->facebook : '#' }}">
                                <p class="text-center">{{ (!empty($profile->twitter)) ? $profile->twitter : 'Not Provided Yet' }}</p>
                            </a>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-youtube margin-r-5"></i> <b>YouTube</b></p>
                            <a href="{{ (!empty($profile->facebook)) ? $profile->facebook : '#' }}">
                                <p class="text-center">{{ (!empty($profile->youtube)) ? $profile->youtube : 'Not Provided Yet' }}</p>
                            </a>
                        </li>
                    </ul>

                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title"> &nbsp;&nbsp;<i class="fa fa-yelp margin-r-5"></i> Other Important Info</h3>
                </div>
                <div class="box-body">
                    {{--<p class="text-muted text-center">Software Engineer</p>--}}
                    <ul class="list-group list-group-unbordered">

                        <li class="list-group-item">
                            <p class="text-center"><i class="fa margin-r-5"></i> <b>Short Name</b></p>
                            <p class="text-center">{{ (!empty($profile->alise)) ? $profile->alise : ''  }}</p>
                        </li>

                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-meetup margin-r-5"></i> <b>Member Since: </b></p>
                            {{--{{ date_format($notice->created_at, 'jS F Y:') }}--}}
                             <p class="text-center">{{ (!empty($profile->created_at)) ? date_format($profile->created_at, 'jS F Y:') : '' }}</p>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-free-code-camp margin-r-5"></i> <b>Code No: </b></p>
                            @php
                                $code = (!empty($profile->ins_code)) ? $profile->ins_code : $profile->clg_code ;
                            @endphp
                            <p class="text-center">{{ (!empty($code)) ? $code : 'Code Not Provided'}}</p>
                        </li>
                        <li class="list-group-item">
                            <p class="text-center"><i class="fa fa-phone margin-r-5"></i> <b>Eiin No: </b></p>
                            <p class="text-center">{{ (!empty($profile->ins_eiin)) ? $profile->ins_eiin : '' }}</p>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
@endsection