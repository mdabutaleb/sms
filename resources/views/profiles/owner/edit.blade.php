@extends('layouts.app')
@section('style')
    <style>
        .box.box-primary {
            border-top-color: #62cb31;
        }
    </style>
@endsection
@section('htmlheader_title')
    {{ Auth::user()->name}}
@endsection
@section('content')
    <div class="panel panel-default">
        <a href="{{ url('dashboard/profile/'. $user->id. '/'.str_slug($user->name, '-')) }}">
            <button class="btn btn-success btn-flat">Back to Profile</button>
        </a>
        <div class="panel-body">
            <br/>
            <br/>
            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($user, [
                'method' => 'PATCH',
                'url' => url('/dashboard/profile/'.$user->id. '/update/'. str_slug($user->name, '-')),
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            <div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
                {!! Form::label('name', ' Name', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::text('name', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::text('email', null, ['class' => 'form-control']) !!}
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
                {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::password('password', ['class' => 'form-control']) !!}
                    {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
            <div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
                {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
                <div class="col-md-8">
                    {!! Form::password('r_password', ['class' => 'form-control']) !!}
                    {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
                </div>
            </div>

            <div class="form-group">
                <div class="col-md-offset-2 col-md-2">
                    {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Update', ['class' => 'btn btn-primary']) !!}
                </div>
            </div>
            {!! Form::close() !!}

        </div>
    </div>

@endsection