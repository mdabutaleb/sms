@extends('layouts.app')

@section('htmlheader_title')
    All teachers
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="row">

                <div class="box box-primary">
                    <div class="box-header">

                        <i class="ion ion-clipboard"></i>
                        <i class="ion ion-clipboard"></i>
                        @php
                            if(Request::segment(2) =='institute'){
                            $url = "dashboard/institute";
                            }else{
                            $url = "/home";
                            }
                        @endphp
                        <a href="{{ url($url) }}" title="Back">
                            <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i>
                                Back
                            </button>
                        </a>
                        <h3 class="box-title"> Details information about
                            <strong> {{ $instituteName }} </strong></h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="panel-body table-responsive">
                        <table class="table table-hover ">
                            <tr>
                                <th>School / Institute name</th>
                                <th>Principle Name</th>
                                <th>Phone</th>
                                <th>Contact Mail</th>
                                <th> Students</th>
                                <th>Total Teachers</th>
                                <th>Address</th>
                            </tr>

                            <tr>
                                <td>{{ $institute->name }}</td>
                                <td>
                                    {{ $institute->principle_name }}
                                    <br/>
                                    <strong>
                                        ({{ $institute->principle_phone }})
                                    </strong>
                                </td>
                                <td>{{ $institute->ins_mobile }}</td>
                                <td>{{ $institute->email }}</td>
                                <td>
                                    <label class="label label-success"> {{ DB::table('students')->where('school_id', $institute->user_id)->count() }}</label>
                                </td>
                                <td>
                                    <label class="label label-primary">{{ DB::table('teachers')->where('school_id', $institute->user_id)->count() }}</label>
                                </td>
                                <td> {{ $institute->ins_vill . ", ". $institute->ins_upazilla. ", ". $institute->district->name. '-'. $institute->ins_post}}</td>
                            </tr>


                        </table>
                    </div>

                    <!-- /.box-body -->

                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 col-lg-8">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>


                    <h3 class="box-title" style="text-align: center"> All Teachers
                        under<strong> {{ $instituteName }}</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="panel-body table-responsive">
                        <table class="table table-hover ">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Email</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($teacher as $item)
                                <tr>
                                    <td>{{ $item->id }}</td>
                                    <td>{{ $item->name }}</td>
                                    <td>{{ $item->email }}</td>
                                    <td>{{ $item->mpo_index }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                        {{ $teacher->links() }}
                    </div>

                </div>
            </div>
        </div>
        <div class="col-md-4 col-lg-4">

            <div class="box box-primary">
                <div class="box-header">
                    <h4 style="text-align: center;">Students Attendence Report</h4>
                    <hr/>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-12 text-center">
                            <h4 style="text-align: center;">Today</h4>
                            <input type="text" class="knob"
                                   value="{{ $todayReport['parcent'] }}"
                                   data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                   data-readonly="true">
                            <div class="knob-label">Total Students : {{ $todayReport['totalStudents'] }}</div>

                            <div class="knob-label"> Todays Present : {{ $todayReport['totalAttend'] }}
                                ( {{ $todayReport['parcent'] }}% )
                            </div>
                            <div class="knob-label"> Absent Today : {{ $todayReport['absent']  }}</div>
                        </div>
                        <div class="col-xs-12 col-md-12 text-center">
                            <h4 style="text-align: center;">Last 7 Days</h4>
                            <input type="text" class="knob"
                                   value="{{ $SevenDayReport['parcent'] }}"
                                   data-width="90" data-height="90" data-fgColor="#3c8dbc"
                                   data-readonly="true">
                            <div class="knob-label">7 Days Students : {{ $SevenDayReport['total7DayStudents'] }}</div>
                            <div class="knob-label"> 7 Days Present : {{ $SevenDayReport['total7DayAttend'] }}
                                ( {{ $SevenDayReport['parcent'] }}% )
                            </div>
                            <div class="knob-label"> Absent last 7 days
                                : {{ $SevenDayReport['absent'] }}</div>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
            </div>

        </div>
    </div>
    <div class="row">
        <div class="col-md-8">
            <div class="box box-primary">
                <div class="box-header">
                    <i class="ion ion-clipboard"></i>
                    <h3 class="box-title">Remarks By District Admin</h3>
                </div>
                <div class="box-body">
                    <!-- See dist/js/pages/dashboard.js to activate the todoList plugin -->
                    <ul class="todo-list" style="margin-top:15px;">
                        @foreach($remarks as $remark)
                            <li>
                                <span class="handle"></span>
                                <span class="text"> <i class="fa fa-calendar-check-o" aria-hidden="true"></i>
                                    {{ $remark->content }}
                                    <small class="label label-success">
                                    <i class="fa fa-clock-o"></i>{{ $remark->created_at->diffForHumans() }}
                                </small>
                            </span>
                                @if(Auth::user()->hasRole('distadmin'))
                                    <div class="pull-right">
                                        {!! Form::open(['method'=>'DELETE','url' => ['/dashboard/remarks', $remark->id],'style' => 'display:inline']) !!}
                                        {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                                'type' => 'submit',
                                                'class' => 'btn btn-danger btn-xs',
                                                'title' => 'Delete Remark',
                                                'onclick'=>'return confirm("Confirm delete?")'
                                        )) !!}
                                        {!! Form::close() !!}
                                    </div>
                                @endif
                            </li>
                        @endforeach
                    </ul>
                    {{ $remarks->links() }}
                </div>
                <!-- /.box-body -->

            </div>
        </div>
        @if(Auth::user()->hasRole('distadmin'))
            <div class="col-md-4">
                <div class="box box-warning">
                    <div class="box-header with-border">
                        <h3 class="box-title">Tell something about this school/institute</h3>
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        {!! Form::open(['url' => '/dashboard/remarks', 'class' => 'form', 'files' => true]) !!}
                        <div class="form-group">
                            {!! Form::label('textarea', "Only ".  $instituteName . " will see this message") !!}
                            {!! Form::textarea('content', null, ['class' => 'form-control', 'rows' => 6, 'placeholder'=>'say something about this institute']) !!}
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" type="submit">Save</button>
                        </div>
                        {!! Form::hidden('school_id', $school_id) !!}
                        {!! Form::hidden('district_id', $district_id) !!}
                        {!! Form::close() !!}
                    </div>
                    <!-- /.box-body -->
                </div>
            </div>
        @endif


    </div>

@endsection
@section('other_script')
    <script>
        jQuery(document).ready(function () {
            $(".knob").knob({
                /*change : function (value) {
                 //console.log("change : " + value);
                 },
                 release : function (value) {
                 console.log("release : " + value);
                 },
                 cancel : function () {
                 console.log("cancel : " + this.value);
                 },*/
                draw: function () {

                    // "tron" case
                    if (this.$.data('skin') == 'tron') {

                        var a = this.angle(this.cv)  // Angle
                            , sa = this.startAngle          // Previous start angle
                            , sat = this.startAngle         // Start angle
                            , ea                            // Previous end angle
                            , eat = sat + a                 // End angle
                            , r = true;

                        this.g.lineWidth = this.lineWidth;

                        this.o.cursor
                        && (sat = eat - 0.3)
                        && (eat = eat + 0.3);

                        if (this.o.displayPrevious) {
                            ea = this.startAngle + this.angle(this.value);
                            this.o.cursor
                            && (sa = ea - 0.3)
                            && (ea = ea + 0.3);
                            this.g.beginPath();
                            this.g.strokeStyle = this.previousColor;
                            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
                            this.g.stroke();
                        }

                        this.g.beginPath();
                        this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
                        this.g.stroke();

                        this.g.lineWidth = 2;
                        this.g.beginPath();
                        this.g.strokeStyle = this.o.fgColor;
                        this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
                        this.g.stroke();

                        return false;
                    }
                }
            })
//            var windowWidth = $(window).width();
//            if(windowWidth > 767){
//                var heights = [];
//                $('.currentRoutineMonitoring .bg-aqua').each(function(){
//                    var height = $(this).height();
//                    heights.push(height);
//                });
//                var max = Math.max.apply(Math,heights);
//            }
//            $('.currentRoutineMonitoring .bg-aqua').css("height",max);
//            alert(max)
        });

    </script>
    <script src="{{ asset('plugins/jquery.knob.js') }}"></script>

    {{--javascript time--}}
@endsection
