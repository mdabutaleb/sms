@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Distadmin {{ $distadmin->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/distadmin') }}" title="Back">
                <button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button>
            </a>
            <a href="{{ url('/dashboard/distadmin/' . $distadmin->id . '/edit') }}" title="Edit Distadmin">
                <button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit
                </button>
            </a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['/dashboard/distadmin', $distadmin->id],
                'style' => 'display:inline'
            ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Distadmin',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                    <tr>
                        <th>ID</th>
                        <td>{{ $distadmin->id }}</td>
                    </tr>
                    <tr>
                        <th> District </th>
                        <td> {{ $distadmin->district->name}} </td>
                    </tr>
                    <tr>
                        <th> User Name</th>
                        <td> {{ $distadmin->user->name }} </td>
                    </tr>
                    <tr>
                        <th> Photo</th>
                        <td>
                        <img src="{{asset($distadmin->photo)}}" width="250px" height="250px">
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
