<div class="form-group {{ $errors->has('district_id') ? 'has-error' : ''}}">
    {!! Form::label('district_id', 'District ', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('district_id', $districts, null , ['class'=>'form-control', 'placeholder' => 'Select District',])!!}
        {!! $errors->first('district_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', ' Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', isset($distadmin->user->name) ? $distadmin->user->name : null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>


<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('email', isset($distadmin->user->email) ? $distadmin->user->email : null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::password('r_password', ['class' => 'form-control']) !!}
        {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('photo') ? 'has-error' : ''}}">
    {!! Form::label('photo', 'Photo', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('photo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
