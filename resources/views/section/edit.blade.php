@extends('layouts.app')

@section('htmlheader_title')
Edit Section
@endsection
@section('content')
<div class="col-md-9">
    <div class="panel panel-default">
        <div class="panel-heading">Edit Section #{{ $section->id }}</div>
        <div class="panel-body">
            <a href="{{ url('/dashboard/section') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <br />
            <br />

            @if ($errors->any())
                <ul class="alert alert-danger">
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            @endif

            {!! Form::model($section, [
                'method' => 'PATCH',
                'url' => ['/dashboard/section', $section->id],
                'class' => 'form-horizontal',
                'files' => true
            ]) !!}

            @include ('section.form', ['submitButtonText' => 'Update'])

            {!! Form::close() !!}

        </div>
    </div>
@endsection
