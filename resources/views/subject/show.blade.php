@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Subject {{ $subject->title }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/subject') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            @if(Auth::user()->hasRole('owner') || Auth::id() == $subject->school_id)
            <a href="{{ url('/dashboard/subject/' . $subject->id . '/edit') }}" title="Edit Subject"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/subject', $subject->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Subject',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            @endif
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr><th> Class Name </th><td> {{ $subject->course->title }} </td></tr><tr><th> Subject Name </th><td> {{ $subject->title }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
