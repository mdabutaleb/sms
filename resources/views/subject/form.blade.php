<div class="form-group {{ $errors->has('course_id') ? 'has-error' : ''}}">
    {!! Form::label('course_id', 'Subject Class', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('course_id', $courses, null , ['class'=>'form-control', 'placeholder' => 'Select Course',])!!}
        {!! $errors->first('course_id', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('code') ? 'has-error' : ''}}">
    {!! Form::label('code', 'Code', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('code', '<p class="help-block">:message</p>') !!}
    </div>
</div><!--<div class="form-group {{ $errors->has('group') ? 'has-error' : ''}}">
    {!! Form::label('group', 'Group', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('group', null, ['class' => 'form-control']) !!}
        {!! $errors->first('group', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('marks') ? 'has-error' : ''}}">
    {!! Form::label('marks', 'Marks', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('marks', null, ['class' => 'form-control']) !!}
        {!! $errors->first('marks', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('mcq') ? 'has-error' : ''}}">
    {!! Form::label('mcq', 'Mcq', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('mcq', null, ['class' => 'form-control']) !!}
        {!! $errors->first('mcq', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('written') ? 'has-error' : ''}}">
    {!! Form::label('written', 'Written', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('written', null, ['class' => 'form-control']) !!}
        {!! $errors->first('written', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('practical') ? 'has-error' : ''}}">
    {!! Form::label('practical', 'Practical', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('practical', null, ['class' => 'form-control']) !!}
        {!! $errors->first('practical', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('added_by') ? 'has-error' : ''}}">
    {!! Form::label('added_by', 'Added By', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('added_by', null, ['class' => 'form-control']) !!}
        {!! $errors->first('added_by', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('update_by') ? 'has-error' : ''}}">
    {!! Form::label('update_by', 'Update By', ['class' => 'col-md-4 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('update_by', null, ['class' => 'form-control']) !!}
        {!! $errors->first('update_by', '<p class="help-block">:message</p>') !!}
    </div>
</div>
-->
<div class="form-group">
    <div class="col-md-offset-4 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
