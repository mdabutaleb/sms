@extends('layouts.app')

@section('htmlheader_title')
Manage Subject
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Subject</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/subject/create') }}" class="btn btn-success btn-sm" title="Add New Subject">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/subject', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>Class Name</th><th>Subject Name</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($subject as $item)
                    <tr>
                        <td>{{ $item->course->title }}</td><td>{{ $item->title }}</td>
                        <td>
                            <a href="{{ url('/dashboard/subject/' . $item->id) }}" title="View Subject"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            @if(Auth::user()->hasRole('owner') || Auth::id() == $item->school_id)
                            <a href="{{ url('/dashboard/subject/' . $item->id . '/edit') }}" title="Edit Subject"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/subject', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Subject',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                            @endif
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $subject->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
