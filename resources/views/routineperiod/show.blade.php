@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Routine period {{ $routineperiod->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/routineperiod') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/routineperiod/' . $routineperiod->id . '/edit') }}" title="Edit Routineperiod"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/routineperiod', $routineperiod->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Routineperiod',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $routineperiod->id }}</td>
                        </tr>
                        <tr><th> School Id </th><td> {{ $routineperiod->school_id }} </td></tr><tr><th> Session </th><td> {{ $routineperiod->session }} </td></tr><tr><th> Title </th><td> {{ $routineperiod->title }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
