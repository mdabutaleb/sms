@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Routine period</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/routineperiod/create') }}" class="btn btn-success btn-sm" title="Add New Routineperiod">
            <i class="fa fa-plus" aria-hidden="true"></i> Add New
        </a>

        {!! Form::open(['method' => 'GET', 'url' => '/dashboard/routineperiod', 'class' => 'navbar-form navbar-right', 'role' => 'search'])  !!}
        <div class="input-group">
            <input type="text" class="form-control" name="search" placeholder="Search...">
            <span class="input-group-btn">
                <button class="btn btn-default" type="submit">
                    <i class="fa fa-search"></i>
                </button>
            </span>
        </div>
        {!! Form::close() !!}

        <br/>
        <br/>
        <div class="table-responsive">
            <table class="table table-borderless">
                <thead>
                    <tr>
                        <th>ID</th><th>Session</th><th>Title</th><th>Start</th><th>End</th><th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($routineperiod as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->acsession->title }}</td>
                        <td>{{ $item->title }}</td>
                        <td>{{ $item->start_time }}</td>
                        <td>{{ $item->end_time }}</td>
                        <td>
                            <a href="{{ url('/dashboard/routineperiod/' . $item->id) }}" title="View Routineperiod"><button class="btn btn-info btn-xs"><i class="fa fa-eye" aria-hidden="true"></i> View</button></a>
                            <a href="{{ url('/dashboard/routineperiod/' . $item->id . '/edit') }}" title="Edit Routineperiod"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
                            {!! Form::open([
                                'method'=>'DELETE',
                                'url' => ['/dashboard/routineperiod', $item->id],
                                'style' => 'display:inline'
                            ]) !!}
                                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                                        'type' => 'submit',
                                        'class' => 'btn btn-danger btn-xs',
                                        'title' => 'Delete Routineperiod',
                                        'onclick'=>'return confirm("Confirm delete?")'
                                )) !!}
                            {!! Form::close() !!}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
            <div class="pagination-wrapper"> {!! $routineperiod->appends(['search' => Request::get('search')])->render() !!} </div>
        </div>

    </div>
</div>
@endsection
