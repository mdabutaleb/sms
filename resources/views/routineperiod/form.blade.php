<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('session', $sessions, null , ['class'=>'form-control', ])!!}
        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('title', null, ['class' => 'form-control']) !!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('start_time') ? 'has-error' : ''}}">
    {!! Form::label('start_time', 'Start Time', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('time', 'start_time', null, ['class' => 'form-control timepicker']) !!}
        {!! $errors->first('start_time', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('end_time') ? 'has-error' : ''}}">
    {!! Form::label('end_time', 'End Time', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::input('time', 'end_time', null, ['class' => 'form-control timepicker']) !!}
        {!! $errors->first('end_time', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>