@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Notice {{ $notice->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/notice') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/notice/' . $notice->id . '/edit') }}" title="Edit Notice"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/notice', $notice->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Notice',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $notice->id }}</td>
                        </tr>
                        <tr><th> School Name </th><td> {{ $notice->user->name }} </td></tr><tr><th> Content </th><td> {{ $notice->content }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
