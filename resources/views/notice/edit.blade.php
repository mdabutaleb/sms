@extends('layouts.app')

@section('content')
        <div class="panel panel-default">
            <div class="panel-heading">Edit Notice #{{ $notice->id }}</div>
            <div class="panel-body">
                <a href="{{ url('/dashboard/notice') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                <br />
                <br />

                @if ($errors->any())
                    <ul class="alert alert-danger">
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                @endif

                {!! Form::model($notice, [
                    'method' => 'PATCH',
                    'url' => ['/dashboard/notice', $notice->id],
                    'class' => 'form-horizontal',
                    'files' => true
                ]) !!}

                @include ('notice.form', ['submitButtonText' => 'Update'])

                {!! Form::close() !!}

            </div>
        </div>
@endsection
