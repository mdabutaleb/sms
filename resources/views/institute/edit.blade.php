@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Edit Institute #{{ $institute->id }}</div>
    <div class="panel-body">
        <a href="{{ url('dashboard/institute') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <br />
        <br />

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::model($institute, [
            'method' => 'PATCH',
            'url' => ['/dashboard/institute', $institute->id],
            'class' => 'form-horizontal',
            'files' => true
        ]) !!}

        @include ('institute.form', ['submitButtonText' => 'Update'])

        {!! Form::close() !!}

    </div>
</div>
@endsection
