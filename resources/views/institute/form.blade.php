<div class="form-group {{ $errors->has('name') ? 'has-error' : ''}}">
    {!! Form::label('name', 'Institute Name', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('alise') ? 'has-error' : ''}}">
    {!! Form::label('alise', 'Institutetitute Name (BN)', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('alise', null, ['class' => 'form-control']) !!}
        {!! $errors->first('alise', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_code') ? 'has-error' : ''}}">
    {!! Form::label('ins_code', 'Institute Code (School)', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('clg_code') ? 'has-error' : ''}}">
    {!! Form::label('clg_code', 'Institute Code (College)', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('clg_code', null, ['class' => 'form-control']) !!}
        {!! $errors->first('clg_code', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_eiin') ? 'has-error' : ''}}">
    {!! Form::label('ins_eiin', 'Institute Eiin', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_eiin', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_eiin', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_vill') ? 'has-error' : ''}}">
    {!! Form::label('ins_vill', 'Institute Village', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_vill', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_vill', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_post') ? 'has-error' : ''}}">
    {!! Form::label('ins_post', 'Institute Post', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_post', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_post', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_upazilla') ? 'has-error' : ''}}">
    {!! Form::label('ins_upazilla', 'Institute Upazilla', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_upazilla', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_upazilla', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('district_id') ? 'has-error' : ''}}">
    {!! Form::label('district_id', 'Institute District', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::select('district_id', $districts, null , ['class'=>'form-control', 'placeholder' => 'Select District',])!!}
        {!! $errors->first('district_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('ins_mobile') ? 'has-error' : ''}}">
    {!! Form::label('ins_mobile', 'Institute Mobile', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('ins_mobile', null, ['class' => 'form-control']) !!}
        {!! $errors->first('ins_mobile', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('email') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('password') ? 'has-error' : ''}}">
    {!! Form::label('password', 'Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::password('password', ['class' => 'form-control']) !!}
        {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('r_password') ? 'has-error' : ''}}">
    {!! Form::label('email', 'Retype Password', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::password('r_password', ['class' => 'form-control']) !!}
        {!! $errors->first('r_password', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('website') ? 'has-error' : ''}}">
    {!! Form::label('website', 'Website', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('website', null, ['class' => 'form-control']) !!}
        {!! $errors->first('website', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('facebook') ? 'has-error' : ''}}">
    {!! Form::label('facebook', 'Facebook', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('facebook', null, ['class' => 'form-control']) !!}
        {!! $errors->first('facebook', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('twitter') ? 'has-error' : ''}}">
    {!! Form::label('twitter', 'Twitter', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('twitter', null, ['class' => 'form-control']) !!}
        {!! $errors->first('twitter', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('youtube') ? 'has-error' : ''}}">
    {!! Form::label('youtube', 'Youtube', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('youtube', null, ['class' => 'form-control']) !!}
        {!! $errors->first('youtube', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('logo') ? 'has-error' : ''}}">
    {!! Form::label('logo', 'Logo', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::file('logo', null, ['class' => 'form-control']) !!}
        {!! $errors->first('logo', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('principle_name') ? 'has-error' : ''}}">
    {!! Form::label('principle_name', 'Principle / Head ', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('principle_name', null, ['class' => 'form-control', 'placeholder' =>'Principle / Head Master Name']) !!}
        {!! $errors->first('principle_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('principle_phone') ? 'has-error' : ''}}">
    {!! Form::label('principle_phone', 'Principle Phone', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('principle_phone', null, ['class' => 'form-control', 'placeholder' =>'Principle / Head Master Phone Number']) !!}
        {!! $errors->first('principle_phone', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('principle_email') ? 'has-error' : ''}}">
    {!! Form::label('principle_email', 'Principle Email', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::text('principle_email', null, ['class' => 'form-control', 'placeholder' =>'Principle / Head Master Email']) !!}
        {!! $errors->first('principle_email', '<p class="help-block">:message</p>') !!}
    </div>
</div>
<div class="form-group {{ $errors->has('principle_img') ? 'has-error' : ''}}">
    {!! Form::label('principle_img', 'Principle img', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-8">
        {!! Form::file('principle_img', null, ['class' => 'form-control']) !!}
        {!! $errors->first('principle_img', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-2">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
