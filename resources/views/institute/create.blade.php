@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Create New Institute</div>
    <div class="panel-body">
        <a href="{{ route('institute.index') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <br />
        <br />

        {!! Form::open(['url' => '/dashboard/institute', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('institute.form')

        {!! Form::close() !!}

    </div>
</div>
@endsection
