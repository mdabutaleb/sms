@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Institute {{ $institute->id }}</div>
    <div class="panel-body">

        <a href="{{ url('/dashboard/institute') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <a href="{{ url('/dashboard/institute/' . $institute->id . '/edit') }}" title="Edit Institute"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['dashboard/institute', $institute->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Institute',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
        <br/>
        <br/>

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr>
                        <th></th><td><img src="{{ url('/'.$institute->logo) }}"/></td>
                    </tr>
                    <tr>
                        <th>ID</th><td>{{ $institute->id }}</td>
                    </tr>
                    <tr><th>Institute Name </th><td> {{ $institute->name }} </td></tr><tr><th> Institute Name (BN) </th><td> {{ $institute->alise }} </td></tr><tr><th> Ins Code </th><td> {{ $institute->ins_code }} </td></tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
@endsection