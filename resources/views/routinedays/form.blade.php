<div class="form-group {{ $errors->has('session') ? 'has-error' : ''}}">
    {!! Form::label('session', 'Session', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('session', $sessions, null , ['class'=>'form-control', ])!!}
        {!! $errors->first('session', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('title') ? 'has-error' : ''}}">
    {!! Form::label('title', 'Title', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::select('title', Config::get('enums.days'), null , ['class'=>'form-control', ])!!}
        {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
    </div>
</div><div class="form-group {{ $errors->has('order') ? 'has-error' : ''}}">
    {!! Form::label('order', 'Order', ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::number('order', null, ['class' => 'form-control']) !!}
        {!! $errors->first('order', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-8">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create', ['class' => 'btn btn-primary']) !!}
    </div>
</div>
