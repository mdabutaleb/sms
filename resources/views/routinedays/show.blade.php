@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Routine day {{ $routineday->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/routinedays') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/routinedays/' . $routineday->id . '/edit') }}" title="Edit Routineday"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/routinedays', $routineday->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Routineday',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $routineday->id }}</td>
                        </tr>
                        <tr><th> Session </th><td> {{ $routineday->acsession->title }} </td></tr>
                        <tr><th> Title </th><td> {{ $routineday->title }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
