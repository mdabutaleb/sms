@extends('layouts.app')
@section('htmlheader_title')
View Class
@endsection
@section('content')
<div class="panel panel-default">
    <div class="panel-heading">Course {{ $course->title }}</div>
    <div class="panel-body">

        <a href="{{ url('/dashboard/course') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        @if(Auth::user()->hasRole('owner') || Auth::id() == $course->school_id)
        <a href="{{ url('/dashboard/course/' . $course->id . '/edit') }}" title="Edit Course"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
        {!! Form::open([
            'method'=>'DELETE',
            'url' => ['dashboard/course', $course->id],
            'style' => 'display:inline'
        ]) !!}
            {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                    'type' => 'submit',
                    'class' => 'btn btn-danger btn-xs',
                    'title' => 'Delete Course',
                    'onclick'=>'return confirm("Confirm delete?")'
            ))!!}
        {!! Form::close() !!}
        @endif
        <br/>
        <br/>

        <div class="table-responsive">
            <table class="table table-borderless">
                <tbody>
                    <tr><th> Title </th><td> {{ $course->title }} </td></tr><tr><th> Aliase </th><td> {{ $course->aliase }} </td></tr>
                </tbody>
            </table>
        </div>

    </div>
</div>
@endsection
