@extends('layouts.app')

@section('htmlheader_title')
Create Class
@endsection
@section('content')

<div class="panel panel-default">
    <div class="panel-heading">Create New Class</div>
    <div class="panel-body">
        <a href="{{ url('/dashboard/course') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
        <br />
        <br />

        @if ($errors->any())
            <ul class="alert alert-danger">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif

        {!! Form::open(['url' => '/dashboard/course', 'class' => 'form-horizontal', 'files' => true]) !!}

        @include ('course.form')

        {!! Form::close() !!}

    </div>
</div>
@endsection
