@extends('layouts.app')

@section('content')

    <div class="panel panel-default">
        <div class="panel-heading">Acsession {{ $acsession->id }}</div>
        <div class="panel-body">

            <a href="{{ url('/dashboard/acsession') }}" title="Back"><button class="btn btn-warning btn-xs"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
            <a href="{{ url('/dashboard/acsession/' . $acsession->id . '/edit') }}" title="Edit Acsession"><button class="btn btn-primary btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>
            {!! Form::open([
                'method'=>'DELETE',
                'url' => ['dashboard/acsession', $acsession->id],
                'style' => 'display:inline'
            ]) !!}
                {!! Form::button('<i class="fa fa-trash-o" aria-hidden="true"></i> Delete', array(
                        'type' => 'submit',
                        'class' => 'btn btn-danger btn-xs',
                        'title' => 'Delete Acsession',
                        'onclick'=>'return confirm("Confirm delete?")'
                ))!!}
            {!! Form::close() !!}
            <br/>
            <br/>

            <div class="table-responsive">
                <table class="table table-borderless">
                    <tbody>
                        <tr>
                            <th>ID</th><td>{{ $acsession->id }}</td>
                        </tr>
                        <tr><th> Title </th><td> {{ $acsession->title }} </td></tr><tr><th> Start Date </th><td> {{ $acsession->start_date }} </td></tr>
                        <tr><th> End Date </th><td> {{ $acsession->end_date }} </td></tr>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
@endsection
